using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Airborne.Predication;

namespace Airborne.Aggregation
{
    /// <summary>
    /// Builds List of entities which are required to the main entity to be added.
    /// </summary>
    public static class AggregateExtensions
    {
        private static List<Aggregate> alltypes = new List<Aggregate>();

        /// <summary>
        /// Resolve the required shared entities and sort them as per priority
        /// </summary>
        public static IEnumerable<Aggregate> ResolveDependencies<T>(this T rootType)
        {
            if (rootType.IsNull())
            {
                throw new ArgumentNullException("{0} cannot be null.".FormatInvariantCulture(rootType.GetType().Name));
            }

            alltypes = new List<Aggregate>();
            AddRootInstanceToTheList(rootType);
            BuildDependencyList(rootType);
            return alltypes.ToList().OrderBy(entity => entity.DependancyOrder);
        }

        private static void BuildDependencyList<T>(T rootType)
        {
            var collection = rootType as ICollection;

            if ((collection.IsNotNull()))
            {
                var items = collection; // (ICollection)rootType;
                foreach (var item in items)
                {
                    ResolveChildren(item);
                }
            }

            else
                ResolveChildren(rootType);
        }

        private static void ResolveChildren<T>(T rootType)
        {
            var propertyInfos = rootType.GetType().GetProperties();

            foreach (var info in propertyInfos)
            {
                var propertyInfo = rootType.GetType().GetProperty(info.Name);

                var attribute = propertyInfo.GetAttribute<AggregationAttribute>(false);

                if (attribute.IsNotNull())
                {
                    var rank = ((AggregationAttribute)attribute);

                    var childType = rootType.GetValue(propertyInfo.Name);

                    if (childType.IsNull() & !rank.AllowNull)
                    {
                        throw new ArgumentNullException("{0} cannot be null. Make sure you are including in your aggregate.".FormatInvariantCulture(propertyInfo.Name));
                    }

                    // if (alltypes.Where(t => t.Entity == childType).Count() == 0)
                    if (childType.IsNotNull())
                    {
                        if (rank.AggregationType == AggregationType.Shared)
                            alltypes.Add(new Aggregate() { DependancyOrder = rank.DependencyOrder, Entity = childType });

                        BuildDependencyList(childType);
                    }
                }
            }
        }



        private static void AddRootInstanceToTheList<T>(T type)
        {
            var attributes = type.GetType().GetCustomAttributes(false);
            var attribute = attributes.FirstOrDefault(a => a.GetType().Name == typeof(AggregationAttribute).Name);
            if (attribute.IsNotNull())
            {
                var value = ((AggregationAttribute)attribute).DependencyOrder;
                alltypes.Add(new Aggregate { DependancyOrder = value, Entity = type });
            }
        }
    }
    /// <summary>
    /// Instance is created when an entity is resolved.
    /// </summary>
    public class Aggregate
    {
        /// <summary>
        /// Describes the dependancy order of the entity.
        /// </summary>
        public int DependancyOrder { get; set; }

        /// <summary>
        /// Entity needs to be added.
        /// </summary>
        public object Entity { get; set; }
    }
}
