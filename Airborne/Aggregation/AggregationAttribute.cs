﻿using System;

namespace Airborne.Aggregation
{
    /// <summary>
    /// Entities are sorted and inserted based on the priority.
    /// e.g. FirstLevelDependency will be inserted/updated first, then comes SecondLevelDependency.
    /// IsIgnore is used to ignore the reflected property(entity) but child entities are considered. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public sealed class AggregationAttribute : Attribute
    {
        /// <summary>
        ///  Set AllowNull true if the entity is not mandatory.
        /// </summary>
        public bool AllowNull { get; private set; }
        /// <summary>
        /// Whether the type is Shared or composite
        /// </summary>
        public AggregationType AggregationType { get; private set; }
        /// <summary>
        /// The dependency order required for the entity
        /// </summary>
        public int DependencyOrder { get; private set; }

        /// <summary>
        /// Constructor for AggregationAttribute 
        /// </summary>
        public AggregationAttribute(AggregationType type, int dependencyOrder, bool allowNull = false)
        {
            AllowNull = allowNull;
           AggregationType = type;
           DependencyOrder = dependencyOrder;
        }
      
        /// <summary>
        /// Constructor for AggregationAttribute 
        /// </summary>
        public AggregationAttribute(AggregationType type, bool allowNull = false)
        {
            AggregationType = type;
            AllowNull = allowNull;
            if (type == AggregationType.Shared)
            {
                DependencyOrder = 1;
            }
        }
    }
}
