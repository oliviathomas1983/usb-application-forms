﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airborne.Caching
{
    /// <summary>
    /// Defines an empty cache provider. 
    /// Note this cache does nothing. It is a placement for a null object.
    /// </summary>
    public class EmptyCacheProvider : ICacheProvider
    {
        #region Implementation of ICacheProvider

        /// <summary>
        /// Adds the object to the cache using the specific key passed. Not really.
        /// </summary>
        public void Add(string key, object value)
        {

        }

        /// <summary>
        /// Add to the cache of a region. Not really.
        /// </summary>
        public void Add(string key, object value, string region)
        {

        }

        /// <summary>
        /// Saves the object to the cache using a built in key generation of type prefixing. Not really.
        /// </summary>
        public void Save<T>(object key, T value) where T : class
        {
        }

        /// <summary>
        /// Save an object to the cache. Not really.
        /// </summary>
        public void Save<T>(object key, T value, ICacheKeyGenerator keyGenerator) where T : class
        {
        }

        /// <summary>
        /// Save an object to the cache. Not really.
        /// </summary>
        public void Save<T>(object key, T value, ICacheKeyGenerator keyGenerator, string region) where T : class
        {
        }

        /// <summary>
        /// Get an object from the cache. Not really. Will always return null.
        /// </summary>
        public T Get<T>(object key) where T : class
        {
            return null;
        }

        /// <summary>
        /// Get an object from the cache. Not really. Will always return null.
        /// </summary>
        public T Get<T>(object key, string region) where T : class
        {
            return null;

        }

        /// <summary>
        /// Get an object from the cache. Not really. Will always return null.
        /// </summary>
        public T Get<T>(object key, ICacheKeyGenerator keyGenerator, string region) where T : class
        {
            return null;

        }

        /// <summary>
        /// Get an object from the cache. Not really. Will always return null.
        /// </summary>
        public T Get<T>(object key, Func<T> loader) where T : class
        {
            return null;

        }

        /// <summary>
        /// Get an object from the cache. Not really. Will always return null.
        /// </summary>
        public T Get<T>(object key, Func<T> loader, ICacheKeyGenerator keyGenerator) where T : class
        {
            return null;

        }

        /// <summary>
        /// Get an object from the cache. Not really. Will always return null.
        /// </summary>
        public T Get<T>(object key, Func<T> loader, ICacheKeyGenerator keyGenerator, string region) where T : class
        {
            return null;

        }

        /// <summary>
        /// Delete from the cache. Not really.
        /// </summary>
        public void Evict<T>(object key)
        {

        }

        /// <summary>
        /// Delete from the cache. Not really.
        /// </summary>
        public void Evict<T>(object key, ICacheKeyGenerator keyGenerator)
        {
        }

        /// <summary>
        /// Clear the cache. Not really.
        /// </summary>
        public void Clear()
        {
        }

        /// <summary>
        /// Gets the available state of the cache provider . eg. online. always returns true.
        /// </summary>
        public bool IsAvailable
        {
            get { return true; }
        }

        /// <summary>
        /// Provide a specific serialisation strategy for a specified type.
        /// </summary>
        /// <param name="cachingStrategy"></param>
        /// <param name="type"> </param>
        public void AddSerialisationStrategy(Type type, ISerialisationStrategy cachingStrategy)
        {
        }

        #endregion
    }
}
