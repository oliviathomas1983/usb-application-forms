﻿using System;
using System.Collections.Generic;

namespace Airborne.Caching
{

    /// <summary>
    /// 
    /// </summary>
    public interface ICacheProvider
    {

        /// <summary>
        /// Adds the object to the cache using the specific key passed
        /// </summary>
        void Add(string key, object value);

        /// <summary>
        /// Add to the cache of a region.
        /// </summary>
        void Add(string key, object value, string region);

        
        /// <summary>
        /// Saves the object to the cache using a built in key generation of type prefixing
        /// </summary>
        void Save<T>(object key, T value) where T : class;

        /// <summary>
        /// Save an object to the cache.
        /// </summary>
        void Save<T>(object key, T value, ICacheKeyGenerator keyGenerator) where T : class;

        /// <summary>
        /// Save an object to the cache.
        /// </summary>
        void Save<T>(object key, T value, ICacheKeyGenerator keyGenerator, string region) where T : class;

        /// <summary>
        /// Get an object from the cache.
        /// </summary>
        T Get<T>(object key) where T : class;

        /// <summary>
        /// Get an object from the cache.
        /// </summary>
        T Get<T>(object key, string region) where T : class;

        /// <summary>
        /// Get an object from the cache.
        /// </summary>
        T Get<T>(object key, ICacheKeyGenerator keyGenerator, string region) where T : class;

        /// <summary>
        /// Get an object from the cache.
        /// </summary>
        T Get<T>(object key, Func<T> loader) where T : class;

        /// <summary>
        /// Get an object from the cache.
        /// </summary>
        T Get<T>(object key, Func<T> loader, ICacheKeyGenerator keyGenerator) where T : class;

        /// <summary>
        /// Get an object from the cache.
        /// </summary>
        T Get<T>(object key, Func<T> loader, ICacheKeyGenerator keyGenerator, string region) where T : class;

        /// <summary>
        /// Delete from the cache.
        /// </summary>
        void Evict<T>(object key);

        /// <summary>
        /// Delete from the cache.
        /// </summary>
        void Evict<T>(object key, ICacheKeyGenerator keyGenerator);

        /// <summary>
        /// Clear the cache.
        /// </summary>
        void Clear();

        /// <summary>
        /// Gets the available state of the cache provider . e.g.. online
        /// </summary>
        bool IsAvailable { get; }

        /// <summary>
        /// Provide a specific serialisation strategy for a specified type.
        /// </summary>
        /// <param name="cachingStrategy"></param>
        /// <param name="type"> </param>
        void AddSerialisationStrategy(Type type, ISerialisationStrategy cachingStrategy);
    }
}



