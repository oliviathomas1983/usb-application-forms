﻿using System.Collections.Generic;

namespace Airborne.Caching
{
    /// <summary>
    /// Defines the contractual obligation for a caching strategy
    /// </summary>
    public interface ICachingStrategy
    {
        /// <summary>
        /// Specification for the cache method
        /// </summary>
        /// <param name="instance">The object instance the caching strategy will inspect.</param>
        /// <param name="cache">The relevant cache provider the caching strategy will utilize as the cache store.</param>
        /// <param name="context">An optional context the caching strategy depends on.</param>
        void Cache(object instance, ICacheProvider cache, object context);
        
        /// <summary>
        /// Specification for the cache method with multiple instances
        /// </summary>
        /// <param name="instances">The object instance the caching strategy will inspect.</param>
        /// <param name="cache">The relevant cache provider the caching strategy will utilize as the cache store.</param>
        /// <param name="context">An optional context the caching strategy depends on.</param>
        void Cache<T>(IEnumerable<T> instances, ICacheProvider cache, object context);

        /// <summary>
        /// Removes items from the cache.
        /// </summary>
        /// <param name="instances">The collection of items which need to be removed from the cache.</param>
        /// <param name="cache">The relevant cache provider the caching strategy will utilize as the cache store.</param>
        /// <param name="context">An optional context the caching strategy depends on.</param>
        void RemoveFromCache<T>(IEnumerable<T> instances, ICacheProvider cache, object context);
    }
}
