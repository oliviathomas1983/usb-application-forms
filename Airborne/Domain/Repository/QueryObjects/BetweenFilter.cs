﻿
namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Filter to evaluate if a value exists between a lower and upper range
    /// </summary>
    public class BetweenFilter : Filter
    {

        #region Properties
        /// <summary>
        /// Gets or sets the property that the <see cref="SimpleFilter"/> will be applied to.
        /// </summary>
        /// <value>The property.</value>
        public string Property
        {
            get;
            private set;
        }


        /// <summary>
        /// Upper value for the <see cref="BetweenFilter"/>
        /// </summary>
        public object Upper
        {
            get;
            private set;
        }

        /// <summary>
        /// Lower value for the <see cref="BetweenFilter"/>
        /// </summary>
        public object Lower { 
            get; 
            private set; 
        }

        /// <summary>
        /// Sets the evaluation of the <see cref="BetweenFilter"/> to include the Upper and Lower bounds.
        /// </summary>
        public bool Inclusive
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="BetweenFilter"/> class.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="lower">The lower value.</param>
        /// <param name="upper">The upper value.</param>
        public BetweenFilter(string property, object lower, object upper)
        {
            Property = property;
            Lower = lower;
            Upper = upper;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BetweenFilter"/> class.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="lower">The lower value.</param>
        /// <param name="upper">The upper value.</param>
        /// <param name="inclusive">if set to <c>true</c> to include the <paramref name="lower"/> and <paramref name="upper"/> values in the result.</param>
        public BetweenFilter(string property, object lower, object upper, bool inclusive)
            :this( property, lower, upper)
        {
            Inclusive = inclusive;
        }
        #endregion
    }
}
