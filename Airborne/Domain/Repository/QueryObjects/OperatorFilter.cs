﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Filter based on one of the <see cref="OperatorType"/>s
    /// </summary>
    public class OperatorFilter : Filter
    {
        #region Properties
        /// <summary>
        /// Gets or sets the <see cref="Filter"/> to be evaluated as the left side filter.
        /// </summary>
        /// <value>The left <see cref="Filter"/>.</value>
        public Filter Left
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the <see cref="Filter"/> to be evaluated as the right side filter.
        /// </summary>
        /// <value>The right <see cref="Filter"/>.</value>
        public Filter Right
        {
            get;
            private set;
        }

        /// <summary>
        /// The operator that is to be applied to the Left and Right filters
        /// </summary>
        public OperatorType Operator
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="OperatorFilter"/> class.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <param name="operator">The @operator.</param>
        public OperatorFilter(Filter left, Filter right, OperatorType @operator)
        {
            Operator = @operator;
            Left = left;
            Right = right;
        }
        #endregion

    }
}
