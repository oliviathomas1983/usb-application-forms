﻿
namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// A <see cref="Filter"/> with a operator that 
    /// will be resolved against a property in an expression tree.
    /// </summary>
    public abstract class PropertyFilter : Filter
    {
        #region Properties
        /// <summary>
        /// Gets or sets the property that the <see cref="SimpleFilter"/> will be applied to.
        /// </summary>
        /// <value>The property.</value>
        public string Property
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the operator used to determine how the Property and Value will be evaluated.
        /// </summary>
        /// <value>The operator.</value>
        public FilterType Operator
        {
            get;
            private set;
        }
        #endregion

         #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Filter"/> class.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="operator">The @operator.</param>
        protected PropertyFilter(string property, FilterType @operator)
        {
            Guard.ArgumentNotEmpty(property, "property");
            Property = property;
            Operator = @operator;

        }

        #endregion

    }
}
