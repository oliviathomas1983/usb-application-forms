﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airborne.Domain.Repository.QueryObjects
{

    /// <summary>
    /// This class is to be used in conjunction with <see cref="Criteria"/>. 
    /// Use this to express sorting requirements.
    /// <example>
    /// To sort on the "Name" property descending:
    /// <code>
    ///  Sort.By("Name").Desc() 
    /// </code>
    /// </example>
    /// </summary>
    public class Sort
    {

        #region Properties
        /// <summary>
        /// Property that the sort wil be based on.
        /// </summary>
        /// <value>The property.</value>
        public string Property { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Sort"/> is to be ordered ascending.
        /// </summary>
        /// <value><c>true</c> if ascending; otherwise, <c>false</c>.</value>
        public bool Ascending { get; set; }

        #endregion


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Sort"/> class.
        /// </summary>
        /// <param name="property">The property.</param>
        public Sort(string property)
            : this(property, true)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sort"/> class.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="ascending">if set to <c>true</c> [ascending].</param>
        public Sort(string property, bool ascending)
        {

            Guard.ArgumentNotEmpty(property, "property");
            Property = property;
            Ascending = ascending;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Sets the sort to be ascending
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Asc", Justification = "This is a well known term in the querying space")]
        public Sort Asc()
        {
            Ascending = true;
            return this;
        }

        /// <summary>
        /// Sets the sort to be Descending
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Desc", Justification = "This is a well known term in the querying space")]
        public Sort Desc()
        {
            Ascending = false;
            return this;
        }
        #endregion

        #region Fluent Methods

        /// <summary>
        /// Creates a <see cref="Sort"/> based on the <paramref name="property"/>
        /// </summary>
        /// <param name="property">The property that the <see cref="Sort"/> will be based on.</param>
        /// <returns></returns>
        public static Sort By(string property)
        {
            return new Sort(property);
        }

        #endregion
    }
}
