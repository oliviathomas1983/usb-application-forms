﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Airborne.Domain.Security
{

    /// <summary>
    /// A grouping of tasks that can be assigned to a user
    /// </summary>
    [DataContract]
    public class Role : IAggregate
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Role"/> class.
        /// </summary>
        public Role()
        {
            Tasks = new List<RoleTaskAssignment>();
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>The display name.</value>
        [DataMember]
        public string DisplayName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is hidden.
        /// </summary>
        /// <value><c>true</c> if this instance is hidden; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsHidden
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        [DataMember]
        public int Version { get; set; }



        /// <summary>
        /// Gets or sets the security tasks.
        /// </summary>
        /// <value>The tasks.</value>
        [DataMember]
        public IList<RoleTaskAssignment> Tasks
        {
            get;
            set;
        }

        #region Nested type: Descriptors

        /// <summary>
        /// Descriptors
        /// </summary>
        public static class Descriptors
        {
            /// <summary>
            /// Id property name
            /// </summary>
            public static string Id = "Id";

            /// <summary>
            /// IsHidden property name
            /// </summary>
            public static string IsHidden = "IsHidden";

            /// <summary>
            /// Name property name
            /// </summary>
            public static string Name = "Name";
        }

        #endregion

    }
}
