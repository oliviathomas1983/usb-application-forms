﻿using System;
using System.Runtime.Serialization;

namespace Airborne.Domain.Security
{
    /// <summary>
    /// Security task
    /// </summary>
    [DataContract]
    public class SecurityTask : IAggregate
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        #region Nested type: Descriptors

        /// <summary>
        /// Descriptors
        /// </summary>
        public static class Descriptors
        {
            /// <summary>
            /// Id property name
            /// </summary>
            public static string Id = "Id";
            
            /// <summary>
            /// Name property name
            /// </summary>
            public static string Name = "Name";
        }

        #endregion

        #region Equality
        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            SecurityTask function = obj as SecurityTask;
            if (obj.IsNotNull())
            {
                return function.Id.Equals(this.Id); 
            }
            return base.Equals(obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
        #endregion
    }
}
