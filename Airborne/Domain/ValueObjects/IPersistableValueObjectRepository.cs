﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airborne.Domain.ValueObjects
{
   /// <summary>
   /// Contract for a value object repository that can be updated and saved.
   /// </summary>
   public interface IPersistableValueObjectRepository : IValueObjectRepository
   {
      /// <summary>
      /// Save changes made to a value object
      /// </summary>
      void Save(ValueObject valueObject);

      /// <summary>
      /// Delete a value object from the store.
      /// </summary>
      void Delete(ValueObject valueObject);
   }
}
