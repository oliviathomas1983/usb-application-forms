using System;

namespace Airborne.Domain.ValueObjects
{
    /// <summary>
    /// Contract for the value object repository.
    /// <see  cref="Airborne.Domain.ValueObjects.ValueObject"/>
    /// </summary>
    public interface IValueObjectRepository : IDisposable
    {
        /// <summary>
        /// Find a value object based on the code.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "There is a method that does not require explicit type parameter")]
        T Find<T>(string code) where T : ValueObject;

        /// <summary>
        /// Finds a value object instance based on <paramref name="type"/>
        /// and the <paramref name="code"/>
        /// </summary>
        ValueObject Find(Type type, string code);


        /// <summary>
        /// Find all the value objects for <typeparam name="T"/>
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "There is a method that does not require explicit type parameter")]
        T[] FindAll<T>() where T : ValueObject;

        /// <summary>
        /// Find all the value objects for <see cref="Type"/>
        /// </summary>
        ValueObject[] FindAll(Type type);
    }
}
