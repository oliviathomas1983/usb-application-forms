﻿using System.Collections.Generic;
using System.Linq;

namespace System.Reflection
{
    /// <summary>
    /// Additional extensions on <see cref="System.Reflection.Assembly"/>
    /// </summary>
    public static class AssemblyExtensions
    {

        /// <summary>
        /// Returns all concrete types that assignable from T 
        /// </summary>
        public static IEnumerable<Type> GetTypesOf<T>(this Assembly instance) where T : class
        {
            var contract = typeof(T);

            var types = from t in instance.GetTypes()
                        where contract.IsAssignableFrom(t) && !t.IsAbstract
                        select t;

            return types.ToList();
        }

    }
}
