﻿using System.Globalization;

namespace System
{
    /// <summary>
    /// Additional extensions on <see cref="System.DateTime"/>
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Returns if the date is today's date
        /// </summary>
        public static bool IsToday(this DateTime value)
        {
            return (value.Date == DateTime.Today.Date);
        }

        /// <summary>
        /// Indicates if the date compared against the comparer is in the past
        /// </summary>
        public static bool IsInPast(this DateTime value, DateTime compare)
        {
            return value < compare;
        }

        /// <summary>
        /// Indicates if the date compared against the comparer is in the future
        /// </summary>
        public static bool IsInFuture(this DateTime value, DateTime compare)
        {
            return value > compare;
        }

        /// <summary>
        /// Indicates if the date compared against today's date is in the future
        /// </summary>
        public static bool IsInFuture(this DateTime value)
        {
            return value.IsInFuture(DateTime.Now.Date);
        }

        /// <summary>
        /// Returns the specified date decorated with the time information of now
        /// </summary>
        public static DateTime RightNow(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, value.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }

        /// <summary>
        /// Returns the length in months between the specified dates.
        /// </summary>
        /// <param name="instance">The date instance</param>
        /// <param name="date">The target date to query against</param>
        /// <returns>int</returns>
        public static int MonthsApart(this DateTime instance, DateTime date)
        {
            var monthsApart = 12 * (instance.Year - date.Year) + instance.Month - date.Month;
            return Math.Abs(monthsApart);
        }

        /// <summary>
        /// Returns the length in weeks between the specified dates.
        /// </summary>
        /// <param name="instance">The date instance</param>
        /// <param name="date">The target date to query against</param>
        /// <returns>int</returns>
        public static int WeeksApart(this DateTime instance, DateTime date)
        {
            var tt = instance - date;
            return Math.Abs(tt.Days / 7);
        }

        /// <summary>
        /// Returns a date time with only year, month, day , hours , min and seconds.
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static DateTime AsShortDateTime(this DateTime instance)
        {
            return new DateTime(instance.Year, instance.Month, instance.Day, instance.Hour, instance.Minute, instance.Second);
        }

        /// <summary>
        /// Returns the length in days between the specified dates.
        /// </summary>
        //public static int DaysApart(this DateTime instance, DateTime date)
        public static int DaysApart(this DateTime instance, DateTime date)
        {
            var tt = instance - date;

            //return Math.Round(tt.TotalDays, 2);
            return Math.Abs(tt.Days);
        }

        /// <summary>
        /// Determines if the date instance falls between <paramref name="date1"/> and <paramref name="date2"/>
        /// </summary>
        public static bool Between(this DateTime value, DateTime date1, DateTime date2)
        {
            var minDate = date1;
            var maxDate = date2;

            if (date2 < date1)
            {
                minDate = date2;
                maxDate = date1;
            }

            return (value >= minDate && value <= maxDate);
        }

        /// <summary>
        /// Returns the end date time of the day provided.
        /// </summary>
        /// <example>
        /// For the date 1 Jan 2010 it will return a date instance of 1 Jan 2010 23H 59M 59s
        /// </example>
        public static DateTime EndOfDay(this DateTime value)
        {
            return value.StartOfDay().AddHours(23).AddMinutes(59).AddSeconds(59);
        }

        /// <summary>
        /// Returns the start date time of the day provided.
        /// </summary>
        /// <example>
        /// For the date 1 Jan 2010 it will return a date instance of 1 Jan 2010 0H 0M 0s
        /// </example>
        public static DateTime StartOfDay(this DateTime value)
        {
            return value.Date;
        }

        /// <summary>
        /// Returns the date of the next <paramref name="day"/> after the <paramref name="value"/> provided.
        /// </summary>
        public static DateTime Next(this DateTime value, DayOfWeek day)
        {
            var num = (day - value.DayOfWeek);
            if (num <= 0)
            {
                num += 7;
            }
            return value.AddDays(num);
        }

        /// <summary>
        /// Returns the start of week i think!
        /// </summary>
        public static DateTime StartOfWeek(this DateTime value, DayOfWeek day)
        {
            var diff = value.DayOfWeek - day;
            if (diff < 0)
            {
                diff += 7;
            }

            return value.AddDays(-1 * diff);
        }

        /// <summary>
        /// Returns the end of the week
        /// </summary>
        public static DateTime EndOfWeek(this DateTime value, DayOfWeek day)
        {
            return (value.DayOfWeek == day) ? value : value.Next(day);
        }

        /// <summary>
        /// Returns the start of the month.
        /// </summary>
        public static DateTime StartOfMonth(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, 1);
        }

        /// <summary>
        /// Gets the starting Monday if the month.
        /// </summary>
        public static DateTime StartingMondayOfMonth(this DateTime value)
        {
            return value.StartOfMonth().StartOfWeek(DayOfWeek.Monday).StartOfDay();
        }

        /// <summary>
        /// Gets the ending Sunday of the month.
        /// </summary>
        public static DateTime EndingSundayOfMonth(this DateTime value)
        {
            return value.EndOfMonth().EndOfWeek(DayOfWeek.Sunday).EndOfDay();
        }

        /// <summary>
        /// Returns if a date is within the same month as a date that is passed in.
        /// </summary>
        /// <param name="date">date to compare</param>
        /// <param name="month">month to compare to</param>
        /// <returns></returns>
        public static bool IsInThisMonth(this DateTime date, DateTime month)
        {
            return month.Month == date.Month && month.Year == date.Year;
        }

        /// <summary>
        /// Returns true or false if the date is within this month
        /// </summary>
        public static bool IsThisMonth(this DateTime date)
        {
            return date.Month == DateTime.Now.Month
                    && date.Year == DateTime.Now.Year;
        }

        /// <summary>
        /// Returns the end of the month
        /// </summary>
        public static DateTime EndOfMonth(this DateTime value)
        {
            var start = value.StartOfMonth();
            return start.AddMonths(1).AddDays(-1);
        }

        /// <summary>
        /// Returns the month name for the specified date.
        /// </summary>
        public static string GetMonthName(this DateTime value, bool includeYear = true)
        {
            var monthName = "{0} {1}".FormatInvariantCulture(value.ToString("MMMM", CultureInfo.CurrentCulture), includeYear ? value.ToString("yyyy", CultureInfo.CurrentCulture) : string.Empty);

            return monthName.Trim();
        }

        /// <summary>
        /// Converts a date to a friendly readable format.
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static string CasualDateDiffHint(this DateTime instance)
        {
            //mins
            var numberOfMinsAgo = (int)Math.Abs((instance - DateTime.Now).TotalMinutes);

            if (numberOfMinsAgo == 0)
            {
                return "a few seconds ago";
            }

            if (numberOfMinsAgo.Between(1, 2))
            {
                return "a few minutes ago";
            }

            if (numberOfMinsAgo.Between(2, 28) || numberOfMinsAgo.Between(41, 59))
            {
                return numberOfMinsAgo + " minutes ago";
            }

            if (numberOfMinsAgo.Between(29, 40))
            {
                return "1/2 hour ago";
            }

            //hours
            var numberOfHoursAgo = (int)Math.Abs((instance - DateTime.Now).TotalHours);
            if (numberOfHoursAgo == 1)
            {
                return "about an hour ago";
            }

            if (numberOfHoursAgo.Between(2, 23))
            {
                return numberOfHoursAgo + " hours ago";
            }

            //days
            var numberOfDaysAgo = Math.Floor(Math.Abs((instance - DateTime.Now).TotalDays));
            if (numberOfDaysAgo < 2)
            {
                return "1 day ago";
            }

            if (numberOfDaysAgo >= 2 && numberOfDaysAgo <= 7)
            {
                return numberOfDaysAgo + " days ago";
            }

            //weeks
            if (numberOfDaysAgo > 7 && numberOfDaysAgo <= 14)
            {
                return "last week " + instance.DayOfWeek;
            }

            if (numberOfDaysAgo > 14 && numberOfDaysAgo <= 21)
            {
                return "about 2 weeks ago on " + instance.DayOfWeek;
            }

            if (numberOfDaysAgo > 21 && numberOfDaysAgo <= 29)
            {
                return "about 3 weeks ago on " + instance.DayOfWeek;
            }

            //months
            if (numberOfDaysAgo >= 30 && numberOfDaysAgo < 365)
            {
                var months = Math.Floor(numberOfDaysAgo / 30);

                return months == 1 ? "about  " + months + " month ago" : "about  " + months + " months ago";
            }

            // years ..
            return "over a year ago";
        }

        /// <summary>
        /// Returns the week of year for the specified date
        /// </summary>
        public static int WeekOfYear(this DateTime date)
        {
            //return (int)Math.Ceiling((date - new DateTime(date.Year, 1, 1)).TotalDays / 7);
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        }

        /// <summary>
        /// Returns the week of year for the specified date with the specified start day
        /// </summary>
        public static int WeekOfYear(this DateTime date, DayOfWeek start)
        {
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, start);
        }

        /// <summary>
        /// Returns the TimeSpan until the end of the given date e.g. If today is 19 July 2012, this
        /// will return 19 July 2012 23:59:59.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static TimeSpan TimeUntilEndOfDay(this DateTime date)
        {
            return (date.EndOfDay() - DateTime.Now);
        }

        /// <summary>
        /// Returns the date as a formatted string such as "Mon Jul 9, 2012 at 10:13 am"
        /// </summary>
        public static string ToLongDateAndTimeString(this DateTime date)
        {
            var day = date.ToString("ddd", CultureInfo.InvariantCulture);
            var month = date.ToString("MMM", CultureInfo.InvariantCulture);
            var dayDate = date.Day;
            var year = date.Year;

            var time = date.ToShortTimeString().ToLower(CultureInfo.InvariantCulture);

            return "{0} {1} {2}, {3} at {4}".FormatInvariantCulture(day, month, dayDate, year, time);
        }
    }
}
