﻿using System.ComponentModel;
using System.Diagnostics;

namespace System
{
    /// <summary>
    /// Additional extensions on <see cref="System.String"/>
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        ///Returns the enum description attribute
        /// </summary>
        public static string GetDescription(this Enum value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());

            var attribute = fieldInfo.IsNotNull() ? (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false) : null;

            if (attribute.IsNotNull() && attribute.Length > 0)
            {
                return attribute[0].Description;
            }
            return value.ToString();
        }
    }
}