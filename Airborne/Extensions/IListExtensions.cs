﻿using System;
using System.Linq;
using System.Collections.Generic;


namespace System.Collections.Generic
{
    /// <summary>
    /// Additional extensions on System.Collections.Generic.IList`1
    /// </summary>
    public static class IListExtensions
    {
        /// <summary>
        /// Removes all items that matches the expression
        /// </summary>
        public static int RemoveAll<T>(this IList<T> list, Func<T, bool> expression)
        {
            var count = 0;
            if (list.IsNotNull())
            {
                foreach (T item in list.Where(expression).ToList())
                {
                    list.Remove(item);
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Applies the action to each item in the list
        /// </summary>
        public static void ForEach<T>(this IList<T> list, Action<T> action)
        {
            list.ToList<T>().ForEach(action);
        }

        /// <summary>
        /// Returns true if the list contains items
        /// </summary>
        public static bool HasValues<T>(this IList<T> list)
        {
            return list.Count() > 0;
        }

        /// <summary>
        /// Returns random item from the list
        /// </summary>
        public static T GetAnyoneFromTheList<T>(this List<T> list)
        {
            var rndElement = new Random();
            if (list.Count > 1)
            {
                var index = rndElement.Next(0, list.Count);
                return list[index];
              
            }
            throw new ArgumentException(list.GetType().FullName + " does not contain values");
        }
    }
}
