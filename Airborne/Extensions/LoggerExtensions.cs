﻿using System;

namespace Airborne.Logging
{
    /// <summary>
    /// Additional extensions for logging functionality
    /// </summary>
    public static class LoggerExtensions
    {
        /// <summary>
        /// Extends the Exception type for logger message formatting
        /// </summary>
        public static string AsLogMessage(this Exception exception, string message)
        {
            return "{0}{1}{2}".FormatInvariantCulture(message, Environment.NewLine, exception.ToString());
        }

        /// <summary>
        /// Extends the Exception type for logger message formatting
        /// </summary>
        public static string AsLogMessage(this Exception exception, string message, object data)
        {
            var msg = exception.AsLogMessage(message);
            return "{0}{1}{2}{3}{4}".FormatInvariantCulture(msg, Environment.NewLine, "-".Repeat(47), Environment.NewLine, data);
        }
    }
}