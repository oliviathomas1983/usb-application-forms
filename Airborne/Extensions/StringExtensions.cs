﻿using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace System
{
    /// <summary>
    /// Additional extensions on <see cref="System.String"/>
    /// </summary>
    [DebuggerStepThrough]
    public static class StringExtensions
    {
        /// <summary>
        ///Returns the boolean check of whether a string can be considered as an email address.
        /// </summary>
        public static bool ValidateEmailAddress(this string value)
        {
            if (value.IsNullOrEmpty())
            {
                return false;
            }
            var emailExpression = @"^(\w+([-+.']\w+)*)@([\w-]+\.)+[\w-]+$";
            return Regex.Match(value, emailExpression).Success;
        }

        /// <summary>
        ///Returns the boolean check of whether a string can be considered as an email address.
        /// </summary>
        public static bool IsEmailAddress(this string value)
        {
            return ValidateEmailAddress(value);
        }

        /// <summary>
        /// Limits this string to a given length and applies an ellipse
        /// </summary>
        public static string Ellipse(this string input, int length)
        {
            if (input.IsNotNull())
            {
                return input.Length > length ? input.Substring(0, length) + "..." : input;
            }

            return string.Empty;
        }

        /// <summary>
        /// Removes all leading and training white-space characters from the System.String object.
        /// Safe to call on null or empty instances as trimming will be ignored for these instances.
        /// </summary>
        public static string SafeTrim(this string input)
        {
            if (input.IsNotNullOrEmpty())
            {
                return input.Trim();
            }
            return input;
        }

        /// <summary>
        /// Formats this string with the CurrentCulture passing in the specified parameters
        /// </summary>
        public static string FormatCurrentCulture(this String format, params object[] values)
        {
            return string.Format(CultureInfo.CurrentCulture, format, values);
        }

        /// <summary>
        /// Formats this string with the InvariantCulture passing in the specified parameters
        /// </summary>
        public static string FormatInvariantCulture(this String format, params object[] values)
        {
            return string.Format(CultureInfo.InvariantCulture, format, values);
        }

        /// <summary>
        /// Indicates whether this string is null or empty
        /// </summary>
        public static bool IsNullOrEmpty(this String value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Indicates whether this string is not null or empty
        /// </summary>
        public static bool IsNotNullOrEmpty(this String value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Does a Left operation on this string
        /// </summary>
        public static string Left(this string value, int length)
        {
            if (value.IsNotNullOrEmpty())
            {
                if (value.Length <= length)
                {
                    return value;
                }
                return value.Substring(0, length);
            }
            return string.Empty;
        }

        /// <summary>
        /// Does a Right operation on this string
        /// </summary>
        public static string Right(this string value, int length)
        {
            if (value.IsNotNullOrEmpty())
            {
                if (value.Length <= length)
                {
                    return value;
                }
                return value.Substring(value.Length - length);
            }
            return string.Empty;
        }

        /// <summary>
        /// Strips all non alphanumeric characters from the string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string RemoveAllNonAlphanumericChars(this string value)
        {
            return Regex.Replace(value, @"\W*", "");
        }

        /// <summary>
        /// Strips spaces from string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string RemoveSpaces(this string value)
        {
            return value.Replace(" ", string.Empty);
        }

        /// <summary>
        /// Formats a CamelCased string to words. e.g Camel Cased
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string CamelCaseToWords(this string value)
        {
            if (value.IsNullOrEmpty())
            {
                return string.Empty;
            }

            var words = Regex.Split(value, @"(?<!^)(?<=[a-z])(?=[A-Z]+)");

            var result = new StringBuilder();
            words.ToList().ForEach(w => result.Append("{0} ".FormatInvariantCulture(w.Trim())));

            return result.ToString().Trim();
        }

        /// <summary>
        /// Removes occurrences of characters, provided in the array
        /// </summary>
        public static string Remove(this string value, char[] exclusions)
        {
            if (value.IsNotNullOrEmpty())
            {
                var builder = new StringBuilder(value);

                var characterIndex = value.IndexOfAny(exclusions, 0);
                while (characterIndex != -1)
                {
                    builder.Remove(characterIndex, 1);
                    characterIndex = builder.ToString().IndexOfAny(exclusions, 0);
                }
                return builder.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Returns only the Numeric digits in a string, by stripping out all non
        /// numeric characters, except those characters in the exclusion list
        /// </summary>
        public static string RemoveText(this string value, char[] exclusions)
        {
            return RemoveDigitOrText(value, true, exclusions);
        }

        /// <summary>
        /// Returns only the Numeric digits in a string
        /// </summary>
        public static string RemoveText(this string value)
        {
            return RemoveDigitOrText(value, true, null);
        }

        /// <summary>
        /// Removes all the numeric digits from a string, except those in the exclusion list
        /// </summary>
        public static string RemoveDigits(this string value, char[] exclusions)
        {
            return RemoveDigitOrText(value, false, exclusions);
        }

        /// <summary>
        /// Removes all the numeric digits from a string
        /// </summary>
        public static string RemoveDigits(this string value)
        {
            return RemoveDigitOrText(value, false, null);
        }

        /// <summary>
        /// Returns only the Numeric digits in a string when bool returnDigit is set to False
        /// Removes all the numeric digits from a string when bool returnDigit is set to True
        /// </summary>
        private static string RemoveDigitOrText(string value, bool removeDigit, char[] exclusions)
        {
            string returnValue = string.Empty;
            bool shouldRemove;
            foreach (char character in value.ToCharArray())
            {
                shouldRemove = removeDigit;
                if (exclusions.IsNotNull() && exclusions.Contains(character))
                {
                    shouldRemove = !removeDigit;
                }
                if (char.IsDigit(character) == shouldRemove)
                {
                    returnValue += character.ToString();
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Repeats the specified string. E.g "x".Repeat(3) = "xxx". nice.
        /// </summary>
        public static string Repeat(this string value, int times)
        {
            var copy = string.Empty;

            for (var x = 0; x < times; x++)
            {
                copy += value;
            }

            return copy;
        }

        /// <summary>
        /// Performs a case insensitive invariant culture comparison of two strings.
        /// </summary>
        public static bool IsSameAs(this string value, string target)
        {
            return string.Compare(value, target, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Performs a case insensitive invariant culture comparison of two strings. Trims leading and training whitespace if any.
        /// </summary>
        public static bool IsSameAs(this string value, string target, bool trim)
        {
            if (trim)
            {
                if (value.IsNotNullOrEmpty() && target.IsNotNullOrEmpty())
                {
                    return value.Trim().IsSameAs(target.Trim());
                }
            }

            return value.IsSameAs(target);
        }

        /// <summary>
        /// Performs a case insensitive invariant culture comparison of two strings.
        /// </summary>
        public static bool IsSameAsViaReflection(this string value, string target)
        {
            return string.Compare(value, target, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Performs a case insensitive invariant culture comparison of two strings.
        /// </summary>
        public static bool IsNotSameAs(this string value, string target)
        {
            return string.Compare(value, target, StringComparison.OrdinalIgnoreCase) != 0;
        }

        /// <summary>
        /// Converts the first letter of the first word in a phrase to uppercase
        /// </summary>
        public static string ConvertFirstLetterOfPhraseToUppercase(this string value)
        {
            if (value.IsNotNullOrEmpty())
            {
                var convertedString = value.ToLowerInvariant();

                return convertedString[0].ToString().ToUpperInvariant() + convertedString.Substring(1);
            }
            return string.Empty;
        }

        /// <summary>
        /// Strips html tags
        /// </summary>
        public static string AsPlainText(this string value)
        {
            if (value.IsNotNullOrEmpty())
            {
                var htmlTagPattern = @"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>";
                var removeScriptBlockPattern = @"(?m)<script\b[^>]*>\n*\s\n?.*?</script>";
                var removeStyleBlockPattern = @"(?m)<style\b[^>]*>\n*\s\n?.*?</style>";

                var scriptBlockRemoved = Regex.Replace(value, removeScriptBlockPattern, string.Empty, RegexOptions.Singleline);
                var styleBlockRemoved = Regex.Replace(scriptBlockRemoved, removeStyleBlockPattern, string.Empty, RegexOptions.Singleline);
                var htmlRemoved = Regex.Replace(styleBlockRemoved, htmlTagPattern, string.Empty);

                return htmlRemoved;
            }

            return string.Empty;
        }

        /// <summary>
        /// Indicates whether this string is a number
        /// </summary>
        public static bool IsNumber(this String value)
        {
            int number;
            return int.TryParse(value, out number);
        }

        /// <summary>
        /// Indicates whether value1 contains value2 with case insensitive.
        /// </summary>
        public static bool Like(this string value1, string value2)
        {
            return value1.ToLowerInvariant().Contains(value2.ToLowerInvariant());
        }

        /// <summary>
        /// Returns a lowercase string with no spaces or special characters
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToLowerWithNoSpacesOrSpecialCharacters(this string value)
        {
            return value.Replace(" ", "").Replace("'", "").Replace("&", "").ToLowerInvariant();
        }

#if !SILVERLIGHT

        /// <summary>
        /// Returns an object of type T deserialized from a XML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T Deserialize<T>(this string xml) where T : new()
        {
            if (xml.IsNullOrEmpty())
            {
                return new T();
            }
            using (var memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(xml)))
            {
                using (var reader = XmlDictionaryReader.CreateTextReader(memoryStream, Encoding.Unicode, new XmlDictionaryReaderQuotas(), null))
                {
                    var dataContractSerializer = new DataContractSerializer(typeof(T));
                    var deserialized = (T)dataContractSerializer.ReadObject(reader);
                    if (deserialized.IsNull())
                    {
                        return new T();
                    }
                    return deserialized;
                }
            }
        }

        /// <summary>
        /// Provides the ability to compute a MD5 hash
        /// </summary>
        public static string ComputeHash(this string value)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                var originalBytes = ASCIIEncoding.Default.GetBytes(value);
                var encodedBytes = md5.ComputeHash(originalBytes);
                return BitConverter.ToString(encodedBytes);
            }
        }

#endif
    }
}