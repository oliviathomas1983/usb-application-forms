
/*  
http://mvccontrib.codeplex.com/
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Airborne.Pagination
{
	/// <summary>
	/// Implementation of IPagination that wraps a pre-paged data source. 
	/// </summary>
	public class CustomPaginationCollection<T> : IPaginationCollection<T>
	{
		private readonly IList<T> _dataSource;

		/// <summary>
		/// Creates a new instance of CustomPagination
		/// </summary>
		/// <param name="dataSource">A pre-paged slice of data</param>
		/// <param name="pageNumber">The current page number</param>
		/// <param name="pageSize">The number of items per page</param>
		/// <param name="totalItems">The total number of items in the overall data source</param>
		public CustomPaginationCollection(IEnumerable<T> dataSource, int pageNumber, int pageSize, int totalItems)
		{
			_dataSource = dataSource.ToList();
			PageNumber = pageNumber;
			PageSize = pageSize;
			TotalItems = totalItems;
		}

        /// <summary>
        /// Returns the data source enumerator
        /// </summary>
		public IEnumerator<T> GetEnumerator()
		{
			return _dataSource.GetEnumerator();
		}
         
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

        /// <summary>
        /// Gets the page number
        /// </summary>
		public int PageNumber { get; private set; }


        /// <summary>
        /// Gets the page size
        /// </summary>
		public int PageSize { get; private set; }
        
        /// <summary>
        /// Gets the total items
        /// </summary>
        public int TotalItems { get; private set; }

        /// <summary>
        /// Gets the total page count
        /// </summary>
        public int TotalPages
        {
            get { return CalculateTotalPages(TotalItems, PageSize); }
        }

        /// <summary>
        /// Calculates the total pages based on the specified item count and page size.
        /// </summary>
        public static int CalculateTotalPages(int totalItems, int pageSize)
        {
            return (int)Math.Ceiling(((double)totalItems) / pageSize);
        }
        
        /// <summary>
        /// Gets the first item's index based on the page size and current page number
        /// </summary>
		public int FirstItem
		{
			get
			{
				return ((PageNumber - 1) * PageSize) + 1;
			}
		}

        /// <summary>
        /// Gets the last item's index based on the page size and current page number
        /// </summary>
		public int LastItem
		{
			get { return FirstItem + _dataSource.Count - 1; }
		}

        /// <summary>
        /// Gets whether the pagination has a previous page.
        /// </summary>
		public bool HasPreviousPage
		{
			get { return PageNumber > 1; }
		}

        /// <summary>
        /// Gets whether the pagination has another page.
        /// </summary>
		public bool HasNextPage
		{
			get { return PageNumber < TotalPages; }
		}
	}
}