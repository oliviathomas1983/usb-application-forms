/*  
http://mvccontrib.codeplex.com/
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Airborne.Pagination
{
	/// <summary>
	/// Executes an IQueryable in order to created a paged set of objects.
	/// The query is not executed until the LazyPagination is enumerated or one of its properties is invoked.
	/// The results of the query are cached.
	/// </summary>
	/// <typeparam name="T">Type of objects in the collection.</typeparam>
	public class LazyPaginationCollection<T> : IPaginationCollection<T>
	{
		/// <summary>
		/// Default page size.
		/// </summary>
		private IList<T> results;
		private int totalItems;

        /// <summary>
        /// Gets the page size.
        /// </summary>
		public int PageSize { get; private set; }

		/// <summary>
		/// Gets the query to execute.
		/// </summary>
		public IQueryable<T> Query { get; protected set; }
		
        /// <summary>
        /// Gets the page number.
        /// </summary>
        public int PageNumber { get; private set; }


		/// <summary>
		/// Creates a new instance of the <see cref="LazyPaginationCollection{T}"/> class.
		/// </summary>
		public LazyPaginationCollection(IQueryable<T> query, int pageNumber, int pageSize)
		{
			PageNumber = pageNumber;
			PageSize = pageSize;
			Query = query;
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			TryExecuteQuery();


            foreach (var item in results)
            {
                yield return item;
            }
		}

		/// <summary>
		/// Executes the query if it has not already been executed.
		/// </summary>
		protected void TryExecuteQuery()
		{
			//Results is not null, means query has already been executed.
			if (results != null)
				return;

			totalItems = Query.Count();
			results = ExecuteQuery();
		}

		/// <summary>
		/// Calls Queryable.Skip/Take to perform the pagination.
		/// </summary>
		/// <returns>The paged set of results.</returns>
		protected virtual IList<T> ExecuteQuery()
		{
			int numberToSkip = (PageNumber - 1) * PageSize;
			return Query.Skip(numberToSkip).Take(PageSize).ToList();
		}

        /// <summary>
        /// Returns the paginations Enumerator
        /// </summary>
        /// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return ((IEnumerable<T>)this).GetEnumerator();
		}

        /// <summary>
        /// Gets the total items
        /// </summary>
		public int TotalItems
		{
			get
			{
				TryExecuteQuery();
				return totalItems;
			}
		}

        /// <summary>
        /// Gets the total pages.
        /// </summary>
		public int TotalPages
		{
			get { return (int)Math.Ceiling(((double)TotalItems) / PageSize); }
		}

        /// <summary>
        /// Gets the first item's index based on the page size and current page number
        /// </summary>
		public int FirstItem
		{
			get
			{
				TryExecuteQuery();
				return ((PageNumber - 1) * PageSize) + 1;
			}
		}

        /// <summary>
        /// Gets the last item's index based on the page size and current page number
        /// </summary>
		public int LastItem
		{
			get
			{
				return FirstItem + results.Count - 1;
			}
		}

        /// <summary>
        /// Gets whether the pagination has a previous page.
        /// </summary>
		public bool HasPreviousPage
		{
			get { return PageNumber > 1; }
		}

        /// <summary>
        /// Gets whether the pagination has another page.
        /// </summary>
		public bool HasNextPage
		{
			get { return PageNumber < TotalPages; }
		}
	}

}
