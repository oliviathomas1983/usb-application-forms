﻿/*  
http://mvccontrib.codeplex.com/
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Query;

namespace Airborne.Pagination
{
    /// <summary>
    /// Extension methods for creating paged lists.
    /// </summary>
    public static class PaginationExtensions
    {
        private const int DefaultPageSize = 20;

        /// <summary>
        /// Converts the specified IEnumerable into an IPagination using the default page size and returns the specified page number.
        /// </summary>
        /// <typeparam name="T">Type of object in the collection</typeparam>
        /// <param name="source">Source enumerable to convert to the paged list.</param>
        /// <param name="pageNumber">The page number to return.</param>
        /// <returns>An IPagination of T</returns>
        public static IPaginationCollection<T> AsPagination<T>(this IEnumerable<T> source, int pageNumber)
        {
            return source.AsPagination(pageNumber, DefaultPageSize);
        }

        /// <summary>
        /// Converts the specified IEnumerable into an IPagination using the specified page size and returns the specified page. 
        /// </summary>
        /// <typeparam name="T">Type of object in the collection</typeparam>
        /// <param name="source">Source enumerable to convert to the paged list.</param>
        /// <param name="pageNumber">The page number to return.</param>
        /// <param name="pageSize">Number of objects per page.</param>
        /// <returns>An IPagination of T</returns>
        public static IPaginationCollection<T> AsPagination<T>(this IEnumerable<T> source, int pageNumber, int pageSize)
        {
            //if (pageNumber < 1 )
            //{
            //    throw new ArgumentOutOfRangeException("pageNumber", "The page number should be greater than or equal to 1.");
            //}

            if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            return new LazyPaginationCollection<T>(source.AsQueryable(), pageNumber, pageSize);
        }

        /// <summary>
        /// Builds paging if pageNumber or pageSize is given.
        /// Converts to serializable QueryResult
        /// </summary>
        public static QueryResult<T> Page<T>(this IEnumerable<T> items, int? pageNumber, int? pageSize)
        {

            var hasPaging = pageNumber.IsNotNull() && pageSize.IsNotNull();
            if (hasPaging)
            {
                var queryResult = new QueryResult<T>
                                      {
                                          Results = items.AsPagination(pageNumber.Value, pageSize.Value).ToList(),
                                          TotalItems = items.Count(),
                                      };
                return queryResult;
            }

            return new QueryResult<T> { Results = items.ToList() };
        }
       
        /// <summary>
        /// Converts to results of QueryResult to pagination results so front end can understand it.
        /// </summary>
        public static QueryResult<T> Page<T>(this QueryResult<T> queryResult, int? pageNumber, int? pageSize)
        {
            var hasPaging = pageNumber.IsNotNull() && pageSize.IsNotNull();
            if (hasPaging)
            {
                queryResult.Results = new CustomPaginationCollection<T>(queryResult.Results.ToList(), pageNumber.Value, pageSize.Value, queryResult.TotalItems);
                return queryResult;
            }
            return queryResult;
        }

    }
}
