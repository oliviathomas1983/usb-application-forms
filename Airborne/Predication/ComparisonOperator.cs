namespace Airborne.Predication
{
    /// <summary>
    /// Represents enum comparison operator
    /// </summary>
    public enum ComparisonOperator
    {
        /// <summary>
        /// Equal operator
        /// </summary>
        Equal,

        /// <summary>
        /// Not equal operator
        /// </summary>
        NotEqual,

        /// <summary>
        /// Contains operator
        /// </summary>
        Contains
    }
}