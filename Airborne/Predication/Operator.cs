namespace Airborne.Predication
{
    /// <summary>
    /// Available logical operator options 
    /// </summary>
    public enum Operator
    {
        /// <summary>
        ///Logical AND operation 
        /// </summary>
        And,
        
        
        /// <summary>
        /// Logical OR operation
        /// </summary>
        Or,


        /// <summary>
        /// Logical AND operation
        /// </summary>
        AndAlso
    }
}