﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Airborne.Properties;

namespace Airborne
{
    /// <summary>
    /// A descriptor used to identify a property on a class.
    /// </summary>
    internal class DomainPropertyDescriptor
    {
        #region Fields
        #endregion

        #region Constructor
        public DomainPropertyDescriptor(Type parentType, string propertyName)
        {
            Guard.ArgumentNotNull(parentType, "parentType");
            Guard.ArgumentNotEmpty(propertyName, "propertyName");


            PropertyInfo propertyInfo = parentType.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            if (propertyInfo == null)
            {
                throw new ArgumentException(Resources.PropertyDoesNotExist.FormatCurrentCulture(propertyName, parentType));
            }
            else
            {
                this.ParentType = parentType;
                this.PropertyName = propertyName;
                this.PropertyInfo = propertyInfo;
            }

        }
        #endregion

        #region Properties
        /// <summary>
        /// The type that the property exisist on.
        /// </summary>
        public Type ParentType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the type of the property.
        /// </summary>
        public Type PropertyType
        {
            get
            {
                return PropertyInfo.PropertyType;
            }
        }

        public PropertyInfo PropertyInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// Name of the property.
        /// </summary>
        public string PropertyName
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        #endregion
    }
}
