﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Airborne.Query
{
    /// <summary>
    /// Interface for QueryResult
    /// </summary>
    public interface IQueryResult<out T>
    {
        /// <summary>
        /// Results that holds pagination results.
        /// </summary>
        [DataMember]
        IEnumerable<T> Results { get; }

        /// <summary>
        /// Returned message.
        /// </summary>
        [DataMember]
        string Message { get; }
    }
}
