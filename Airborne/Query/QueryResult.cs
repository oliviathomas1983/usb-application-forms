using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Airborne.Query
{
    /// <summary>
    /// QueryResult 
    /// </summary>
    [DataContract]
    public class QueryResult<T> : IQueryResult<T>
    {
        /// <summary>
        /// Returned results after execution of the query.
        /// </summary>
        [DataMember]
        public IEnumerable<T> Results { get; set; }

        /// <summary>
        /// Total items found for the query.
        /// </summary>
        [DataMember]
        public int TotalItems { get; set; }

        /// <summary>
        /// Returned message.
        /// </summary>
        [DataMember]
        public string Message { get; set; }

        /// <summary>
        /// Creates a new instance of QueryResult
        /// </summary>
        public QueryResult()
        {
            Results = new List<T>();
            TotalItems = 0;
        }
    }
}