﻿namespace Airborne.Utilities
{
    /// <summary>
    ///Defines a typed  assembler "mapping" contract from one type to another.
    ///<para>See http://www.martinfowler.com/eaaCatalog/dataTransferObject.html for more information regarding the assembler pattern.</para>
    /// </summary>
    //public interface IObjectMapper<in TIn, out TOut>
    public interface IObjectMapper<TIn, TOut>
    {
        /// <summary>
        ///
        /// </summary>
        TOut MapObjects(TIn inObject);

        /// <summary>
        ///
        /// </summary>
        TOut MapObjects(TIn inObject, TOut refObject);
    }
}