﻿
function IsDefined(object) {
    return (typeof (object) !== "undefined");
}

function IsDefinedAndNotNull(object) {
    return IsDefined(object) && (object != null);
}

function IsAnObject(object) {
    return (typeof (object) === "object");
}
function IsAString(object) {
    return (typeof (object) === "string");
}

function IsAFunction(object) {
    return (typeof (object) === "function");
}

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};


function Get(controller, action) {
    var records = null;
    $.ajax({
        url: basePath + "/" + controller + "/" + action,
        type: "GET",
        cache: false,
        success: function (result) {
            var object = ProcessResult(result);
            if (object != null) {
                records = object;
            }
        },
        error: function (error) {
            ShowDialog(error.ErrorMessage, "Error");
        }
    });
    return records;
}
function Get(url) {
    var html = null;
    $.ajax({
        url: basePath + url,
        type: "GET",
        cache: false,
        contentType: "application/html",
        success: function (result) {
            html = object;
        },
        error: function (error) {
            ShowDialog(error.ErrorMessage, "Error");
        }
    });
    return html;
}

function GetAsync(url, callback, convertToObservable) {
    var html = null;
    $.ajax({
        url: url,
        type: "GET",
        cache: false,
        async: true,
        success: function (result) {
            if (ResultIsValid(result) == true) {
                var object = ProcessResult(result, convertToObservable);
                if (object != null) {
                    if (IsDefinedAndNotNull(callback)) {
                        callback(object, true);
                    }
                } else {
                      callback(IsDefinedAndNotNull(result.ErrorMessage) ? result.ErrorMessage :null , false);
                }
            }
        },
        error: function (error) {
            callback(GetErrorText(error), false);
        }
    });
    return html;
}


function GetActionAsync(controller, action, callback) {
    var html = null;
    $.ajax({
        url: basePath + "/" + controller + "/" + action,
        type: "GET",
        cache: false,
        async: true,
        contentType: "text/html",
        success: function (result) {
            if (ResultIsValid(result) == true) {
                var object = ProcessResult(result, convertToObservable);
                if (object != null) {
                    if (IsDefinedAndNotNull(callback)) {
                        callback(object, true);
                    }
                } else {
                    callback(IsDefinedAndNotNull(result.ErrorMessage) ? result.ErrorMessage : null, false);
                }
            }
        },
        error: function (error) {
            callback(GetErrorText(error), false);
        }
    });
    return html;
}

function GetData(url, convertToObservable) {
    var records = null;
    $.ajax({
        url: url,
        type: "GET",
        async: false,
        cache: false,
        contentType: "application/json",
        success: function (result) {
            if (ResultIsValid(result) == true) {
                var object = ProcessResult(result, convertToObservable);
                if (object != null)
                    records = object;
            }
        },
        error: function (error) {
            ShowDialog(error.responseText + error.responseText, "Error");
        }
    });
    return records;
}

function ResultIsValid(result) {
    if (IsDefinedAndNotNull(result)) {
        if (IsDefinedAndNotNull(result.Success)) {
            if (GetValue(result.Success) == false) {
                ShowDialog("An error occurred. " + ((IsDefinedAndNotNull(result) && IsDefinedAndNotNull(result.ErrorMessage)) ? " " + GetValue(result.ErrorMessage) : ""), "Error");
                return false;
            };
        }
        return true;
    }
    ShowDialog("An error occurred. Result is not defined ");
    return false;
}

function GetActionDataAsync(controller, action, callback, convertToObservable) {
    $.ajax({
        url: basePath + "/" + controller + "/" + action,
        type: "GET",
        async: true,
        cache: false,
        contentType: "application/json",
        success: function (result) {
            if (ResultIsValid(result) == true) {
                var object = ProcessResult(result, convertToObservable);
                if (object != null) {
                    if (IsDefinedAndNotNull(callback)) {
                        callback(object, true);
                    }
                } else {
                    callback(IsDefinedAndNotNull(result.ErrorMessage) ? result.ErrorMessage : null, false);
                }
            }
        },
        error: function (error) {
            callback(GetErrorText(error), false);
        }
    });
}



function GetDataAsync(url, callback, convertToObservable) {
    $.ajax({
        url: url,
        type: "GET",
        async: true,
        cache: false,
        contentType: "application/json",
        success: function (result) {
            if (ResultIsValid(result) == true) {
                var object = ProcessResult(result, convertToObservable);
                if (object != null) {
                    if (IsDefinedAndNotNull(callback)) {
                        callback(object, true);
                    }
                } else {
                    callback(IsDefinedAndNotNull(result.ErrorMessage) ? result.ErrorMessage : null, false);
                }
            }
        },
        error: function (error) {
            callback(GetErrorText(error), false);
        }
    });
}

function Post(controller, action, data, convertToObservable) {
    var records = null;
    var datatoSend = data;
    $.ajax({
        url: basePath + "/" + controller + "/" + action,
        type: "POST",
        data: ko.toJSON(datatoSend),
        async: false,
        cache: false,
        contentType: "application/json",
        success: function (result) {
            if (ResultIsValid(result) == true) {
                var object = ProcessResult(result, convertToObservable);
                if (object != null)
                    records = object;
            } else {
                callback(IsDefinedAndNotNull(result.ErrorMessage) ? result.ErrorMessage : null, false);
            };
        },
        error: function (error) {
            callback(GetErrorText(error), false);
        }
    });
    return records;
}


function PostAsync(controller, action, data, callback, convertToObservable) {

    var datatoSend = data;
    $.ajax({
        url: basePath + "/" + controller + "/" + action,
        type: "POST",

        data: ko.toJSON(datatoSend),
        async: true,
        cache: false,
        contentType: "application/json",
        success: function (result) {
            if (ResultIsValid(result) == true) {
                var object = ProcessResult(result, convertToObservable);
                if (object != null) {
                    if (IsDefinedAndNotNull(callback)) {
                        callback(object, true);
                    }
                }
            } else {
                callback(IsDefinedAndNotNull(result.ErrorMessage) ? result.ErrorMessage :null , false);
            };
        },
        error: function (error) {
            callback(GetErrorText(error), false);
        }
    });
}





function ProcessResult(result, convertToObservable) {

    if (!IsDefinedAndNotNull(convertToObservable))
        convertToObservable = true;

    var returnValue = (convertToObservable == true) ? ko.mapping.fromJS(result) : result;
    return returnValue;
}

function RedirectToAction(controller, action, params) {
    var url = basePath + controller + "/" + action + "?" + GetParamsListForObject(params);
    window.location.href = url;
    return url;
}


function HtmlEncode(value) {
    return $('<div/>').html(value).text();
}

function GetParamsListForObject(obj) {
    var paramList = "";
    if (IsDefinedAndNotNull(obj)) {
        $.each(obj, function (index, item) {
            var valuePair = index.concat("=", item);
            paramList += ("&" + valuePair);
        });
        paramList = HtmlEncode(paramList.substring(1));

    };
    return paramList;
}


function GetErrorText(error) {
    return (IsDefinedAndNotNull(error.responseText) ? error.responseText : "") + " " + (IsDefinedAndNotNull(error.statusText) ? error.statusText : "")
}


Object.extend = function (destination, source) {

    if (IsDefinedAndNotNull(source)) {
        if ((IsAnObject(source) && !Array.isArray(source) && !IsAString(source))) {
            for (var property in source) {
                if (!isNaN(property)) {
                    continue;
                }
                if ((IsAFunction(source[property]) && !ko.isObservable(source[property])) || !source.hasOwnProperty(property)) {
                    destination[property] = source[property];
                } else {
                    if (ko.isObservable(source[property])) {

                        if (IsDefinedAndNotNull(source[property]())) {
                            var local = source[property]();
                            var value = Object.extend({}, local);
                            var result = Array.isArray(value) ? ko.observableArray(value) : ko.observable(value);
                            destination[property] = result;
                        } else {
                            destination[property] = ko.observable(IsDefined(source[property]()) ? null : undefined);
                        }

                    }
                    else
                        destination[property] = Object.extend({}, source[property]);

                }
            }
        }
        else if (Array.isArray(source)) {
            destination = [];
            $.each(source, function (index, item) {
                destination.push(Object.extend({}, item));
            });
        }
        else {
            destination = IsAFunction(source) ? source : JSON.parse(JSON.stringify(source));
        }
    }
    else {
        destination = source;
    }

    return destination;
};

Object.defineProperty(Object.prototype, "Clone", {
    enumerable: false,
    value: function () {
        return Object.extend({}, this);
    }
});



function ShowDialog(message, title, width, height) {
    $("#dialogMessage").html(message);
    $("#dialog").dialog(
    {
        modal: true,
        draggable: true,
        resizable: true,
        open: function () {
            $('.ui-dialog-title').html(typeof (title) === 'undefined' ? "Information" : title);

        },
        buttons: {
            "Cancel": function () {
                $(this).dialog("close");
                $(this).dialog("destroy");
                $(".dialog").each(function () {
                    $(this).empty();
                    $(this).remove();
                });
            }
        },
        width: IsDefinedAndNotNull(width) ? width : 670,
        height: IsDefinedAndNotNull(height) ? height : "auto",

        dialogClass: 'customdialog'

    });
    return $("#dialog");
}


function ShowCustomDialog(elem, title, width, height, showOk, okCallback, showCancel) {
    var close = function () {
        $(this).dialog("close");
        $(this).dialog("destroy");
        $(".dialog").each(function () {
            $(this).empty();
            $(this).remove();
        });
    };

    var ok = function () {
        if (IsDefinedAndNotNull(okCallback)) {
            okCallback();
        }
        else
        {
            $(this).dialog("close");
        }
    };
    var getButtons = function(mustShowOK, mustShowCancel) {
        var result = {};
        if (IsDefinedAndNotNull(mustShowOK) && mustShowOK == true) {
            result["Ok"] = ok;
        }

        if (!IsDefinedAndNotNull(mustShowCancel) || mustShowCancel == true) {
            result["Cancel"] = close;
        } 

        return result;
    };
    $(elem).addClass("dialog");
    $(elem).dialog(
    {
        modal: true,
        draggable: true,
        resizable: true,
        open: function () {
            $('.ui-dialog-title').html(typeof (title) === 'undefined' ? "Information" : title);

        },
        buttons: getButtons(showOk, showCancel),
        width: IsDefinedAndNotNull(width) ? width : 470,
        height: IsDefinedAndNotNull(height) ? height : "auto",
        dialogClass: 'customdialog'

    });

    return (elem);
}


function CloseAllDialogs() {

    $(".dialog").each(function () {

        if (IsDefinedAndNotNull($(this).dialog)) {

            $(this).dialog("destroy");
        }
        $(this).empty();
        $(this).remove();

    });
}

function CloseDialog(elem) {
    if (!IsDefinedAndNotNull(elem)) {
        elem = $(".dialog");
    };

    if (IsDefinedAndNotNull($(elem).dialog)) {
        $(elem).dialog("destroy");
    }
    $(elem).empty();
    $(elem).remove();
}


function ShowCustomDialogForAction(parentElem, controller, action, data, title, width, height, showOk, okCallback) {
    $(".dialog").dialog("destroy");
    $(".dialog").empty();
    $(".dialog").remove();

    var elem = $("<div class='dialog'></div>");

    $(loadingDiv).appendTo(elem);
    $(elem).appendTo(parentElem);
    var callback = function (result, success) {
        if (success) {
            $(elem).html(result);
        }
        $("#LoadingDiv").hide();
    };
    $("#LoadingDiv").show();
    PostAsync(controller, action, data, callback, false);
    ShowCustomDialog(elem, title, width, height, showOk, okCallback);
}

function ShowCustomDialogForUrl(parentElem, url, title, width, height, showOk, okCallback) {
    $(".dialog").each(function () {

        if (IsDefinedAndNotNull($(this).dialog)) {
            alert
            $(this).dialog("destroy");
        }
        $(this).empty();
        $(this).remove();

    });
    var elem = $("<div class='dialog'></div>");

    $(loadingDiv).appendTo(elem);
    $(elem).appendTo(parentElem);
    var callback = function (result, success) {
        if (success) {
            $(elem).html(result);
        }
        $("#LoadingDiv").hide();
    };
    $("#LoadingDiv").show();
    GetAsync(url, callback, false);
    ShowCustomDialog(elem, title, width, height, showOk, okCallback);

}

function Null(object, defaultValue) {
    return (IsDefined(object) && object != null) ? object : defaultValue;
}
function ObservableNull(object, defaultValue) {
    if (!IsDefined(object) || (object == null))
        return defaultValue;

    var local = ko.isObservable(object) ? object() : object;
    return (local != null) ? local : defaultValue;
}

function GetValue(object) {
    return (ko.isObservable(object)) ? object() : object;
}

function SetValue(object, value) {
    if (ko.isObservable(object)) {
        object(value);
    } else {
        object = value;
    }
}

function GetProperty(data, id) {
    var parts = id.split(".");
    if (parts.length > 1) {
        var remaining = id.substring(parts[0].length);
        return GetProperty(data, remaining);
    };
    return data[id];
}

function AddValidationRules(model) {

    $.each(model, function (index, property) {

        if (typeof (index) !== "string" || index.indexOf("MetaData") > 0)
            return;

        if (!ko.isObservable(property) && IsAFunction(property))
            return;


        var metadata = model[index + "MetaData"];

        if (IsDefinedAndNotNull(metadata)) {
            if (typeof (metadata["data-val-required"]) !== "undefined") {
                property = property.extend({ required: { params: true, message: metadata["data-val-required"]() } });
            }

            if (typeof (metadata["data-val-length-min"]) !== "undefined") {
                property = property.extend({ minLength: { params: metadata["data-val-length-min"](), message: metadata["data-val-length"]() } });
            }

            if (typeof (metadata["data-val-length-max"]) !== "undefined") {
                property = property.extend({ maxLength: { params: metadata["data-val-length-max"](), message: metadata["data-val-length"]() } });
            }

            if (typeof (metadata["data-val-date"]) !== "undefined") {
                property = property.extend({ date: { params: true, message: metadata["data-val-date"]() } });
            }

            if (typeof (metadata["data-val-regex-pattern"]) !== "undefined") {
                property = property.extend({ pattern: { params: metadata["data-val-regex-pattern"](), message: metadata["data-val-regex"]() } });
            }
        }
        else {

            var local = (ko.isObservable(property) ? property() : property);

            if (local != null && (typeof (local) !== "string")) {
                if (Array.isArray(local) && (local.length > 0) && (local[0] !== "_destroy")) {
                    $.each(local, function (index1, item1) {
                        AddValidationRules(item1);
                    });
                }
                if ((IsAnObject(local))) {
                    AddValidationRules(local);
                }
            }
        }

    });
}

String.prototype.Contains = function (string2) {
    return this.trim().toUpperCase().indexOf(string2.trim().toUpperCase()) >= 0;
}

function ParseJsonDate(jsonDateString) {
    var thisDate = new Date(parseInt(jsonDateString.replace('/Date(', '')));
    return (thisDate.getFullYear() + "/" + thisDate.getMonth() + 1) + "/" + (thisDate.getDate() + 1);
}

function GetPageData(data, page) {
    var pageSize = 10;
    if (data.length == 0) {
        return data;
    };

    var count = 0;
    var rangeStart = ((page - 1) * pageSize);
    var rangeEnd = page * pageSize - 1;
    var result = $.grep(data, function () {
        var inRange = (count >= rangeStart && count <= rangeEnd);
        count++;
        return inRange;
    });

    return result;

}

function SetColumnWidths(elem) {

    $.each($(elem).find(".data-row"), function (index, row) {
        $.each($(row).children(), function (i, item) {
            var headerCell = $(elem).find(".header-cell:nth-child(" + (i + 1) + ")");
            var widthProperty = $(headerCell).css("width");
            if (IsDefinedAndNotNull(widthProperty)) {
                var csswidth = headerCell.css("width");
                $(item).css("width", csswidth);
            };
        });
    });

}

function GetCellWidth(elem) {
    var totalWidth = 0;
    if ($(elem).text().length > 0) {
        totalWidth = getTextWidth($(elem).text(), "16pt arial");
        return totalWidth;
    }
    if ($(elem).children().length > 0) {
        $.each($(elem).children(), function (i, child) {
            totalWidth += GetCellWidth(child);
        });
    }
    return totalWidth;
}

function SetColumnWidthsD() {

    $.each($(".datagrid"), function (cnt, grid) {
        var maxWidths = {};
        $.each($(grid).find(".header-cell"), function (index, headerCell) {
            if (!IsDefinedAndNotNull(maxWidths[index])) {
                maxWidths[index] = GetCellWidth(headerCell);
            };
        });

        $.each($(grid).find(".data-row"), function (index, row) {
            $.each($(row).find(".row-cell"), function (i, item) {

                var cellWidth = GetCellWidth(item);
                if (cellWidth > maxWidths[i]) {
                    maxWidths[i] = cellWidth;
                }
            });
        });
        $.each($(grid).find(".header-cell"), function (index, cell) {
            $(cell).css("width", maxWidths[index] + "px");
        });
        $.each($(grid).find(".data-row"), function (rowIndex, row) {
            $.each($(row).find(".row-cell"), function (index, cell) {
                $(cell).css("width", maxWidths[index] + "px");
            });
        });
    });
}
var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));

function getTextWidth(text, font) {
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return metrics.width;
}
