﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.SharePoint.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using System.IO;
using SettingsMan = USB.Domain.Properties.Settings;
using StellenboschUniversity.Usb.Integration.Crm;
using Usb.Domain.Sharepoint;

namespace StellenboschUniversity.Usb.Integration.Crm.Crm
{
    public static class CrmFileUpload
    {
        public static void CreateUploadedDocumentLocation(Guid enrollmentId)
        {
            CreateDocumentLocationForEnrollment(enrollmentId);
        }

        public static void CreateDocumentLocationForEnrollment(Guid enrollmentId)
        {
            if ( !DoesDocumentLocationForEnrollmentExist(enrollmentId) )
            {
                Guid siteId = GetSharepointSiteId(SettingsMan.Default.CrmSharepointSiteSet);

                using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
                {
                    var sharePointDocumentLocation = new SharePointDocumentLocation()
                    {
                        Name = enrollmentId.ToString(),
                        Description = "",
                        ParentSiteOrLocation = new CrmEntityReference(SharePointSite.EntityLogicalName, siteId),
                        RelativeUrl = enrollmentId.ToString(),
                        RegardingObjectId = new CrmEntityReference(usb_studentacademicrecord.EntityLogicalName, enrollmentId)
                    };

                    try
                    {
                        organizationService.Create(sharePointDocumentLocation);
                    }
                    finally
                    {
                        organizationService.Dispose();
                    }
                }
            }
        }

        public static bool DoesDocumentLocationForEnrollmentExist(Guid enrollmentId)
        {
            SharePointDocumentLocation crmResult = null;
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    crmResult = (from q in crmServiceContext.SharePointDocumentLocationSet
                                 where q.RelativeUrl == enrollmentId.ToString()
                                 select q).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

            }

            if (crmResult == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private static Guid GetSharepointSiteId(string siteName)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                SharePointSite crmResult = null;

                try
                {
                    crmResult = (from q in crmServiceContext.SharePointSiteSet
                                 //QA and Prod CRM settings not the same, confirmed that there is only one Sahrepoint site.
                                // where q.Name == siteName
                                 select q).First();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmResult == null)
                {
                    throw new Exception();
                }

                return crmResult.Id;
            }
        }

    }
}
