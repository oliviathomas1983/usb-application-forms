﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public static class CrmIdToSisCodeMapping
    {
        public static string GetTitleSisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_title lookup = null;

                try
                {
                    lookup = (from s in crmServiceContext.usb_titleSet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.usb_SISCode;
            }
        }

        public static string GetGenderSisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_gender lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.usb_genderSet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                switch (lookup.usb_name.ToUpper())
                {
                    case "MALE":
                        return "M";

                    case "FEMALE":
                        return "V";

                    case "NON BINARY":
                        return "N";

                    default:
                        throw new Exception();
                }
            }
        }

        public static string GetEthnicitySisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_ethnicity lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.usb_ethnicitySet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.usb_SISCode;
            }
        }

        public static string GetNationalitySisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_nationality lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.usb_nationalitySet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.usb_SISCode;
            }
        }

        public static string GetLanguageSisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_language lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.usb_languageSet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.usb_LanguageCode;
            }
        }
    }
}
