﻿using Microsoft.Xrm.Client;
using SettingsMan = USB.Domain.Properties.Settings;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public static class CrmServer
    {
        private static CrmConnection crmConnection = Microsoft.Xrm.Client.CrmConnection.Parse(SettingsMan.Default.CrmConnectionString);

        public static CrmConnection CrmConnection
        {
            get
            {
                return crmConnection;
            }
        }
    }
}
