﻿using System;
using System.Collections.Generic;
using System.Linq;
using USB.Domain.Models.Enums;
using SettingsMan = USB.Domain.Properties.Settings;
using Airborne;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public static class Dropdowns
    {
        public static Dictionary<string, Guid> Titles()
        {
            var titles = new Dictionary<string, Guid>();
            var firstTitles = new List<string>() { "Mr", "Mrs", "Miss", "Dr", "Prof" };

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var crmResults = from q in crmServiceContext.usb_titleSet orderby q.usb_nameenglish select q;

                foreach (var crmResult in crmResults)
                {
                    if (titles.ContainsKey(crmResult.usb_nameenglish) == false)
                    {
                        titles.Add(crmResult.usb_nameenglish, crmResult.Id);
                    }
                }
            }

            var result = new Dictionary<string, Guid>();

            foreach (var firstTitle in firstTitles)
            {
                if (titles.ContainsKey(firstTitle))
                {
                    result.Add(firstTitle.ToTitleCase(), titles[firstTitle]);
                }
            }

            foreach (var title in titles)
            {
                if (result.ContainsKey(title.Key.ToTitleCase()) == false)
                {
                    result.Add(title.Key.ToTitleCase(), title.Value);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> Genders()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_gender> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_genderSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_name, crmResult.Id);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> Ethnicities()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_ethnicity> crmResults = null;

                try
                {
                    crmResults = from q in crmServiceContext.usb_ethnicitySet orderby q.usb_nameenglish select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_nameenglish.ToTitleCase(), crmResult.Id);
                }
            }

            return result;
        }


        public static Dictionary<string, Guid> NonSAEthnicities()
        {
            var result = new Dictionary<string, Guid>();
            var ethnicities = Ethnicities();

            var africanGuid = ethnicities.FirstOrDefault(e => e.Key == "African").Value;
            var asianGuid = ethnicities.FirstOrDefault(e => e.Key == "Indian").Value;
            var caucasianGuid = ethnicities.FirstOrDefault(e => e.Key == "White").Value;
            var mixedGuid = ethnicities.FirstOrDefault(e => e.Key == "Coloured").Value;
            var preferNotToSayGuid = ethnicities.FirstOrDefault(e => e.Key == "Prefer Not To Say").Value;
            var otherGuid = ethnicities.FirstOrDefault(e => e.Key == "African").Value;

            result.Add("African", africanGuid);
            result.Add("Asian", asianGuid);
            result.Add("Caucasian", caucasianGuid);
            result.Add("Mixed", mixedGuid);
            result.Add("Prefer Not To Say", preferNotToSayGuid);
            result.Add("Other", otherGuid);

            return result;
        }


        public static Dictionary<string, Guid> Nationalities()
        {
            var nationalities = new Dictionary<string, Guid>();
            var firstNationalities = new List<string>() { "South Africa" };

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_nationality> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_nationalitySet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (nationalities.ContainsKey(crmResult.usb_name) == false)
                    {
                        nationalities.Add(crmResult.usb_name, crmResult.Id);
                    }
                }
            }

            var result = new Dictionary<string, Guid>();

            foreach (var firstNationality in firstNationalities)
            {
                if (nationalities.ContainsKey(firstNationality))
                {
                    result.Add(firstNationality, nationalities[firstNationality]);
                }
            }

            foreach (var nationality in nationalities)
            {
                if (result.ContainsKey(nationality.Key) == false)
                {
                    result.Add(nationality.Key, nationality.Value);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> CorrespondenceLanguages()
        {
            var correspondenceLanguages = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_correspondencelanguage> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_correspondencelanguageSet
                                 orderby q.usb_nameenglish
                                 select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (correspondenceLanguages.ContainsKey(crmResult.usb_nameenglish) == false)
                    {
                        correspondenceLanguages.Add(crmResult.usb_nameenglish, crmResult.Id);
                    }
                }
            }

            return correspondenceLanguages;
        }

        public static Dictionary<string, Guid> Languages()
        {
            var languages = new Dictionary<string, Guid>();
            var firstLanguages = new List<string>() { "ENGLISH", "AFRIKAANS" };

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_language> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_languageSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (languages.ContainsKey(crmResult.usb_name) == false)
                    {
                        languages.Add(crmResult.usb_name, crmResult.Id);
                    }
                }
            }

            var result = new Dictionary<string, Guid>();

            foreach (var firstLanguage in firstLanguages)
            {
                if (languages.ContainsKey(firstLanguage))
                {
                    result.Add(firstLanguage, languages[firstLanguage]);
                }
            }

            foreach (var language in languages)
            {
                if (result.ContainsKey(language.Key) == false)
                {
                    result.Add(language.Key, language.Value);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> ForeignIdentificationTypes()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_idtype> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_idtypeSet orderby q.usb_NameEnglish select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    string name = crmResult.usb_NameEnglish;

                    if (String.Equals(name, "Id Number", StringComparison.InvariantCultureIgnoreCase) == false)
                    {
                        result.Add(crmResult.usb_NameEnglish, crmResult.Id);
                    }
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> SouthAfricaIdentificationTypes()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_idtype> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_idtypeSet orderby q.usb_NameEnglish select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    string name = crmResult.usb_NameEnglish;

                    if (String.Equals(name, "Id Number", StringComparison.InvariantCultureIgnoreCase) == true)
                    {
                        result.Add(crmResult.usb_NameEnglish, crmResult.Id);
                    }
                }
            }

            return result;
        }

        public static IEnumerable<KeyValuePair<string, Guid>> Disabilities()
        {
            var result = new Dictionary<string, Guid>();
            var resultNoneValueTop = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_disability> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_disabilitySet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (crmResult.usb_name == "None")
                    {
                        resultNoneValueTop.Add(crmResult.usb_name, crmResult.Id);
                    }
                    else
                    {
                        result.Add(crmResult.usb_name, crmResult.Id);
                    }
                }
            }

            var n = resultNoneValueTop.Concat(result);

            return n;
        }

        public static Dictionary<string, Guid> AddressCountries()
        {
            var addressCountries = new Dictionary<string, Guid>();
            var firstaddressCountries = new List<string>() { "South Africa" };

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_country> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_countrySet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (addressCountries.ContainsKey(crmResult.usb_name) == false)
                    {
                        addressCountries.Add(crmResult.usb_name, crmResult.Id);
                    }
                }
            }

            var result = new Dictionary<string, Guid>();

            foreach (var firstAddressCountry in firstaddressCountries)
            {
                if (addressCountries.ContainsKey(firstAddressCountry))
                {
                    result.Add(firstAddressCountry, addressCountries[firstAddressCountry]);
                }
            }

            foreach (var addressCountry in addressCountries)
            {
                if (result.ContainsKey(addressCountry.Key) == false)
                {
                    result.Add(addressCountry.Key, addressCountry.Value);
                }
            }

            return result;
        }

        public static Dictionary<string, int> QualificationFields()
        {
            //Demian option set
            //var crmResults = from q in crmServiceContext.usb_FieldOfStudyOptionset orderby q.stb_NameEnglish select q;
            return CrmQueries.GetOptionSetValues("usb_qualifications", "usb_FieldOfStudyOptionset");
        }

        public static Dictionary<string, Guid> QualificationTypes()
        {
            var intermediateResult = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_qualifications> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_qualificationsSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    intermediateResult.Add(crmResult.usb_name, crmResult.Id);
                }
            }

            var result = new Dictionary<string, Guid>();
            string key;
            Guid value;

            key = "Grade 12";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "Certificate, Diploma";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "B Degree";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "Post Graduate, Honours";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "Doctorate, Masters";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "Other";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            return result;
        }

        public static Dictionary<string, int> GrossSalaries()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName, "usb_salaryoptionset");
        }

        public static Dictionary<string, int> Industries()
        {
            return CrmQueries.GetOptionSetValues(usb_employmenthistory.EntityLogicalName, "usb_industryoptionset");
        }

        public static Dictionary<string, int> WorkAreas()
        {
            return CrmQueries.GetOptionSetValues(usb_employmenthistory.EntityLogicalName, "usb_workareaoptionset");
        }

        public static Dictionary<string, int> EmployerWorkAreas()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName, "usb_employerworkarea");
        }

        public static Dictionary<string, int> LeesAfrikaans()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_afrikaansleesoptionset");
        }

        public static Dictionary<string, int> PraatAfrikaans()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_afrikaanspraatoptionset");
        }

        public static Dictionary<string, int> SkryfAfrikaans()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_afrikaansskryfoptionset");
        }

        public static Dictionary<string, int> VerstaanAfrikaans()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_afrikaansverstaanoptionset");
        }

        public static Dictionary<string, int> EnglishRead()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName, "usb_englishreadOptionset");
        }

        public static Dictionary<string, int> EnglishSpeak()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_englishspeakoptionset");
        }

        public static Dictionary<string, int> EnglishUnderstand()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_englishunderstandoptionset");
        }

        public static Dictionary<string, int> EnglishWrite()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_englishwriteoptionset");
        }

        public static Dictionary<string, int> OccupationalCategories()
        {
            return CrmQueries.GetOptionSetValues("usb_employmenthistory", "stb_leadsource");
        }

        public static Dictionary<string, int> MarketingSources()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName, "stb_leadsource");
        }

        public static Dictionary<string, int> MarketingReasons()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName, "stb_marketingreason");
        }

        public static Dictionary<string, int> EmployerAssistFinancially()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_employerassistfinanciallyOptionset");
        }

        public static Dictionary<string, int> EmploymentType()
        {
            return CrmQueries.GetOptionSetValues(usb_employmenthistory.EntityLogicalName, "usb_employmenttypeoptionset");
        }

        public static Dictionary<string, Guid> PermitTypes()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_permittype> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_permittypeSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_name, crmResult.Id);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> MaritalStatus()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_maritalstatus> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_maritalstatusSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_name, crmResult.Id);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> Offerings(bool isMBA, Guid programmeGuid)
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_programmeoffering> crmProgrammeOffering = null;
                try
                {
                    if (isMBA)
                    {
                        crmProgrammeOffering = from q in crmServiceContext.usb_programmeofferingSet
                                               orderby q.usb_name
                                               where q.usb_name.StartsWith("MBA")
                                                     && q.statuscode == (int)usb_programmeoffering_statuscode.OfferingLaunched
                                               select q;
                    }
                    else
                    {
                        crmProgrammeOffering =
                            crmServiceContext.usb_programmeofferingSet.Where(
                                p =>
                                    p.usb_ProgrammeLookup.Id == programmeGuid
                                    && p.statuscode == (int)usb_programmeoffering_statuscode.OfferingLaunched);
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmProgrammeOffering)
                {
                    if (crmResult.usb_YearOffered.IsNotNullOrEmpty())
                    {
                        if (!crmResult.usb_YearOffered.Contains(" "))
                        {
                            int defaultDate;
                            int.TryParse(crmResult.usb_YearOffered, out defaultDate);

                            if (defaultDate >= DateTime.Now.Year)
                            {
                                result.Add(crmResult.usb_name, crmResult.Id);
                            }
                        }
                    }
                }
            }

            return result;
        }


        public static List<Tuple<string, Guid, ProgrammeOfferingType>> EnrolmentIdGetOfferings(bool isMBA, Guid programmeGuid, Guid enrolmentId)
        {
            var result = new List<Tuple<string, Guid, ProgrammeOfferingType>>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_programmeoffering> crmProgrammeOffering = null;
                usb_studentacademicrecord crmEnrolmentPOId = null; //Curent Programme Offering ID for specific enrolmentId to populate PO's eventhough they are not Launced

                try
                {
                    if (isMBA)
                    {
                        crmEnrolmentPOId = crmServiceContext.usb_studentacademicrecordSet.Where(x => x.usb_studentacademicrecordId == enrolmentId).FirstOrDefault();

                        crmProgrammeOffering = crmServiceContext.usb_programmeofferingSet.Where(x => (x.usb_name.Contains("MBA")
                        && x.statuscode == (int)usb_programmeoffering_statuscode.OfferingLaunched)
                        || x.usb_programmeofferingId == crmEnrolmentPOId.usb_ProgrammeOfferingLookup.Id).OrderBy(x => x.usb_name);

                    }
                    else
                    {
                        crmEnrolmentPOId = crmServiceContext.usb_studentacademicrecordSet.Where(x => x.usb_studentacademicrecordId == enrolmentId).FirstOrDefault();
                        crmProgrammeOffering =
                            crmServiceContext.usb_programmeofferingSet.Where(
                                p =>
                                    p.usb_ProgrammeLookup.Id == programmeGuid
                                    && p.statuscode == (int)usb_programmeoffering_statuscode.OfferingLaunched
                                    || p.usb_programmeofferingId == crmEnrolmentPOId.usb_ProgrammeOfferingLookup.Id);
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmProgrammeOffering)
                {
                    if (crmResult.usb_YearOffered.IsNotNullOrEmpty() && !crmResult.usb_YearOffered.Contains(" "))
                    {

                        int defaultDate;
                        int.TryParse(crmResult.usb_YearOffered, out defaultDate);
                        
                        //if (defaultDate >= DateTime.Now.Year)
                        //{
                        //    result.Add(Tuple.Create(crmResult.usb_name, crmResult.Id,
                        //        (ProgrammeOfferingType)
                        //        Enum.ToObject(typeof(ProgrammeOfferingType),
                        //            crmResult.usb_OfferingTypeOptionset.GetValueOrDefault())));
                        //}
                       
                        if (crmResult.Id == crmEnrolmentPOId.usb_ProgrammeOfferingLookup.Id)
                        {
                            var myProgramme = Tuple.Create(crmResult.usb_name, crmResult.Id,
                                (ProgrammeOfferingType)Enum.ToObject(typeof(ProgrammeOfferingType),
                                    crmResult.usb_OfferingTypeOptionset.GetValueOrDefault()));
                            if (!result.Contains(myProgramme))
                                result.Add(myProgramme);
                        }

                    }
                }
            }

            return result;
        }

        public static List<Tuple<string, Guid, ProgrammeOfferingType>> GetOfferings(bool isMBA, Guid programmeGuid)
        {
            var result = new List<Tuple<string, Guid, ProgrammeOfferingType>>();
            //HasSARWithApplicantStatus

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_programmeoffering> crmProgrammeOffering = null;             

                try
                {
                    if (isMBA)
                    {
                        crmProgrammeOffering = from q in crmServiceContext.usb_programmeofferingSet
                                               orderby q.usb_name
                                               where q.usb_name.Contains("MBA")
                                                     && q.statuscode == (int)usb_programmeoffering_statuscode.OfferingLaunched
                                               select q;
                    }
                    else
                    {          
                        crmProgrammeOffering =
                            crmServiceContext.usb_programmeofferingSet.Where(
                                p =>
                                    p.usb_ProgrammeLookup.Id == programmeGuid
                                    && p.statuscode == (int)usb_programmeoffering_statuscode.OfferingLaunched);
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmProgrammeOffering)
                {
                    if (crmResult.usb_YearOffered.IsNotNullOrEmpty())
                    {
                        if (!crmResult.usb_YearOffered.Contains(" "))
                        {
                            int defaultDate;
                            int.TryParse(crmResult.usb_YearOffered, out defaultDate);

                            if (defaultDate >= DateTime.Now.Year)
                            {
                                result.Add(Tuple.Create(crmResult.usb_name, crmResult.Id,
                                    (ProgrammeOfferingType)
                                    Enum.ToObject(typeof(ProgrammeOfferingType),
                                        crmResult.usb_OfferingTypeOptionset.GetValueOrDefault())));
                            }
                        }
                    }
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> Programme()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_programme> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_programmeSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_name, crmResult.Id);
                }
            }

            return result;
        }

        public static Dictionary<string, int> MathematicCompetency()
        {
            //return new Dictionary<string, int>() { 
            //{ " No Secondary Schooling Mathematics", 1 }, 
            //{ "South African Grade 9", 2 }, 
            //{ "South African Grade 12 SG", 3 },
            //{ "South African Grade 12 HG", 4 },
            //{ "GCSE", 5 },
            //{ "A Levels", 6 },
            //{ "Tertiary Education", 7 }, 
            //{ "Other", 8 },
            //{ "Chartered Accountant", 9 },
            //{"IEB Grade 12 HG", 10 },
            //{"Engineering Mathematics, Applied Mathematics", 11},
            //{"German Abitur (Higher Secondary Education)", 12},
            //{"Technical University Diploma in Germany", 13},
            //{"First Semester Calcs and Stats, National Diploma",14},
            //{"Statistics Honours", 15},
            //{"German A-Level, Witten Examination; Several Math courses at University level",16},
            //{"German Abitur and all Math courses, Chemistry master with all advanced math courses",17}
            //};

            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_highestlevelmathematicscompetedOptionset");
        }

        //TODO:Mathematics percentage is not in Data dictionary
        public static Dictionary<string, int> MathematicPercentage()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_highestlevelmathematicscompetedoptionset");
        }

        public static Dictionary<string, int> PriorInvolvement()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName,
                "usb_whatdidyoudolastyearoptionset");
        }

        public static Dictionary<string, Guid> Institution()
        {
            var result = new Dictionary<string, Guid>();
            var resultOther = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_academicinstitution> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_academicinstitutionSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (crmResult.usb_name.ToLower().Trim().Contains("other"))
                    {
                        resultOther.Add(crmResult.usb_name, crmResult.Id);
                    }
                    else
                    {
                        result.Add(crmResult.usb_name, crmResult.Id);
                    }

                }
                foreach(var crmother in resultOther)
                {
                    result.Add(resultOther.Keys.First(), resultOther.Values.First());
                }
              
            }

            return result;
        }

        public static Dictionary<string, int> FieldOfStudy()
        {
            return CrmQueries.GetOptionSetValues(usb_qualifications.EntityLogicalName, "usb_fieldofstudyoptionset");
        }

        public static Dictionary<string, int> CertificationNumber()
        {
            return CrmQueries.GetOptionSetValues(usb_qualifications.EntityLogicalName, "usb_certificationnumber");
        }

        public static Dictionary<string, int> WorkArea()
        {
            return CrmQueries.GetOptionSetValues(usb_employmenthistory.EntityLogicalName, "usb_workareaoptionset");
        }

        public static Dictionary<string, int> Organization()
        {
            return CrmQueries.GetOptionSetValues(usb_employmenthistory.EntityLogicalName,
                "usb_typeoforganizationoptionset");
        }

        public static Dictionary<string, int> Industry()
        {
            return CrmQueries.GetOptionSetValues(usb_employmenthistory.EntityLogicalName, "usb_industryoptionset");
        }

        public static Dictionary<string, int> Degrees()
        {
            return CrmQueries.GetOptionSetValues(usb_employmenthistory.EntityLogicalName, "usb_degreelookup");
        }

        public static Dictionary<string, int> InfoSessions()
        {
            return CrmQueries.GetOptionSetValues(usb_studentacademicrecord.EntityLogicalName, "usb_infosessionoptionset");
        }

        public static Dictionary<string, int> GetBlendedOptions()
        {
            return
                Enum.GetValues(typeof(BlendedAttendance))
                    .Cast<BlendedAttendance>()
                    .ToList()
                    .ToDictionary(item => item.GetDescription(), item => (int)item);
        }

        public static Dictionary<string, int> GetMBAStreamOptions()
        {
            return
                Enum.GetValues(typeof(MBAStream))
                    .Cast<MBAStream>()
                    .ToList()
                    .ToDictionary(item => item.GetDescription(), item => (int)item);
        }


        public static  Dictionary<string, int> EnrolmentIdGetBlendedOptions(Guid enrolmentId)
        {

            //get sar
            //get option blended
            var result = new Dictionary<string, int?>();
            usb_studentacademicrecord studentAcademicRecord = null;
            studentAcademicRecord = CrmActions.LoadStudentAcademicRecordByEnrolrmentId(enrolmentId);
            int? sarBlendedOption = null;
          
            if (studentAcademicRecord.IsNotNull())
            {

                sarBlendedOption = studentAcademicRecord.usb_BlendedAttendanceOptionset;
               
            }
            
           
            return Enum.GetValues(typeof(BlendedAttendance))
                   .Cast<BlendedAttendance>().Where(item => (int)item == sarBlendedOption)
                   .ToList()
                   .ToDictionary(item => item.GetDescription(), item => (int)item); 
        }


        public static Dictionary<string, int> EnrolmentIdGetMBAStreamOptions(Guid enrolmentId)
        {


            usb_studentacademicrecord studentAcademicRecord = null;
            studentAcademicRecord = CrmActions.LoadStudentAcademicRecordByEnrolrmentId(enrolmentId);

            int? sarStreamOption = null;

            if (studentAcademicRecord.IsNotNull())
            {

                sarStreamOption = studentAcademicRecord.usb_MBAStream;

            }

            return Enum.GetValues(typeof(MBAStream))
                 .Cast<MBAStream>().Where(item => (int)item == sarStreamOption)
                 .ToList()
                 .ToDictionary(item => item.GetDescription(), item => (int)item);

           

        }

    }
}