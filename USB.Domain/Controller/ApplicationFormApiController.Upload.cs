﻿using System;
using NLog;
using System.Net;
using StellenboschUniversity.Usb.Integration.Crm;
using Usb.Domain.Sharepoint;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers
{
    public partial class ApplicationFormApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private const string FileAlreadyExistMessage = "File wth the same name already exists in SharePoint."; 

        public string UploadDocument(Guid documentTypeId, Guid enrolmentId, string fileName, byte[] fileData) {
            Tuple<string, byte[]> documentData = new Tuple<string, byte[]>(fileName, fileData);

            if (!SharepointActions.DocumentFileNameExist(enrolmentId, fileName, documentTypeId) &&
                !Cache.ContainsItemWithFileName(enrolmentId.ToString(), fileName))
            {
                if (Cache.ContainsItem(enrolmentId.ToString(), documentTypeId.ToString()))
                {
                    Cache.RemoveItem(enrolmentId.ToString(), documentTypeId.ToString());
                }

                Cache.AddOrReplace(enrolmentId.ToString(), documentTypeId.ToString(), documentData);

                return "success";
            }
            else return FileAlreadyExistMessage;
        }
        public string SaveDocument(Guid documentTypeId, Guid enrolmentId, string fileName, byte[] fileData) {
            Tuple<string, byte[]> documentData = new Tuple<string, byte[]>(fileName, fileData);

            if (!SharepointActions.DocumentFileNameExist(enrolmentId, fileName, documentTypeId) &&
                !Cache.ContainsItemWithFileName(enrolmentId.ToString(), fileName))
            {
                if (Cache.ContainsItem(enrolmentId.ToString(), documentTypeId.ToString()))
                {
                    Cache.RemoveItem(enrolmentId.ToString(), documentTypeId.ToString());
                }

                Cache.AddOrReplace(enrolmentId.ToString(), documentTypeId.ToString(), documentData);

                return "success";
            }
            else return FileAlreadyExistMessage;
        }

        public Tuple<string, byte[]> GetUploadedDocument(int documentId, Guid enrolmentId)
        {
            Tuple<string, byte[]> documentData;

            if (Cache.ContainsItem(enrolmentId.ToString(), documentId.ToString()))
            {
                documentData = Cache.GetCachedItem(enrolmentId.ToString(), documentId.ToString());
            } else {
                documentData = SharepointActions.DownloadDocument(documentId, enrolmentId);
            }

            return documentData;
        }
      public Tuple<string, byte[]> GetUploadedDocumentDirect(int documentId, Guid enrolmentId)
        {
            Tuple<string, byte[]> documentData;
          
            documentData = SharepointActions.DownloadDocument(documentId, enrolmentId);

            return documentData;
        }

        public Tuple<string, byte[]> GetDocument(int documentId, Guid enrolmentId)
        {
            Tuple<string, byte[]> documentData;
            documentData = SharepointActions.DownloadDocument(documentId, enrolmentId);
            return documentData;
        }
    }
}
