﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB.Domain.Models;
using USB.Domain.Models.Enums;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class ApplicationSubmission
    {
        public Guid Offering { get; set; }

        public int? SelectedBlendedOption { get; set; }

        public int? SelectedMbaStream { get; set; }

        public string AlternativeEmailAddress { get; set; }

        public ApplicationSource ApplicationSource { get; set; }

        public ProgrammeType ProgrammeType { get; set; }

        public Guid? ContactIdGuid { get; set; }
        public Guid? EnrolmentGuid { get; set; }

        public string Surname { get; set; }
        public string FirstNames { get; set; }
        public string Initials { get; set; }
        public string GivenName { get; set; }
        public Guid? Title { get; set; }
        public Guid? Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public string NameOnCertificate { get; set; }

        public Guid? PermitType { get; set; }
        public string IdNumber { get; set; }
        public Guid? Ethnicity { get; set; }
        public Guid? MaritalStatus { get; set; }
        public Guid? Nationality { get; set; }
        public Guid? CountryOfIssue { get; set; }
        public Guid? ForeignIdType { get; set; }
        public string PassportNumber { get; set; }
        public DateTime? PassportExpiryDate { get; set; }
        public string ForeignIdNumber { get; set; }
        public DateTime? ForeignIdExpiryDate { get; set; }
        public Guid? GovernmentIdType { get; set; }

        public Guid? CorrespondanceLanguage { get; set; }
        public Guid? Language { get; set; }
        public string MaidenName { get; set; }
        public Guid? DietaryRequirements { get; set; }
        public string DietaryRequirementsOther { get; set; }
        public Guid? Disability { get; set; }
        public string OtherDisability { get; set; }
        public bool UsesWheelchair { get; set; }

        public bool? IsSouthAfrican { get; set; }

        public bool? PersonalInformationTwoOptions { get; set; }
        public bool? AddressDetailsTwoOptions { get; set; }
        public bool? WorkStudiesTwoOptions { get; set; }
        public bool? TellUsMoreTwoOptions { get; set; }
        public bool? DocumentationTwoOptions { get; set; }

        public bool? PaymentTwoOptions { get; set; }
        public bool? StatusTwoOptions { get; set; }
        public bool? SHLTestRequiredTwoOptions { get; set; }
        public bool? DocumentsOutstandingTwoOptions { get; set; }
        public bool? DocumentsCompleteTwoOptions { get; set; }
        public List<DocumentType> UploadedDocumentTypes { get; set; }

        public bool? ApplicationSubmittedTwoOptions { get; set; }

        public bool? Round1InterviewReqTwoOptions { get; set; }

        public int? EmployerAssistFinancially { get; set; }
        public string EmployerEmail { get; set; }
        public string EmployerTelephone { get; set; }
        public string EmployerName { get; set; }

        public bool usb_SearchEngineTwoOptions { get; set; }

        public int? usb_CareerChangeOptionSet  { get; set; }
                    
        public int? usb_ProgressCareerOptionSet { get; set; }
        public int? usb_AcquireManagementSkillsOptionset { get; set; }
        public int? usb_DevelopAsALeaderOptionset { get; set; }
        public int? usb_AcquireBusinessKnowledgeOptionset { get; set; }
        public int? usb_AquireThinkingSkillsOptionset { get; set; }
        public int? usb_ResponsibleLeader { get; set; }
        public int? usb_HigherSalary { get; set; }
        public int? usb_GainKnowledge { get; set; }

        public int? usb_LecturerQaulityOptionset { get; set; }
        public int? usb_ElectronicStudyMaterialOptionset { get; set; }
        public int? usb_StudyMaterialOptionset { get; set; }
        public int? usb_ResearchSupervisionOptionset { get; set; }
        public int? usb_ICTSupportOptionset { get; set; }
        public int? usb_AvailableCareerOptionset { get; set; }
        public int? usb_LibraryServicesOptionset { get; set; }

        public int? usb_AcademicSupportOptionset { get; set; }
        public int? usb_MentorsOptionset { get; set; }
        public int? usb_CampusSecurityOptionset { get; set; }
        public int? usb_CateringServicesOptionset { get; set; }

        public int? usb_AdminSupportOptionset{ get; set; }
        public int? usb_OffcampusSupportOptionset { get; set; }
        public int? usb_DiverseClassGroupOptionset { get; set; }
        public int? usb_DiverseFaultyCompositeOptionset { get; set; }
        public int? usb_BlendedLearningOptionset { get; set; }
        public int? usb_ClassroomBasedOptionset { get; set; }
        public bool? usb_advertisementTwoOptions { get; set; }
        public bool? usb_PromotionalEmailsTwoOptions { get; set; }
        public bool? usb_ReferralByEmployerTwoOptions { get; set; }
        public bool? usb_ReferralByAlumnusTwoOptions { get; set; }
        public bool? usb_ReferralByCurrentStudentTwoOptions { get; set; }
        public bool? usb_ReferralByFamilyFriendTwoOptions { get; set; }
        public bool? usb_SocialMediaTwoOptions { get; set; }
        public bool? usb_NewsArticleTwoOptions { get; set; }
        public bool? usb_ConferenceExpoTwoOptions { get; set; }
        public bool? usb_StellenboschUniversityTwoOptions { get; set; }
        public bool? usb_USBWebsiteTwoOptions { get; set; }
        public bool? usb_USBEdTwoOptions { get; set; }

        public bool? usb_BrochureTwoOptions { get; set; }
        public bool? usb_OnCampusEventTwoOptions { get; set; }
        public bool? usb_TripleAccreditationTwoOptions { get; set; }
        public bool? usb_ValueForMoneyTwoOptions { get; set; }
        public bool? usb_RecommendedByAlumniTwoOptions { get; set; }
        public bool? usb_CloseToHomeTwoOptions { get; set; }
        public bool? usb_ExcellentReputationTwoOptions { get; set; }
        public bool? usb_RecommendedByEmployerTwoOptions { get; set; }
        public bool? usb_HighQualityStudentsTwoOptions { get; set; }
        public bool? usb_ProgrammeFormatTwoOptions { get; set; }
        public bool? usb_USBInternationalTwoOptions { get; set; }
        public bool? usb_USBAfricaFocusTwoOptions { get; set; }
        public bool? usb_USBGeneralLeadershipFocusTwoOptions { get; set; }
        public bool? usb_USBCollaborativeLearningTwoOptions { get; set; }
        public bool? usb_IAmAMatieTwoOptions { get; set; }
        public bool? usb_InternationalLecturersInClassTwoOptions { get; set; }
        public bool? usb_InternationalStudentsTwoOptions { get; set; }
        public bool? usb_DeliveryModeTwoOptions { get; set; }
        public int? usb_AcquireDevFinSkillsOptionset {get; set; }
        public int? usb_AfricaSustainableDevelopmentOptionset {get; set; }
        public int? usb_AfricaEmergingMarketOptionset {get; set; }
        public int? usb_DevelopSAEconomyOptionset { get; set; }
        public int? usb_AfricaMultiNationalEnvironmentOptionset {get; set; }
        public int? usb_AfricaStrategicDecisionOptionset {get; set; }
        public int? usb_BusinessLeaderOptionset {get; set; }
        public int? usb_InsightPoliciesProgrammesOptionset {get; set; }
        public int? usb_SelfDevelopmentOptionset {get; set; }
        public int? usb_EnhanceCoachingSkillsOptionset {get; set; }
        public int? usb_TheoreticalCoachingOptionset {get; set; }
        public int? usb_OwnCoachingPracticeOptionset {get; set; }
        public int? usb_OwnFrameworkModelOptionset {get; set; }
        public int? usb_DevelopmentOfOthersOptionset {get; set; }
        public int? usb_ProfessionalCoachesCommunityOptionset {get; set; }
        public int? usb_ProfessionalFuturistOptionset {get; set; }
        public int? usb_BenefitFromUniqueTypeOptionset {get; set; }
        public int? usb_ComplexOrgEnvironmentOptionset {get; set; }
        public int? usb_SAGrowthContributionOptionset {get; set; }
        public int? usb_TheoryOfFututeStudiesOptionset {get; set; }
        public int? usb_DevelopefutureStudiesOptionset {get; set; }
        public int? usb_AlignForFuturistOptionset {get; set; }
        public int? usb_SustainableFutureOptionset {get; set; }
        public int? usb_EntrepreneurialKnowledgeOptionset {get; set; }
        public int? usb_InnovativeOptionset {get; set; }
        public int? usb_DigitalSkillsOptionset {get; set; }
        public int? usb_ProgressToMBAOptionset {get; set; }
        public int? usb_PMSkillsOptionset {get; set; }
        public int? usb_ManageProjectsOptionset {get; set; }
        public int? usb_AquireScareSkillsOptionset {get; set; }
        public int? usb_ScenarioToolsOptionset {get; set; }
        public int? usb_AuthenticLeadershipOptioset {get; set; }
        public int? usb_MakeADifferenceOptionset {get; set; }
        public int? usb_HowToBeALeaderOptionset {get; set; }
        public int? usb_SustainableTransOptionset {get; set; }
        public int? usb_WhatDidYouDoLastYearOptionset {get; set; }
        public int? usb_EstablishCredentialsOptionset { get; set; }
        public int? usb_CertifiedFinPlannerOptionset { get; set; }
        public int? usb_AlignForFinPlanningOptionset { get; set; }
        public int? usb_CommunityFinPlannerOptionset { get; set; }
        public int? usb_ExcellentStudyMaterialOptionset { get; set; }
        public int? usb_ExcellentSupervisionOptionset { get; set; }
        public int? usb_GoodMealsOptionset { get; set; }
        public string usb_Referee1_Cellphone { get; set; }
        public string usb_Referee1_Position { get; set; }
        public string usb_Referee1_Name { get; set; }
        public string usb_Referee1_Telephone { get; set; }
        public string usb_Referee1_Email { get; set; }
        public string usb_Referee2_Cellphone {get; set; }
        public string usb_Referee2_Position {get; set; }
        public string usb_Referee2_Name {get; set; }
        public string usb_Referee2_Telephone {get; set; }
        public string usb_Referee2_Email {get; set; }
        public int? usb_salaryOptionset {get; set; }

        public int? usb_rplinterviewedby {get; set; }
        public int? usb_RPLCandidateTwoOptions {get; set; }

        public string TotalYearsWorkingExperience { get; set; }

        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string MobileNumber { get; set; }
        public string WorkNumber { get; set; }
        public string FaxNumber { get; set; }

        public string PostalStreet1 { get; set; }
        public string PostalStreet2 { get; set; }
        public string PostalStreet3 { get; set; }
        public string PostalSuburb { get; set; }
        public string PostalCity { get; set; }
        public string PostalPostalCode { get; set; }
        public Guid?  PostalAfrigis { get; set; }
        public Guid?  PostalCountry { get; set; }
        public string PostalStateOrProvince { get; set; }
        public string PostalAddressedTo { get; set; }

        public string ResidentialStreet1 { get; set; }
        public string ResidentialStreet2 { get; set; }
        public string ResidentialStreet3 { get; set; }
        public string ResidentialSuburb { get; set; }
        public string ResidentialCity { get; set; }
        public string ResidentialPostalCode { get; set; }
        public Guid?  ResidentialAfrigis { get; set; }
        public Guid?  ResidentialCountry { get; set; }
        public string ResidentialStateOrProvince { get; set; }

        public string BusinessStreet1 { get; set; }
        public string BusinessStreet2 { get; set; }
        public string BusinessStreet3 { get; set; }
        public string BusinessSuburb { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessPostalCode { get; set; }
        public Guid? BusinessAfrigis { get; set; }
        public Guid? BusinessCountry { get; set; }
        public string BusinessStateOrProvince { get; set; }

        public string BillingAddressedTo { get; set; }
        public string BillingStreet1 { get; set; }
        public string BillingStreet2 { get; set; }
        public string BillingStreet3 { get; set; }
        public string BillingSuburb { get; set; }
        public string BillingCity { get; set; }
        public string BillingPostalCode { get; set; }
        public Guid? BillingAfrigis { get; set; }
        public Guid? BillingCountry { get; set; }
        public string BillingStateOrProvince { get; set; }

        public int? IsRplCandidate { get; set; }
        
        public int? EnglishProficiencyRead { get; set; }
        public int? EnglishProficiencyWrite { get; set; }
        public int? EnglishProficiencyUnderstand { get; set; }
        public int? EnglishProficiencyTalk { get; set; }

        public int? MathematicsCompetency { get; set; }
        public string MathematicsPercentage { get; set; }

        public string WorkEmail { get; set; }
        public string WorkStudiesPersonName { get; set; }
        public string WorkContactTelephone { get; set; }

        public int? AnnualSalary { get; set; }

        public List<TertiaryEducationHistory> Qualification { get; set; }
        public List<EmploymentHistory> EmploymentHistory { get; set; }
        
        public int MarketingSource { get; set; }
        public string MarketingSourceOther { get; set; }
        public int MarketingReason { get; set; }
        public string MarketingReasonOther { get; set; }

        public Guid? PaymentResponsibility { get; set; }
        public string PaymentContactPerson { get; set; }
        public string PaymentContactNumber { get; set; }
        public string PaymentContactEmail { get; set; }
        public string PaymentDebtorCode { get; set; }

        public string PaymentCompanyName { get; set; }
        public string PaymentEmployerJobTitle { get; set; }
        public int? EmployerWorkAreas { get; set; }

        public string UsNumber { get; set; }
        public Guid WebTokenId { get; set; }

        #region marketing
        
        public bool? infoSessionAttended { get; set; }
        public int? usb_InfoSessionOptionSet { get; set; }

        public bool checkboxAdvertisement { get; set; }
        public bool checkboxNewsArticle { get; set; }
        public bool checkboxUsbPromotionalEmails { get; set; }
        public bool checkboxConferenceOrExpo { get; set; }
        public bool checkboxReferralByMyEmployer { get; set; }
        public bool checkboxStellenboschUniversity { get; set; }
        public bool checkboxReferralByAnAlumnus { get; set; }
        public bool checkboxUsbWebsite { get; set; }
        public bool checkboxReferralByACurrentStudent { get; set; }
        public bool checkboxUsbEd { get; set; }
        public bool checkboxReferralByAFamilyMember { get; set; }
        public bool checkboxBrochure { get; set; }
        public bool checkboxSocialMedia { get; set; }
        public bool checkboxOnCampusEvent { get; set; }


        public bool checkboxEarnMoreMoney { get; set; }

        public bool checkboxGiveMyChildABetterFuture { get; set; }

        public bool checkboxIncreaseMyStatus { get; set; }

        public bool checkboxMakeMyParentsProud { get; set; }

        public bool checkboxProvideStability { get; set; }

        public bool checkboxGetAPromotion { get; set; }

        public bool checkboxQualifyForOportunities { get; set; }

        public bool checkboxAdvanceInCareer { get; set; }

        public bool checkboxHaveMoreControl { get; set; }

        public bool checkboxHaveSatisfyingCareer { get; set; }

        public bool checkboxBetterNetworkingOportunities { get; set; }

        public bool checkboxImproveSkills { get; set; }

        public bool checkboxImproveLeadershipSkills { get; set; }

        public bool checkboxQualifyToWorkAtOtherCompanies { get; set; }

        public bool checkboxForeignWorkOportunities { get; set; }

        public bool checkboxCatchUpWithPeers { get; set; }

        public bool checkboxStartOwnBusiness { get; set; }

        public bool checkboxLearnSomethingDifferent { get; set; }

        public bool checkboxGetRespect { get; set; }

        public bool checkboxRoleModel { get; set; }

        public bool checkboxStandOut { get; set; }

        public bool checkboxImproveSocioEconomicStatus { get; set; }

        public bool checkboxDevelopSkills { get; set; }

        public bool checkboxKeepUpWithChangingWorld { get; set; }

        public bool checkboxHaveMoreInfluence { get; set; }

        public bool checkboxGainInternationalExposure { get; set; }

        public bool checkboxManagementJob { get; set; }

        public bool checkboxBecomeAnExpert { get; set; }

        public bool checkboxReinventMyself { get; set; }

        public bool checkboxIncreaseConfidence { get; set; }

        public bool checkboxOvercomeSocialBarriers { get; set; }



        public bool checkboxCommunicateFromHome { get; set; }

        public bool checkboxLocatedInCurrentCountry { get; set; }

        public bool checkboxLocationThatIWouldLike { get; set; }

        public bool checkboxExcellentAcademic { get; set; }

        public bool checkboxGoodReputationForBusiness { get; set; }

        public bool checkboxGraduatesAreMoreSuccessful { get; set; }

        public bool checkboxHasTheSpecificProgram { get; set; }

        public bool checkboxHighlyRankedSchool { get; set; }

        public bool checkboxParentsGraduated { get; set; }

        public bool checkboxWellKnownInternationally { get; set; }

        public bool checkboxItsAlumniIncludeMany { get; set; }

        public bool checkboxHighQualityInstructors { get; set; }

        public bool checkboxGraduatesFromThisSchool { get; set; }

        public bool checkboxRecommendedByEmployer { get; set; }

        public bool checkboxRecommenedByFriends { get; set; }

        public bool checkboxEaseOfFittingIn { get; set; }

        public bool checkboxOnlyTheBestStudents { get; set; }

        public bool checkboxLowerTuitionCosts { get; set; }

        public bool checkboxOfferGenerousScholarships { get; set; }

        public bool checkboxADegreeFromThisSchool { get; set; }

        public bool checkboxOffersGoodStudentExperience { get; set; }

        public bool checkboxGoodOnCampusCareer { get; set; }

        public bool checkboxModernFacilities { get; set; }

        public bool checkboxOffersOnlineClasses { get; set; }

        public bool checkboxHasAStrongAlumniNetwork { get; set; }

        #endregion

        public List<int> DocumentUploadKeys { get; set; }
        public List<string> DocumentUploadNames { get; set; }

        public List<int> DocumentExclusionKeys { get; set; }

        public string ApplicationFormsBaseUrl { get; set; }

    }
}
