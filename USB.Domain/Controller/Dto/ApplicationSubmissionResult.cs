﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class ApplicationSubmissionResult
    {
        public string UsNumber { get; set; }

        public Guid? EnrolmentId { get; set; }

        public Guid? ContactId { get; set; }

        //public Guid? ErrorId { get; set; }
        public string ErrorId { get; set; }

        public bool Successful
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }
    }
}
