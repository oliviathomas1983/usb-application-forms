﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public enum ApplicantStatus: int
    {
        Applicant               = 1,

        ApplicationCompleted    = 864480000,

        InterviewRound1         = 864480001,
        SelectionCommittee      = 864480002,
        InterviewRound2         = 864480003,
        
        Cancelled               = 864480004,
        NotAdmitted             = 864480005,
        
        WaitingList             = 864480006,

        RequestAdmitted         = 864480015, 
        
        Admitted                = 864480007,
        
        FailedAdmitted          = 864480016,

        Postponed               = 864480008,

        RequestNotAccepted      = 864480017,
        NotAccepted             = 864480009,
        FailedNotAccepted       = 864480018,
        
        Accepted                = 864480010,
        Registered              = 864480011,

        Unsuccessful            = 864480012,
        Discontinued            = 864480013,
        
        Graduated               = 864480014,

        Deactivated             = 2
    }
}
