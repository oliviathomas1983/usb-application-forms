﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public partial class EnrolmentMapper
    {
        public static MarketingModel MapToMarketingModel(usb_studentacademicrecord studentAcademicRecord)
        {
            return new MarketingModel()
            {
                checkboxAdvertisement = studentAcademicRecord.usb_advertisementTwoOptions.GetValueOrDefault(),
                checkboxNewsArticle = studentAcademicRecord.usb_NewsArticleTwoOptions.GetValueOrDefault(),
                checkboxUsbPromotionalEmails = studentAcademicRecord.usb_PromotionalEmailsTwoOptions.GetValueOrDefault(),
                checkboxReferralByMyEmployer =
                    studentAcademicRecord.usb_ReferralByEmployerTwoOptions.GetValueOrDefault(),
                checkboxStellenboschUniversity =
                    studentAcademicRecord.usb_StellenboschUniversityTwoOptions.GetValueOrDefault(),
                checkboxReferralByAnAlumnus = studentAcademicRecord.usb_ReferralByAlumnusTwoOptions.GetValueOrDefault(),
                checkboxUsbWebsite = studentAcademicRecord.usb_USBWebsiteTwoOptions.GetValueOrDefault(),
                checkboxReferralByACurrentStudent =
                    studentAcademicRecord.usb_ReferralByCurrentStudentTwoOptions.GetValueOrDefault(),
                checkboxUsbEd = studentAcademicRecord.usb_usb_USBEdTwoOptions.GetValueOrDefault(),
                checkboxReferralByAFamilyMember =
                    studentAcademicRecord.usb_ReferralByFamilyFriendTwoOptions.GetValueOrDefault(),
                checkboxBrochure = studentAcademicRecord.usb_BrochureTwoOptions.GetValueOrDefault(),
                checkboxSocialMedia = studentAcademicRecord.usb_SocialMediaTwoOptions.GetValueOrDefault(),
                checkboxConferenceOrExpo = studentAcademicRecord.usb_ConferenceExpoTwoOptions.GetValueOrDefault(),
                checkboxOnCampusEvent = studentAcademicRecord.usb_OnCampusEventTwoOptions.GetValueOrDefault(),

                checkboxEarnMoreMoney = studentAcademicRecord.usb_EarnMoreMoneyTwoOptions.GetValueOrDefault(),
                checkboxGiveMyChildABetterFuture =
                    studentAcademicRecord.usb_GiveChildrenBetterFutureTwoOptions.GetValueOrDefault(),
                checkboxIncreaseMyStatus = studentAcademicRecord.usb_IncreaseStatusAmongTwoOptions.GetValueOrDefault(),
                checkboxMakeMyParentsProud = studentAcademicRecord.usb_MakeMyParentsProudTwoOptions.GetValueOrDefault(),
                checkboxProvideStability = studentAcademicRecord.usb_ProvideStabilityCareeTwoOptions.GetValueOrDefault(),
                checkboxGetAPromotion = studentAcademicRecord.usb_GetAPromotionTwoOptions.GetValueOrDefault(),
                checkboxQualifyForOportunities =
                    studentAcademicRecord.usb_QualifyForOpportunityTwoOptions.GetValueOrDefault(),
                checkboxAdvanceInCareer = studentAcademicRecord.usb_AdvanceCareerTwoOptions.GetValueOrDefault(),
                checkboxHaveMoreControl = studentAcademicRecord.usb_HaveMoreControlTwoOptions.GetValueOrDefault(),
                checkboxHaveSatisfyingCareer =
                    studentAcademicRecord.usb_HaveFulfilingCareerTwoOptions.GetValueOrDefault(),
                checkboxBetterNetworkingOportunities =
                    studentAcademicRecord.usb_BetterNetworkingTwoOptions.GetValueOrDefault(),
                checkboxImproveSkills = studentAcademicRecord.usb_ImproveSpecificSkillsTwoOptions.GetValueOrDefault(),
                checkboxImproveLeadershipSkills =
                    studentAcademicRecord.usb_ImproveLeadershipSkillsTwoOptions.GetValueOrDefault(),
                checkboxQualifyToWorkAtOtherCompanies =
                    studentAcademicRecord.usb_QualifyAtOtherCompaniesTwoOptions.GetValueOrDefault(),
                checkboxForeignWorkOportunities =
                    studentAcademicRecord.usb_AccessEmploymentTwoOptions.GetValueOrDefault(),
                checkboxCatchUpWithPeers = studentAcademicRecord.usb_HelpKeepUpToPeersTwoOptions.GetValueOrDefault(),
                checkboxStartOwnBusiness = studentAcademicRecord.usb_StartRunOwnBusinessTwoOptions.GetValueOrDefault(),
                checkboxLearnSomethingDifferent =
                    studentAcademicRecord.usb_LearnSomethingdifferentTwoOptions.GetValueOrDefault(),
                checkboxGetRespect = studentAcademicRecord.usb_GetMoreRespectTwoOptions.GetValueOrDefault(),
                checkboxRoleModel = studentAcademicRecord.usb_BeARoleModelTwoOptions.GetValueOrDefault(),
                checkboxStandOut = studentAcademicRecord.usb_StandOutFromOthersTwoOptions.GetValueOrDefault(),
                checkboxImproveSocioEconomicStatus =
                    studentAcademicRecord.usb_ImproveSocioEconomicTwoOptions.GetValueOrDefault(),
                checkboxDevelopSkills = studentAcademicRecord.usb_DevelopSkillsToHaveTwoOptions.GetValueOrDefault(),
                checkboxKeepUpWithChangingWorld =
                    studentAcademicRecord.usb_KeepUpWithTheFastTwoOptions.GetValueOrDefault(),
                checkboxHaveMoreInfluence = studentAcademicRecord.usb_HaveMoreInflueneceTwoOptions.GetValueOrDefault(),
                checkboxGainInternationalExposure =
                    studentAcademicRecord.usb_GainInternationalExposureTwoOptions.GetValueOrDefault(),
                checkboxManagementJob = studentAcademicRecord.usb_MoveIntoManagementTwoOptions.GetValueOrDefault(),
                checkboxBecomeAnExpert = studentAcademicRecord.usb_BecomeExpertTwoOptions.GetValueOrDefault(),
                checkboxReinventMyself = studentAcademicRecord.usb_ReinventMyselfTwoOptions.GetValueOrDefault(),
                checkboxIncreaseConfidence = studentAcademicRecord.usb_IncreaseConfidenceTwoOptions.GetValueOrDefault(),
                checkboxOvercomeSocialBarriers = studentAcademicRecord.usb_OvercomeSocialTwoOptions.GetValueOrDefault(),


                checkboxCommunicateFromHome = studentAcademicRecord.usb_NearbyCanCommuteTwoOptions.GetValueOrDefault(),
                checkboxLocatedInCurrentCountry = studentAcademicRecord.usb_LocatedInMyTwoOptions.GetValueOrDefault(),
                checkboxLocationThatIWouldLike =
                    studentAcademicRecord.usb_LocationThatTWouldTwoOptions.GetValueOrDefault(),
                checkboxExcellentAcademic = studentAcademicRecord.usb_ExcellentAcademicTwoOptions.GetValueOrDefault(),
                checkboxGoodReputationForBusiness =
                    studentAcademicRecord.usb_GoodReputationForTwoOptions.GetValueOrDefault(),
                checkboxGraduatesAreMoreSuccessful =
                    studentAcademicRecord.usb_GraduateAreMoreTwoOptions.GetValueOrDefault(),
                checkboxHasTheSpecificProgram =
                    studentAcademicRecord.usb_HasSpecificProgramTwoOptions.GetValueOrDefault(),
                checkboxHighlyRankedSchool = studentAcademicRecord.usb_HighRankedSchoolTwoOptions.GetValueOrDefault(),
                checkboxParentsGraduated = studentAcademicRecord.usb_ParentGraduatedTwoOptions.GetValueOrDefault(),
                checkboxWellKnownInternationally = studentAcademicRecord.usb_WellKnownTwoOptions.GetValueOrDefault(),
                checkboxItsAlumniIncludeMany = studentAcademicRecord.usb_AlumniIncludeManyTwoOptions.GetValueOrDefault(),
                checkboxHighQualityInstructors =
                    studentAcademicRecord.usb_HighQualityFacultyTwoOptions.GetValueOrDefault(),
                checkboxGraduatesFromThisSchool =
                    studentAcademicRecord.usb_GraduatesFromSchoolTwoOptions.GetValueOrDefault(),
                checkboxRecommendedByEmployer =
                    studentAcademicRecord.usb_RecommendedByEmployerTwoOptions.GetValueOrDefault(),
                checkboxRecommenedByFriends =
                    studentAcademicRecord.usb_RecommendedByColleguesTwoOptions.GetValueOrDefault(),
                checkboxEaseOfFittingIn = studentAcademicRecord.usb_EaseOfFittingInTwoOptions.GetValueOrDefault(),
                checkboxOnlyTheBestStudents = studentAcademicRecord.usb_OnlyBestStudentsTwoOptions.GetValueOrDefault(),
                checkboxLowerTuitionCosts = studentAcademicRecord.usb_LowerTuitionCostTwoOptions.GetValueOrDefault(),
                checkboxOfferGenerousScholarships =
                    studentAcademicRecord.usb_usb_OffersGenerousScholarTwoOptions.GetValueOrDefault(),
                checkboxADegreeFromThisSchool =
                    studentAcademicRecord.usb_ADegreeFromThisSchoolTwoOptions.GetValueOrDefault(),
                checkboxOffersGoodStudentExperience =
                    studentAcademicRecord.usb_OfferAGoodStudentTwoOptions.GetValueOrDefault(),
                checkboxGoodOnCampusCareer = studentAcademicRecord.usb_GoodOnCampusCareerTwoOptions.GetValueOrDefault(),
                checkboxModernFacilities =
                    studentAcademicRecord.usb_StateOfTheArtFacilitiesTwoOptions.GetValueOrDefault(),
                checkboxOffersOnlineClasses =
                    studentAcademicRecord.usb_OffersOnlineClassesTwoOptoins.GetValueOrDefault(),
                checkboxHasAStrongAlumniNetwork =
                    studentAcademicRecord.usb_HasAStrongAlumniNetTwoOptions.GetValueOrDefault()
            };
        }

        public static Workstudies MapToWorkStudies(usb_studentacademicrecord studentAcademicRecord)
        {
            return new Workstudies()
            {
                IsRplCandidate = studentAcademicRecord.usb_RPLCandidateOptionset.GetValueOrDefault(),

                AnnualGrossSalary = studentAcademicRecord.usb_SalaryOptionset.GetValueOrDefault(),

                ContactPersonEmailAddress = studentAcademicRecord.usb_EmployerEmail,
                ContactPersonName = studentAcademicRecord.usb_EmployerName,
                ContactPersonTelephone = studentAcademicRecord.usb_EmployerTelephone,
                EmployerName = studentAcademicRecord.usb_EmployerCompany,
                EmployerJobTitle = studentAcademicRecord.usb_EmployerJobTitle,
                EmployerWorkAreas = studentAcademicRecord.usb_EmployerWorkArea,
                EnglishProficiencyReading = studentAcademicRecord.usb_EnglishReadOptionset,
                EnglishProficiencySpeaking = studentAcademicRecord.usb_EnglishSpeakOptionset,
                EnglishProficiencyUnderstanding = studentAcademicRecord.usb_EnglishUnderstandOptionset,
                EnglishProficiencyWriting = studentAcademicRecord.usb_EnglishWriteOptionset,
                MathematicsCompetency = studentAcademicRecord.usb_HighestLevelMathematicsCompetedOptionset,
                MathematicsPercentage = studentAcademicRecord.usb_SymbolAchieved,

                PriorInvolvement = studentAcademicRecord.usb_WhatDidYouDoLastYearOptionset,
                YearsOfWorkingExperience = studentAcademicRecord.usb_TotalYearsWorkingExperience,
                WillEmployerBeAssistingFinancially = studentAcademicRecord.usb_EmployerassistfinanciallyOptionset,

                //MathematicsPercentage = studentAcademicRecord.math
                Referee = new List<Referee>()
                {
                    new Referee()
                    {
                        RefereeCellPhone = studentAcademicRecord.usb_Referee1_Cellphone,
                        RefereeEmail = studentAcademicRecord.usb_Referee1_Email,
                        RefereeName = studentAcademicRecord.usb_Referee1_Name,
                        RefereePosition = studentAcademicRecord.usb_Referee1_Position,
                        RefereeTelephone = studentAcademicRecord.usb_Referee1_Telephone
                    },
                    new Referee()
                    {
                        RefereeCellPhone = studentAcademicRecord.usb_Referee2_Cellphone,
                        RefereeEmail = studentAcademicRecord.usb_Referee2_Email,
                        RefereeName = studentAcademicRecord.usb_Referee2_Name,
                        RefereePosition = studentAcademicRecord.usb_Referee2_Position,
                        RefereeTelephone = studentAcademicRecord.usb_Referee2_Telephone
                    }
                },
            };
        }
    }
}
