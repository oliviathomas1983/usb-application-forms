﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Domain.Models
{
    public enum ApplicationSource
    {
        AddressDetails,
        ContactDetails,
        WorkStudies,
        TellUsMore,
        Documentation,
        Payment,
        Status
    }
}
