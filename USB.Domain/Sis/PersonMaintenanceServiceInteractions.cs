﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using StellenboschUniversity.Usb.Integration.Crm;
using USB.Domain.Models;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public static class PersonMaintenanceServiceInteractions
    {
        public static string GetApplicantUSNumber(ApplicationSubmission application, string correspondenceLanguageCode)
        {
            var applicant = new Person();

            if (String.IsNullOrEmpty(application.FirstNames))
            {
                throw new Exception("First names of applicant are missing.");
            }

            if (String.IsNullOrEmpty(application.IdNumber) == false && String.IsNullOrEmpty(application.PassportNumber) == true && String.IsNullOrEmpty(application.ForeignIdNumber) == true)
            {
                applicant.SouthAfricanIdNumber = application.IdNumber.Trim();
            }
            else
            {
                applicant.SouthAfricanIdNumber = "NONE";
            }

            applicant.FirstNames = (String.IsNullOrEmpty(application.GivenName) == false) ? application.GivenName.Trim() : application.FirstNames.Trim();
            applicant.Surname = application.Surname.Trim();
            applicant.FirstNames = application.FirstNames.Trim();
            applicant.Initials = application.Initials.Trim().ToUpper();
            applicant.EmailAddress = application.Email.Trim();
            applicant.DateOfBirth = application.DateOfBirth.GetValueOrDefault();
            applicant.Title = CrmIdToSisCodeMapping.GetTitleSisCode(application.Title.GetValueOrDefault());
            applicant.Gender = CrmIdToSisCodeMapping.GetGenderSisCode(application.Gender.GetValueOrDefault());
            applicant.Ethnicity = CrmIdToSisCodeMapping.GetEthnicitySisCode(application.Ethnicity.GetValueOrDefault());
            applicant.Nationality = CrmIdToSisCodeMapping.GetNationalitySisCode(application.Nationality.GetValueOrDefault());
            applicant.CorrespondenceLanguage = correspondenceLanguageCode;
            applicant.CellphoneNumber = application.MobileNumber;
            applicant.GivenName = application.GivenName;
            applicant.Language = CrmIdToSisCodeMapping.GetLanguageSisCode(application.Language.GetValueOrDefault());
            applicant.MaidenName = application.MaidenName;
            applicant.WorkTelephoneNumber = application.WorkContactTelephone;

            if (String.IsNullOrEmpty(application.PassportNumber) == false)
            {
                applicant.PassportNumber = application.PassportNumber;
                applicant.PassportExpiryDate = application.PassportExpiryDate.HasValue ? application.PassportExpiryDate : null;
            }

            if (String.IsNullOrEmpty(application.ForeignIdNumber) == false)
            {
                applicant.ForeignIdNumber = application.ForeignIdNumber;
                applicant.PassportExpiryDate = application.PassportExpiryDate.HasValue ? application.PassportExpiryDate : null;
            }

            var reply = applicant.CreateApplicant(applicant);

            if ((reply.Success == false) || (reply.UsNumber == "0"))
            {
                throw new ApplicationException(String.Format("{0}", reply.Message));
            }
            else
            {
                return reply.UsNumber;
            }
        }

        //public static ApplicationSubmission GetSisPersonInformation(string usNumber)
        //{
        //    ApplicationSubmission result = new ApplicationSubmission();

        //    using (var personMaintenanceServiceClient = new PersMaintWSClient("PersonMaintenanceService"))
        //    {
        //        var personMaintenanceServiceReply = personMaintenanceServiceClient.getPersoon(PersonMaintenanceServiceUsername, PersonMaintenanceServicePassword, usNumber, personMaintenanceServiceLanguage);

        //        if ((personMaintenanceServiceReply.persoon == null) || (personMaintenanceServiceReply.persoon.usNommer == "0"))
        //        {
        //            throw new ApplicationException(
        //                String.Format("Could not get persoon details of student number from SIS.  SIS Error Code: {0}; SIS Error Message: {1}",
        //                personMaintenanceServiceReply.errorCode,
        //                personMaintenanceServiceReply.errorText));
        //        }
        //        else
        //        {
        //            var person = personMaintenanceServiceReply.persoon;

        //            result.UsNumber = person.usNommer.Trim();
        //            result.FirstNames = person.usVoorname.ToTitleCase();
        //            result.Surname = person.usVan.ToTitleCase();
        //            result.GivenName = person.usNoemnaam.ToTitleCase();
        //            result.Initials = person.usVoorletters.Trim();
        //            result.IdNumber = ((person.usIdNommer == "GEEN") || (person.usIdNommer == "NONE")) ? "" : person.usIdNommer.Trim();
        //            result.PassportNumber = person.usPaspoortNr.Trim();
        //            result.PassportExpiryDate = person.usPaspoortVervaldat.ConvertFromSisDate();
        //            result.ContactNumber = person.selfoonNommer.Trim();
        //            result.FaxNumber = person.usFaksSkakel.Trim() + person.usFaksNr.Trim();
        //            result.Email = person.usEmailAdres.Trim().ToLower();
        //            result.DateOfBirth = person.usGebDatum.ConvertFromSisDate().Value;
        //            result.Nationality = SisCodeToCrmIdMapping.GetNationalityCrmId(person.usNasionaliteitkode);
        //            result.Title = SisCodeToCrmIdMapping.GetTitleCrmId(person.usTitelkode);
        //            result.Gender = SisCodeToCrmIdMapping.GetGenderCrmId(person.usGeslagkode);
        //            result.Ethnicity = SisCodeToCrmIdMapping.GetEthnicityCrmId(person.usRasKode);
        //            result.Language = SisCodeToCrmIdMapping.GetLanguageCrmId(person.usTaalkode);

        //            // get postal address
        //            using (var addressServiceClient = new AddressWebServiceClient("AddressInfrastructureService"))
        //            {
        //                var addressServiceReply = addressServiceClient.getAddress(usNumber, postalAddressType, addressServiceLanguage);

        //                if (addressServiceReply.status.Contains("SUCCESSFUL"))    // success
        //                {
        //                    var address = addressServiceReply.returnObject as AddressData;

        //                    result.PostalStreet1 = address.adrLine1.ToTitleCase();
        //                    // TODO: check what to do with address lines
        //                    //result. = SisUtilities.ToTitleCase(addressQueryServiceReply.adrLine2);
        //                    //userInformation["address3"] = SisUtilities.ToTitleCase(addressQueryServiceReply.adrLine3);
        //                    result.PostalPostalCode = address.zipCode.Trim();
        //                    // TODO: check below
        //                    //result.PostalCountry = SisUtilities.ToTitleCase(addressQueryServiceReply.country);
        //                    result.PostalCity = address.town.ToTitleCase();
        //                    result.PostalSuburb = address.suburb.ToTitleCase();

        //                    if ((address.afrigisCode != "0") && (address.afrigisCode != ""))
        //                    {
        //                        result.PostalAfrigis = SisCodeToCrmIdMapping.GetAfrigisLocationFromCode(address.afrigisCode.TrimStart(new char[] { '0' })).Id;
        //                    }
        //                }
        //            }

        //            // get courier address
        //            using (var addressServiceClient = new AddressWebServiceClient("AddressInfrastructureService"))
        //            {
        //                var addressServiceReply = addressServiceClient.getAddress(usNumber, courierAddressType, addressServiceLanguage);

        //                if (addressServiceReply.status.Contains("SUCCESSFUL"))    // success
        //                {
        //                    var address = addressServiceReply.returnObject as AddressData;

        //                    result.ResidentialStreet1 = address.adrLine1.ToTitleCase();
        //                    // TODO: check what to do with address lines
        //                    //result. = SisUtilities.ToTitleCase(addressQueryServiceReply.adrLine2);
        //                    //userInformation["address3"] = SisUtilities.ToTitleCase(addressQueryServiceReply.adrLine3);    
        //                    result.ResidentialPostalCode = address.zipCode.Trim();
        //                    // TODO: check below
        //                    //result.PostalCountry = SisUtilities.ToTitleCase(addressQueryServiceReply.country);
        //                    result.ResidentialCity = address.town.ToTitleCase();
        //                    result.ResidentialSuburb = address.suburb.ToTitleCase();

        //                    if ((address.afrigisCode != "0") && (address.afrigisCode != ""))
        //                    {
        //                        result.ResidentialAfrigis = SisCodeToCrmIdMapping.GetAfrigisLocationFromCode(address.afrigisCode.TrimStart(new char[] { '0' })).Id;
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return result;
        //}
    }
}
