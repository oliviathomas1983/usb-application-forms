﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public static class Cache
    {
        private static FileCache cache;
        private static TimeSpan expirationAge = new TimeSpan(24, 0, 0);

        static Cache()
        {
            cache = new FileCache(System.IO.Path.GetTempPath());

            Flush();
        }

        public static void AddOrReplace(string collectionKey, string itemKey, Tuple<string, byte[]> item)
        {
            Flush();

            Dictionary<string, Tuple<string, byte[]>> dictionary;

            if(!cache.Contains(collectionKey)) 
            {
                dictionary = new Dictionary<string, Tuple<string, byte[]>>();
                dictionary.Add(itemKey, item);
            } 
            else 
            {
                dictionary = (Dictionary<string, Tuple<string, byte[]>>)cache[collectionKey];

                if(!dictionary.ContainsKey(itemKey))
                {
                    dictionary.Add(itemKey, item);
                }
                else
                {
                    dictionary[itemKey] = item;
                }
            }

            cache.Set(collectionKey, dictionary, cache.DefaultPolicy);
        }

        public static IDictionary<string, Tuple<string, byte[]>> GetCachedCollection(string key)
        {
            Flush();

            return (IDictionary<string, Tuple<string, byte[]>>)cache[key];
        }

        public static Tuple<string, byte[]> GetCachedItem(string collectionKey, string itemKey)
        {
            Flush();

            return ((Dictionary<string, Tuple<string, byte[]>>)cache[collectionKey])[itemKey];
        }

        public static void RemoveItem(string collectionKey, string itemKey)
        {
            if ( ContainsItem(collectionKey, itemKey) )
            {
                var dictionary = (Dictionary<string, Tuple<string, byte[]>>)cache[collectionKey];

                dictionary.Remove(itemKey);

                cache.Set(collectionKey, dictionary, cache.DefaultPolicy);
            }
        }

        public static bool ContainsCollectionKey(string collectionKey)
        {
            return cache.Contains(collectionKey);
        }

        public static bool ContainsItem(string collectionKey, string itemKey)
        {
            return cache.Contains(collectionKey) &&
                   ((Dictionary<string, Tuple<string, byte[]>>)cache[collectionKey]).ContainsKey(itemKey);
        }

        public static bool ContainsItemWithFileName(string collectionKey, string fileName)
        {
            if (cache.Contains(collectionKey))
            {
                var dictionary = GetCachedCollection(collectionKey);

                foreach (var item in dictionary)
                {
                    if (item.Value.Item1 == fileName)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static void Flush()
        {
            cache.Flush(DateTime.Now.Subtract(expirationAge));
        }
    }
}
