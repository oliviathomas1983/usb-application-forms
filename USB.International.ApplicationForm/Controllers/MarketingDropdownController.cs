﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers;
using USB.International.ApplicationForm.ViewModels;

namespace USB.International.ApplicationForm.Controllers
{
    public class MarketingDropdownController : BaseController
    {
        [HttpGet]
        public ActionResult LoadDropdown(string type)
        {
            var dropdownList = new List<KeyValuePair<string, int>>();
            var controller = new ApplicationFormApiController();

            switch (type)
            {
                case "infoSessions": ;
                    dropdownList = controller.InfoSessions().ToList();
                    break;
        
                default: ;
                    break;
            }

            return Json(new { dropdownList }, JsonRequestBehavior.AllowGet);
        }
    }
}
