﻿var knockoutValidationSettings = {
    insertMessages: false,
    decorateElement: true,
    errorElementClass: 'input-validation-error',
    messagesOnModified: true,
    decorateElementOnModified: true,
    decorateInputElement: true
};

ko.validation.configuration = knockoutValidationSettings;

function WorkAndStudies(isDisabled) {
    var self = this;

    var rootUrl = "/International/";

    var yearList = [""];
    var start = new Date().getFullYear() - 80;
    var end = new Date().getFullYear();
    var options = "";

    for (var year = start ; year <= end; year++) {
        options += year;
        yearList.push(year);
    }

    self.redirect_url = rootUrl + "Page/Documentation";

    self.loading = ko.observableArray();

    self.loading.subscribe(function () {
        if (self.loading().length === 0) {
            // Last dropdown population async ajax call has ended

            // Fill in inputs if existing informantion exists
            self.loadExistingWorkStudies();
        }
    });

    self.isDisabled = ko.observable((isDisabled !== null) ? isDisabled : false);

    //Academic Information
    self.HomeInstitution = ko.observable();
    self.ContactPersonName = ko.observable();
    self.ContactPersonEmailAddress = ko.observable();
    self.PrincipleFieldOfStudy = ko.observable();
    self.YearsCompletedAtHomeInstitution = ko.observable().extend({ min: 0, max: 99, number: true });
    self.DurationOfExchange = ko.observable();

    //Observable Arrays
    self.principleFieldOfStudyList = ko.observableArray();
    self.durationOfExchangeList = ko.observableArray();
    self.institutionsList = ko.observableArray();
    self.fieldOfStudyList = ko.observableArray();
    self.workAreaList = ko.observableArray();
    self.organizationList = ko.observableArray();
    self.industryList = ko.observableArray();
    self.employmentTypeList = ko.observableArray();

    self.years = function() {
        var start = 0;
        var end = 100;
        var arr = [];

        while (start <= end) {
            arr.push(start++);
        }

        return arr;
    };

    self.allMonths = function () {
        var start = 0;
        var end = 12;
        var arr = [];

        while (start <= end) {
            arr.push(start++);
        }

        return arr;
    };

    self.yearsOfWorkingExperienceList = ko.observableArray(self.years());
    self.monthsOfWorkingExperienceList = ko.observableArray(self.allMonths());
    
    self.YearsOfWorkingExperience = ko.observable().extend({ min: 0, max: 99, number: true });
    self.MonthsOfWorkingExperience = ko.observable().extend({ min: 0, max: 11, number: true });

    self.availableYears = ko.observableArray(yearList);

    self.serverErrorMessage = ko.observable();

    //Tertiary Education
    var ObservableTertiaryEducationHistory = function (historyItem) {
        var te_self = this;

        te_self.Id = historyItem.Id;

        te_self.Institution = ko.observable(historyItem.Institution).extend({ required: true });
        te_self.OtherInstitution = ko.observable(historyItem.OtherInstitution);
        te_self.Degree = ko.observable(historyItem.Degree).extend({ required: true });
        te_self.FieldOfStudy = ko.observable(historyItem.FieldOfStudy).extend({ required: true });
        te_self.StudentNumber = ko.observable(historyItem.StudentNumber).extend({ required: true });
        te_self.StartDate = ko.observable(historyItem.StartDate).extend({ required: true });
        te_self.EndDate = ko.observable(historyItem.EndDate).extend({ required: true, min: te_self.StartDate });
        te_self.Completed = ko.observable(historyItem.Completed).extend({ required: false });
        te_self.First = ko.observable(historyItem.First).extend({ required: true });
        te_self.isOtherInstitution = ko.observable();

        if (!te_self.OtherInstitution()) {
            te_self.isOtherInstitution(false);
        }
        else {
            te_self.isOtherInstitution(te_self.OtherInstitution().length > 0);
        }
    };

    self.TertiaryEducationHistory = ko.observableArray();
    self.ContactPersonName = ko.observable();
    self.ContactPersonEmailAddress = ko.observable();

    function AddTertiaryEducation() {
        self.TertiaryEducationHistory.push(new ObservableTertiaryEducationHistory({
            'Institution': '',
            'OtherInstitution': '',
            'Degree': '',
            'FieldOfStudy': '',
            'StudentNumber': '',
            'StartDate': '',
            'EndDate': '',
            'Completed': false,
            'First': false,
            'isOtherInstitution': false
        }));
    };

    //Employement History
    var ObservableEmploymentHistory = function (historyItem) {
        var oe_self = this;

        oe_self.Id = historyItem.Id;

        oe_self.Employer = ko.observable(historyItem.Employer).extend({ required: true });
        oe_self.JobTitle = ko.observable(historyItem.JobTitle).extend({ required: true });
        oe_self.WorkArea = ko.observable(historyItem.WorkArea).extend({ required: true });
        oe_self.Organization = ko.observable(historyItem.Organization).extend({ required: true });
        oe_self.Industry = ko.observable(historyItem.Industry).extend({ required: true });
        oe_self.StartDate = ko.observable(historyItem.StartDate).extend({ required: true });
        oe_self.EndDate = ko.observable(historyItem.EndDate).extend({ required: true, min: oe_self.StartDate });
        oe_self.Type = ko.observable(historyItem.Type).extend({ required: true });
        oe_self.Latest = ko.observable(historyItem.Latest).extend({ required: true });
    };

    self.EmploymentHistory = ko.observableArray();

    function AddEmploymentHistory() {
        self.EmploymentHistory.push(new ObservableEmploymentHistory({
            'Employer': '',
            'JobTitle': '',
            'WorkArea': '',
            'Organization': '',
            'Industry': '',
            'StartDate': '',
            'EndDate': '',
            'Type': '',
            'Latest': false
        }));
    }

    self.addTertiaryEducationHistory = function () {
        AddTertiaryEducation();
    }.bind(self);

    self.addEmploymentHistory = function () {
        AddEmploymentHistory();
    }.bind(self);

    self.setRequiredFields = function () {
        self.HomeInstitution.extend({ required: true });
        self.ContactPersonName.extend({ required: true });
        self.ContactPersonEmailAddress.extend({ required: true });
        self.PrincipleFieldOfStudy.extend({ required: true });
        self.YearsCompletedAtHomeInstitution.extend({ required: true });
        self.MonthsOfWorkingExperience.extend({ required: true });
        self.DurationOfExchange.extend({ required: true });
    };

    var WorkAndStudies = {
        HomeInstitution: self.HomeInstitution,
        ContactPersonName: self.ContactPersonName,
        ContactPersonEmailAddress: self.ContactPersonEmailAddress,
        PrincipleFieldOfStudy: self.PrincipleFieldOfStudy,
        YearsCompletedAtHomeInstitution: self.YearsCompletedAtHomeInstitution,
        DurationOfExchange: self.DurationOfExchange,
        YearsOfWorkingExperience: self.YearsOfWorkingExperience,
        MonthsOfWorkingExperience: self.MonthsOfWorkingExperience,
        TertiaryEducationHistory: self.TertiaryEducationHistory,
        EmploymentHistory: self.EmploymentHistory
    };

    WorkAndStudies.errors = ko.validation.group(WorkAndStudies, { deep: true, observable: true, live: true });

    self.loadDropDowns = function () {
        self.loadDropDown("principleFieldOfStudy");
        self.loadDropDown("durationOfExchange");
        self.loadDropDown("institution");
        self.loadDropDown("fieldOfStudy");
        self.loadDropDown("workArea");
        self.loadDropDown("organization");
        self.loadDropDown("industry");
        self.loadDropDown("employmentType");
        self.loadDropDown("grossSalary");
    };

    self.loadDropDown = function (type) {
        self.loading.push(true);
        $.ajax({
            url: rootUrl + "WorkStudiesDropdown/LoadDropdown?type=" + type,
            type: 'GET',
            contentType: "application/json",
            success: function (j) {
                self.populateDropDownList(j.list, type);
            },
            error: function (error) {
                console.log(error.responseText);
            }
        }).always(function () {
            self.loading.pop();
        });
    };

    self.populateDropDownList = function (list, type) {

        switch (type) {
            case "institution":
                self.institutionsList(list);
                break;
            case "organization":
                self.organizationList(list);
                break;
            case "workArea":
                self.workAreaList(list);
                break;
            case "fieldOfStudy":
                self.fieldOfStudyList(list);
                break;
            case "industry":
                self.industryList(list);
                break;
            case "employerAssistFinancially":
                self.employerAssistFinanciallyList(list);
                break;
            case "employmentType":
                self.employmentTypeList(list);
            case "principleFieldOfStudy":
                self.principleFieldOfStudyList(list);
                break;
            case "durationOfExchange":
                self.durationOfExchangeList(list);
                break;
            default:
                break;
        }
    }

    $(function () {
        waitingDialog.show("Preparing Work and Studies Form");

        self.setRequiredFields();
        self.loadDropDowns();
    });

    self.loadExistingWorkStudies = function () {
        $.ajax({
            url: rootUrl + "Page/PopulateWorkStudies",
            type: 'POST',
            contentType: "application/json",
            //data: AddAntiForgeryToken({}),
            success: function(data) {
                if (data.HomeInstitution !== null) {

                    var mapping = {
                        'ignore': ["TertiaryEducationHistory", "EmploymentHistory"]
                    }

                    ko.mapping.fromJS(data, mapping, self);

                    if (data.EmploymentHistory.length > 0) {
                        self.EmploymentHistory(ko.utils.arrayMap(data.EmploymentHistory,
                            function(item) {
                                return new ObservableEmploymentHistory(item);
                            }));
                    } else {
                        AddEmploymentHistory();
                    }

                    if (data.TertiaryEducationHistory.length > 0) {
                        self.TertiaryEducationHistory(ko.utils.arrayMap(data.TertiaryEducationHistory,
                            function(item) {
                                return new ObservableTertiaryEducationHistory(item);
                            }));
                    } else {
                        AddTertiaryEducation();
                    }
                } else {
                    AddEmploymentHistory();
                    AddTertiaryEducation();
                }
            }
        }).always(function () {
            waitingDialog.hide();
        });
    };

    self.StepThreeSaveAndProceed = function () {
        var data = ko.mapping.toJSON(WorkAndStudies);
        if (WorkAndStudies.errors().length === 0) {
            waitingDialog.show('Saving Work and Studies Details');
            $.ajax({
                url: rootUrl + "Page/SubmitWorkStudies",
                contentType: 'application/json; charset=utf-8',
                data: ko.mapping.toJSON(WorkAndStudies),
                type: "POST",
                success: function (data) {
                    console.log(data);
                    if (data.Success == false) {
                        self.serverErrorMessage(data.Message);
                        $('#serverValidationAlert').show();
                        window.scroll(0, 0);
                    }
                    else {
                        window.location.href = rootUrl + "Page/Documentation";
                    }
                },
                error: function (error) {
                    console.log(error.responseText);
                }
            }).done(function (response) {
                console.log(response);
            });
        } else {
            $('#requiredFieldsAlert').show();
            WorkAndStudies.errors.showAllMessages();
            window.scrollTo(0, 0);
        }
    }

    self.removeEducationHistoryItem = function (item) {
        self.TertiaryEducationHistory.remove(item);
    };

    self.removeEmploymentHistory = function (item) {
        self.EmploymentHistory.remove(item);
    };

    self.institutionChange = function (data) {
        var selectedValue = data.Institution;
        var selectOther = $('#otherUniversity option').filter(function () { return $(this).html() == "Other University"; }).val();
        if (selectedValue() === selectOther) {
            data.isOtherInstitution(true);
        }
        else {
            data.isOtherInstitution(false);
            data.OtherInstitution('');
        }
    };
}

function LoadKeyValuePairJSONObject(data, observableArray) {
    var jsonResult = data;
    var list = observableArray;
    for (key in data) {
        var item = {
            name: key,         // Push the key on the array
            value: jsonResult[key] // Push the key's value on the array
        };
        list.push(item);
    };
    return list;
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });

});
