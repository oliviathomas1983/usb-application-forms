﻿var Application = function()
{
    var instance = this;
    
    instance.personalDetails_Url = "/International/Page/PersonalDetails";
    instance.addressDetails_Url = "/International/Page/AddressDetails";
    instance.work_Study_Url = "/International/Page/WorkStudies";
    //instance.marketing_Url = "/International/Page/Marketing";
    instance.documentation_Url = "/International/Page/Documentation";
    //instance.payment_Url = "/International/Page/Payment";
    instance.status_Url = "/International/Page/Status";
     
    var activeTab = null;

    this.load = function () {
        instance.registerEvents();
        instance.setActiveTab();
    }

    this.registerEvents = function () {
        $('.navPersonalDetails').on('click', function () {
            window.location.href = instance.personalDetails_Url;
        });
        $('.navAddressDetails').on('click', function () {
            window.location.href = instance.addressDetails_Url;            
        });
        $('.navWorkAndStudyDetails').on('click', function () {
            window.location.href = instance.work_Study_Url;
        });
        //$('.navMarketingDetails').on('click', function () {
        //    window.location.href = instance.marketing_Url;
        //});
        $('.navDocumentationDetails').on('click', function () {
            window.location.href = instance.documentation_Url;
        });
        //$('.navPaymentDetails').on('click', function () {
        //    window.location.href = instance.payment_Url;
        //});
        $('.navStatusDetails').on('click', function () {
            window.location.href = instance.status_Url;
        });
    }

    this.setActiveTab = function () {
        var url = window.location.pathname;
        if (url == instance.personalDetails_Url) {
            $("#tab1").addClass('active');
            $('#currentPage').text('Page 1 of 5');
        }
        if (url == instance.addressDetails_Url) {
            $("#tab2").addClass('active');
            $('#currentPage').text('Page 2 of 5');
        }
        if (url == instance.work_Study_Url) {
            $("#tab3").addClass('active');
            $('#currentPage').text('Page 3 of 5');
        }
        if (url == instance.documentation_Url) {
            $("#tab4").addClass('active');
            $('#currentPage').text('Page 4 of 5');
        }
        if (url == instance.status_Url) {
            $("#tab5").addClass('active');
            $('#currentPage').text('Page 5 of 5');
        }
        //if (url == instance.payment_Url) {
        //    $("#tab6").addClass('active');
        //    $('#currentPage').text('Page 6 of 6');
        //}
        //if (url == instance.status_Url) {
        //    $("#tab7").addClass('active');
        //    $('#currentPage').text('Page 7 of 7');
        //}
    }

    AddAntiForgeryToken = function (data) {
        data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
        return data;
    };
}
