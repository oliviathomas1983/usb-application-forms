﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using StellenboschUniversity.Usb.Integration.Crm;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using USB.International.Domain.Controller.Dto;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public static class CrmActions
    {
        public static ApplicationSubmission CreateOrUpdateContact(string usNumber, ApplicationSubmission application, Guid? contactId)
        {
            var contact = new Contact()
            {
                Id = contactId.Value,
                usb_StudentNumber = usNumber.Trim()
            };

            #region Contact Details

            if (application.ApplicationSource == USB.International.Domain.Models.ApplicationSource.ContactDetails)
            {
                contact.BirthDate = application.DateOfBirth.ToUtcWithoutConversion();
                contact.LastName = application.Surname.Trim();
                contact.FirstName = application.FirstNames.Trim();
                contact.usb_Initials = application.Initials.Trim();
                contact.usb_GivenName = (String.IsNullOrEmpty(application.GivenName) == false) ? application.GivenName.Trim() : "";
                contact.usb_MaidenName = application.MaidenName ?? string.Empty;
                contact.usb_PassportNumber = application.PassportNumber;
                contact.usb_PassportExpiryDate = application.PassportExpiryDate.ToUtcWithoutConversion();

                if (application.Gender.HasValue)
                {
                    contact.usb_GenderLookup = new CrmEntityReference(usb_gender.EntityLogicalName, application.Gender.GetValueOrDefault());
                }
                if (application.Title.HasValue)
                {
                    contact.usb_TitleLookup = new CrmEntityReference(usb_Title.EntityLogicalName, application.Title.GetValueOrDefault());
                }
                if (application.Ethnicity.HasValue)
                {
                    contact.usb_EthnicityLookup = new CrmEntityReference(usb_ethnicity.EntityLogicalName, application.Ethnicity.GetValueOrDefault());
                }
                if (application.MaritalStatus.HasValue)
                {
                    contact.usb_MaritalStatusLookup = new CrmEntityReference(usb_maritalstatus.EntityLogicalName, application.MaritalStatus.GetValueOrDefault());
                }
                if (application.Nationality.HasValue)
                {
                    contact.usb_NationalityLookup = new CrmEntityReference(usb_Nationality.EntityLogicalName, application.Nationality.GetValueOrDefault());
                }
                if (application.IsSouthAfrican.GetValueOrDefault())
                {
                    contact.GovernmentId = application.IdNumber;
                }
                else
                {
                    if (application.CountryOfIssue != Guid.Empty)
                    {
                        contact.usb_PassportCountryIssuedLookup = new CrmEntityReference(usb_Nationality.EntityLogicalName, application.CountryOfIssue.GetValueOrDefault());
                    }
                }

                if (application.PermitType.HasValue && application.PermitType != Guid.Empty)
                {
                    contact.usb_PermitTypeLookup = new CrmEntityReference(usb_permittype.EntityLogicalName, application.PermitType.GetValueOrDefault());
                }

                if (application.Language.HasValue)
                {
                    Guid languageId = CrmQueries.GetHomeLanguage(application.Language.Value);
                    contact.usb_HomeLanguageLookup = new CrmEntityReference(usb_languagehome.EntityLogicalName, languageId);
                }

                if (application.CorrespondanceLanguage.HasValue)
                {
                    Guid correspondenceLanguageId = CrmQueries.GetCrmCorrespondenceLanguage(application.CorrespondanceLanguage.GetValueOrDefault());
                    contact.usb_CorrespondenceLanguageLookup = new CrmEntityReference(usb_languagecorrespondence.EntityLogicalName, correspondenceLanguageId);
                }

                if (application.Disability.HasValue)
                {
                    contact.usb_DisabilityLookup = new CrmEntityReference(usb_disability.EntityLogicalName, application.Disability.GetValueOrDefault());
                }
                contact.usb_DisabilityOther = application.OtherDisability ?? string.Empty;

                contact.usb_UseWheelchair = application.UsesWheelchair;
                contact.EMailAddress1 = application.Email;
                contact.MobilePhone = application.MobileNumber;
                contact.Telephone1 = application.WorkNumber;
                contact.Telephone2 = application.ContactNumber;
                contact.Fax = application.FaxNumber;

                contact.usb_IdentificationType = (int)application.ForeignIdType;
            }

            #endregion

            #region Address Details

            if (application.ApplicationSource == USB.International.Domain.Models.ApplicationSource.AddressDetails)
            {
                #region ResidentialCity

                if (application.ResidentialCity.IsNotNull())
                {
                    // Residential Address
                    if (application.ResidentialCountry.IsNotNull())
                    {
                        contact.usb_address1_CountryLookup = new CrmEntityReference(usb_country.EntityLogicalName, application.ResidentialCountry.GetValueOrDefault());
                    }

                    //if (application.ResidentialCountry.IsNotNull())
                    //{
                    //    contact.usb_address1_Country = new CrmEntityReference(usb_country.EntityLogicalName, application.ResidentialCountry.GetValueOrDefault());
                    //}
                    
                    contact.Address1_Line1 = application.ResidentialStreet1;
                    contact.Address1_Line2 = application.ResidentialStreet2;
                    contact.Address1_Line3 = application.ResidentialStreet3;
                    contact.usb_address1_City = application.ResidentialCity;
                    contact.usb_address1_PostalCode = application.ResidentialPostalCode;
                    contact.usb_AddressValidUntil = application.ResidentialValidUntil;

                    // Emergency Contact Details
                    if (application.EmergencyContactTitle.IsNotNull())
                    {
                        contact.usb_EContactTitleLookup = new CrmEntityReference(usb_Title.EntityLogicalName, application.EmergencyContactTitle.GetValueOrDefault());
                    }

                    if (application.EmergencyContactCountry.IsNotNull())
                    {
                        contact.usb_EContactCountryLookup = new CrmEntityReference(usb_country.EntityLogicalName, application.EmergencyContactCountry.GetValueOrDefault());
                    }

                    contact.usb_EContactInitals = application.EmergencyContactInitals;
                    contact.usb_EContactName = application.EmergencyContactName;
                    contact.usb_EContactSurname = application.EmergencyContactSurname;
                    contact.usb_EContactRelationship = application.EmergencyContactRelationship;
                    contact.usb_EContactStreet1 = application.EmergencyContactStreet1;
                    contact.usb_EContactStreet2 = application.EmergencyContactStreet2;
                    contact.usb_EContactStreet3 = application.EmergencyContactStreet3;
                    contact.usb_EContactCityTown = application.EmergencyContactCityTown;
                    contact.usb_EContactPostalCode = application.EmergencyContactPostalCode;
                    contact.usb_EContactTelephone = application.EmergencyContactTelephone;
                    contact.usb_EContactEmail = application.EmergencyContactEmail;
                }

                #endregion
            }

            #endregion

            #region Work and Study Details

            if (application.ApplicationSource == USB.International.Domain.Models.ApplicationSource.WorkStudies)
            {
                contact.usb_AcademicInstitutionHome = application.HomeInstitution;
                contact.usb_FieldOfStudyHome = (int)application.PrincipleFieldOfStudy;
                contact.usb_NoOfYearsCompleted = int.Parse(application.YearsCompletedAtHomeInstitution);
                contact.usb_DurationofExchange = (int)application.DurationOfExchange;
                contact.usb_TotalYearsOfWorkExperience = application.YearsOfWorkingExperience;
                contact.usb_totalmonthsofworkexperience = application.MonthsOfWorkingExperience;
                contact.usb_contactnamehome = application.ContactPersonName;
                contact.usb_contactemailhome = application.ContactPersonEmailAddress;

                //Guid? sar = GetEnrolmentGuid(usNumber);

                //if (application.Qualification.Any())
                //{
                //    foreach (var item in application.Qualification)
                //    {
                //        var qualification = new usb_HighestQualification
                //        {
                //            usb_AcademicInstitution = new CrmEntityReference(usb_academicinstitution.EntityLogicalName, item.Institution.GetValueOrDefault()),
                //            usb_AcademicInstitutionOther = item.OtherInstitution,
                //            usb_Completed = item.Completed,
                //            usb_FieldOfStudyOptionset = item.FieldOfStudy,
                //            usb_PeriodFrom = new DateTime(item.StartDate.Value, 1, 1),
                //            usb_PeriodTo = new DateTime(item.EndDate.Value, 1, 1),
                //            usb_Qualification = item.OtherInstitution,
                //            usb_StudentNumber = item.StudentNumber,
                //            usb_FirstQualificationTwoOptions = item.First,
                //            usb_StudentAcademicRecordLookup = new CrmEntityReference(usb_StudentAcademicRecord.EntityLogicalName, sar.GetValueOrDefault())
                //        };

                //        using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
                //        {
                //            try
                //            {
                //                crmServiceContext.Update(qualification);
                //            }
                //            finally
                //            {
                //                crmServiceContext.Dispose();
                //            }
                //        }
                //    }
                //}

                //if (application.EmploymentHistory.Any())
                //{
                //    foreach (var item in application.EmploymentHistory)
                //    {
                //        var history = new usb_WorkHistory
                //        {
                //            usb_CurrentEmployer = item.Employer,
                //            usb_JobTitle = item.JobTitle,
                //            usb_LatestJobIndicatorTwoOptions = item.Latest,
                //            usb_Organisation = item.OrganizationOther,
                //            usb_PeriodFrom = new DateTime(item.StartDate.Value, 1, 1),
                //            usb_PeriodTo = new DateTime(item.EndDate.Value, 1, 1),
                //            usb_TypeOfOrganisationOptionset = item.TypeOfOrganisation,
                //            usb_WorkAreaOptionSet = item.WorkArea,
                //            usb_EmploymentType = item.Industry
                //        };

                //        using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
                //        {
                //            try
                //            {
                //                crmServiceContext.Update(history);
                //            }
                //            finally
                //            {
                //                crmServiceContext.Dispose();
                //            }
                //        }
                //    }
                //}
            }

            #endregion

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    crmServiceContext.Update(contact);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return application;
        }

        public static Guid CreateOrUpdateEnrollment(string usNumber, ApplicationSubmission application)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Guid? enrolmentId = new Guid();

                enrolmentId = GetEnrolmentGuid(usNumber, application.EnrolmentGuid.GetValueOrDefault());
                var enrolment = ApplicationSubmissionMapper.MapToEnrollment(application);
                
                enrolment.usb_StudentNumber = usNumber.Trim();
                enrolment.Id = enrolmentId.GetValueOrDefault();
                enrolmentId = enrolment.usb_StudentAcademicRecordId;

                //enrolment.usb_SHLTestRequiredTwoOptions = application.SHLTestRequiredTwoOptions;

                if (!HasApplicationFormURL(enrolmentId.GetValueOrDefault()))
                {
                    enrolment.usb_ApplicationFormURL = application.ApplicationFormsBaseUrl + "?id=" + enrolmentId.Value;
                }

                switch (application.ApplicationSource)
                {
                    case USB.International.Domain.Models.ApplicationSource.ContactDetails:
                        enrolment.usb_StudentName = $"{application.GivenName} {application.Surname}";
                        enrolment.usb_PersonalInformationTwoOptions = true;
                        break;

                    case USB.International.Domain.Models.ApplicationSource.AddressDetails:
                        enrolment.usb_AddressDetailsTwoOptions = true;
                        break;

                    case USB.International.Domain.Models.ApplicationSource.WorkStudies:

                        try
                        {
                            ModifyQualificationHistory(enrolment.Id, application, crmServiceContext);
                            ModifyEmploymentHistory(enrolment.Id, application, crmServiceContext);
                        }
                        finally
                        {
                            crmServiceContext.Dispose();
                        }

                        enrolment.usb_WorkStudiesTwoOptions = true;
                        break;

                    case USB.International.Domain.Models.ApplicationSource.Documentation:
                        enrolment.usb_DocumentationTwoOptions = application.DocumentsCompleteTwoOptions;
                        break;
                }

                enrolment.usb_StudentNumber = usNumber.Trim();

                if (application.Offering.IsNotNull() && (application.Offering != Guid.Empty))
                {
                    enrolment.usb_ProgrammeOfferingLookup = new CrmEntityReference(usb_programmeoffering.EntityLogicalName, application.Offering);

                }

                var contactId = GetContactId(usNumber);

                if (contactId.IsNotNull())
                {
                    enrolment.usb_ContactLookup = new CrmEntityReference(Contact.EntityLogicalName, contactId);
                }

                try
                {
                    crmServiceContext.Update(enrolment);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (application.Offering.IsNotNull() && (application.Offering != Guid.Empty))
                {
                    Guid offeringOwner = CrmQueries.GetCourseOfferingOwner(application.Offering);
                    CrmActions.SetOwnershipOfEnrollment(enrolment.Id, offeringOwner);
                }

                return enrolmentId.Value;
            }
        }

        public static usb_StudentAcademicRecord LoadStudentAcademicRecordByEnrolrmentId(Guid enrolmentID)
        {
            Guid enrolmentId;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_StudentAcademicRecord studentAcademicRecord = null;
                try
                {
                    studentAcademicRecord = (from c in crmServiceContext.usb_StudentAcademicRecordSet
                                             where c.Id == enrolmentID
                                             select c).FirstOrDefault();

                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (studentAcademicRecord.IsNull())
                {
                    enrolmentId = crmServiceContext.Create(new usb_StudentAcademicRecord());
                    studentAcademicRecord = new usb_StudentAcademicRecord {usb_StudentAcademicRecordId = enrolmentId};
                }


                return studentAcademicRecord;
            }
        }

        public static usb_StudentAcademicRecord LoadStudentAcademicRecordByOfferingId(Guid contactId, Guid offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Contact contact = null;

                try
                {
                    crmServiceContext.ContactSet.FirstOrDefault(p => p.ContactId == contactId);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                var sar = contact.usb_contact_usb_studentacademicrecord_Contact;
                var offeringSar = sar.FirstOrDefault(s => s.usb_ProgrammeOfferingLookup.Id == offeringId);

                return offeringSar;
            }
        }

        private static void ModifyEmploymentHistory(Guid enrolmentGuid, ApplicationSubmission applicationSubmission, CrmServiceContext crmServiceContext)
        {
            var employmentHistoryCRM = crmServiceContext.usb_WorkHistorySet.Where(p => p.usb_StudentAcademicRecordLookup.Id == enrolmentGuid);

            if ((applicationSubmission.EmploymentHistory.IsNull() || (applicationSubmission.EmploymentHistory.Count == 0)) && employmentHistoryCRM.IsNotNull())
            {
                foreach (var item in employmentHistoryCRM)
                {
                    crmServiceContext.Delete(usb_WorkHistory.EntityLogicalName, item.Id);
                }
            }
            else if (applicationSubmission.EmploymentHistory.IsNotNull() && (applicationSubmission.EmploymentHistory.Count > 0))
            {
                if (employmentHistoryCRM.IsNotNull())
                {
                    foreach (var item in employmentHistoryCRM)
                    {
                        bool exists = applicationSubmission.EmploymentHistory.Exists(p => p.Id == item.Id);
                        if (!exists)
                        {
                            crmServiceContext.Delete(usb_WorkHistory.EntityLogicalName, item.Id);
                        }
                    }
                }

                foreach (var employmentHistoryItem in applicationSubmission.EmploymentHistory)
                {
                    var employmenthistory = new usb_WorkHistory
                    {
                        usb_StudentAcademicRecordLookup = new CrmEntityReference(usb_StudentAcademicRecord.EntityLogicalName, enrolmentGuid),
                        usb_CurrentEmployer = employmentHistoryItem.Employer,
                        usb_JobTitle = employmentHistoryItem.JobTitle,
                        usb_WorkAreaOptionSet = employmentHistoryItem.WorkArea,
                        usb_Organisation = employmentHistoryItem.Organization,
                        usb_Industry = employmentHistoryItem.Industry,
                        usb_PeriodFrom = employmentHistoryItem.StartDate.HasValue ? new DateTime(employmentHistoryItem.StartDate.Value, 1, 1) : DateTime.MinValue,
                        usb_PeriodTo = employmentHistoryItem.EndDate.HasValue ? new DateTime(employmentHistoryItem.EndDate.Value, 1, 1) : DateTime.MinValue,
                        usb_TypeOfOrganisationOptionset = employmentHistoryItem.Type,
                        usb_LatestJobIndicatorTwoOptions = employmentHistoryItem.Latest,
                    };
                    
                    //usb_CurrentEmployer = employmentHistoryItem.Employer,
                    //usb_JobTitle = employmentHistoryItem.JobTitle,
                    //usb_LatestJobIndicatorTwoOptions = employmentHistoryItem.Latest,
                    //usb_Organisation = employmentHistoryItem.OrganizationOther,
                    //usb_PeriodFrom = new DateTime(employmentHistoryItem.StartDate.GetValueOrDefault(), 1, 1),
                    //usb_PeriodTo = new DateTime(employmentHistoryItem.EndDate.GetValueOrDefault(), 1, 1),
                    //usb_TypeOfOrganisationOptionset = employmentHistoryItem.TypeOfOrganisation,
                    //usb_WorkAreaOptionSet = employmentHistoryItem.WorkArea


                    if (employmentHistoryItem.Id.IsNull() || employmentHistoryItem.Id == Guid.Empty)
                    {
                        crmServiceContext.Create(employmenthistory);
                    }
                    else
                    {
                        employmenthistory.Id = employmentHistoryItem.Id.GetValueOrDefault();
                        crmServiceContext.Update(employmenthistory);
                    }
                }
            }
        }

        private static void ModifyQualificationHistory(Guid enrolmentGuid, ApplicationSubmission applicationSubmission, CrmServiceContext crmServiceContext)
        {
            var tertiaryEducationHistoryCRM = crmServiceContext.usb_HighestQualificationSet.Where(p => p.usb_StudentAcademicRecordLookup.Id == enrolmentGuid);

            if ((applicationSubmission.Qualification.IsNull() || (applicationSubmission.Qualification.Count == 0)) && tertiaryEducationHistoryCRM.IsNotNull())
            {
                foreach (var item in tertiaryEducationHistoryCRM)
                {
                    crmServiceContext.Delete(usb_HighestQualification.EntityLogicalName, item.Id);
                }
            }
            else if (applicationSubmission.Qualification.IsNotNull() && (applicationSubmission.Qualification.Count > 0))
            {
                if (tertiaryEducationHistoryCRM.IsNotNull())
                {
                    foreach (var item in tertiaryEducationHistoryCRM)
                    {
                        bool exists = applicationSubmission.Qualification.Exists(p => p.Id == item.Id);
                        if (!exists)
                        {
                            crmServiceContext.Delete(usb_HighestQualification.EntityLogicalName, item.Id);
                        }
                    }
                }

                foreach (var item in applicationSubmission.Qualification)
                {
                    var studies = new usb_HighestQualification
                    {
                        usb_StudentAcademicRecordLookup = new CrmEntityReference(usb_StudentAcademicRecord.EntityLogicalName, enrolmentGuid)
                    };


                    if (item.Institution.IsNotNull())
                    {
                        studies.usb_AcademicInstitutionLookup = new CrmEntityReference(usb_academicinstitution.EntityLogicalName, item.Institution.GetValueOrDefault());

                    }

                    studies.usb_AcademicInstitutionLookup = new CrmEntityReference(usb_academicinstitution.EntityLogicalName, item.Institution.GetValueOrDefault());
                    studies.usb_AcademicInstitutionOther = item.OtherInstitution;
                    studies.usb_Completed = item.Completed;
                    studies.usb_FieldOfStudyOptionset = item.FieldOfStudy;
                    studies.usb_PeriodFrom = item.StartDate.HasValue ? new DateTime(item.StartDate.Value, 1, 1) : DateTime.MinValue;
                    studies.usb_PeriodTo = item.EndDate.HasValue ? new DateTime(item.EndDate.Value, 1, 1) : DateTime.MinValue;
                    studies.usb_Qualification = item.Degree;
                    studies.usb_StudentNumber = item.StudentNumber;
                    studies.usb_FirstQualificationTwoOptions = item.First;

                    //studies.usb_AcademicInstitutionOther = item.OtherInstitution.IsNull() ? "" : item.OtherInstitution;
                    //studies.usb_StudentNumber = item.StudentNumber;
                    //studies.usb_Completed = item.Completed;

                    //studies.usb_PeriodTo = new DateTime(item.EndDate.GetValueOrDefault(), 1, 1);
                    //studies.usb_PeriodFrom = new DateTime(item.StartDate.GetValueOrDefault(), 1, 1);
                    //studies.usb_FirstQualificationTwoOptions = item.First;
                    //studies.usb_Qualification = item.Degree;
                    //studies.usb_FieldOfStudyOptionset = item.FieldOfStudy;

                    if (item.Id.IsNull() || item.Id == Guid.Empty)
                    {
                        crmServiceContext.Create(studies);
                    }
                    else
                    {
                        studies.Id = item.Id.GetValueOrDefault();
                        crmServiceContext.Update(studies);
                    }
                }
            }
        }

        public static void SetOwnershipOfEnrollment(Guid enrollmentId, Guid ownerId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                var assignRequest = new AssignRequest()
                {
                    Assignee = new EntityReference(SystemUser.EntityLogicalName, ownerId),
                    Target = new EntityReference(usb_StudentAcademicRecord.EntityLogicalName, enrollmentId)
                };

                try
                {
                    organizationService.Execute(assignRequest);
                }
                finally
                {
                    organizationService.Dispose();
                }
            }
        }

        public static void ExecuteApplicationAcknowledgmentWorkFlow(Guid workflowId, Guid sarId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                ExecuteWorkflowRequest request = new ExecuteWorkflowRequest()
                {
                    WorkflowId = workflowId,
                    EntityId = sarId
                };

                try
                {
                    ExecuteWorkflowResponse response = (ExecuteWorkflowResponse)organizationService.Execute(request);
                }
                finally
                {
                    organizationService.Dispose();
                }
            }
        }

        public static Guid GetQualificationGuid(Guid enrolmentId)
        {
            Guid qualificationId;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_HighestQualification qualification = null;
                try
                {
                    qualification = (from c in crmServiceContext.usb_HighestQualificationSet
                                     where c.usb_highestqualification_StudentAcademicRecordLookup.Id == enrolmentId
                                     select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (qualification != null)
                {
                    qualificationId = qualification.Id;
                }
                else
                {
                    qualificationId = crmServiceContext.Create(new usb_HighestQualification());
                }

                return qualificationId;
            }
        }

        public static Guid GetEmploymenthistoryGuid(Guid enrolmentId)
        {
            Guid employmenthistoryGuid;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_WorkHistory employmenthistory = null;
                try
                {
                    employmenthistory = (from c in crmServiceContext.usb_WorkHistorySet
                                         where c.usb_workhistory_StudentAcademicRecordLookup.Id == enrolmentId
                                         select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (employmenthistory != null)
                {
                    employmenthistoryGuid = employmenthistory.Id;
                }
                else
                {
                    employmenthistoryGuid = crmServiceContext.Create(new usb_WorkHistory());
                }

                return employmenthistoryGuid;
            }
        }

        public static Guid GetContactId(string usNumber)
        {
            Guid contactNumber;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Contact contact = null;

                try
                {

                    contact = (from c in crmServiceContext.ContactSet
                               where c.usb_StudentNumber == usNumber
                               select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (contact == null)
                {
                    // create new contact
                    contactNumber = crmServiceContext.Create(new Contact());
                }
                else
                {
                    // get ID of existing contact
                    contactNumber = contact.Id;
                }

                return contactNumber;
            }
        }

        public static usb_StudentAcademicRecord LoadStudentAcademicRecordByUsNumber(string usNumber)
        {
            Guid enrolmentId;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_StudentAcademicRecord studentAcademicRecord = null;
                try
                {
                    studentAcademicRecord = (from c in crmServiceContext.usb_StudentAcademicRecordSet
                                             where c.usb_StudentNumber == usNumber
                                             select c).FirstOrDefault();

                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (studentAcademicRecord.IsNull())
                {
                    enrolmentId = crmServiceContext.Create(new usb_StudentAcademicRecord());
                    studentAcademicRecord = new usb_StudentAcademicRecord();
                    studentAcademicRecord.usb_StudentAcademicRecordId = enrolmentId;
                }


                return studentAcademicRecord;
            }
        }

        public static Guid GetEnrolmentGuid(string usNumber, Guid enrolmentId)
        {
            if (enrolmentId.IsNotNull() && enrolmentId != Guid.Empty)
            {
                return enrolmentId;
            }

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var SARStatusCode = new SARStatusCode();

                usb_StudentAcademicRecord sar = null;

                try
                {
                    var studentAcademicRecords = (from c in crmServiceContext.usb_StudentAcademicRecordSet
                                                  where c.usb_StudentNumber == usNumber &&
                                                        c.usb_ProgrammeOfferingLookup != null
                                                  select c).ToList();

                    if (studentAcademicRecords.IsNotNull())
                    {
                        var records = studentAcademicRecords.Where(x => x.usb_StudentNumber == usNumber);

                        var usbStudentacademicrecords = records as usb_StudentAcademicRecord[] ?? records.ToArray();

                        if (usbStudentacademicrecords.Any())
                        {
                            foreach (var record in usbStudentacademicrecords)
                            {
                                var statusCode = SARStatusCode.GetStatusCode(record.statuscode.GetValueOrDefault());

                                if (statusCode != usb_studentacademicrecord_statuscode.Applicant) continue;
                                var offering = record.usb_ProgrammeOfferingLookup.IsNotNull()
                                    ? CrmQueries.GetOffering(record.usb_ProgrammeOfferingLookup.Id)
                                    : null;

                                sar = offering.statuscode == (int)usb_programmeoffering_statuscode.Launched ? record : null;
                            }
                        }
                    }
                }

                finally
                {
                    crmServiceContext.Dispose();
                }

                enrolmentId = sar?.usb_StudentAcademicRecordId ?? Guid.Empty;

                return enrolmentId;
            }
        }

        public static Guid GetEnrolmentGuid(string usNumber, Guid? offeringId)
        {
            Guid enrolmentId;
            var applicationFormApiController = new ApplicationFormApiController();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var SARStatusCode = new SARStatusCode();

                usb_StudentAcademicRecord sar = null;

                try
                {
                    var studentAcademicRecord = (from c in crmServiceContext.usb_StudentAcademicRecordSet
                                                 where c.usb_StudentNumber == usNumber && c.usb_ProgrammeOfferingLookup != null
                                                 select c).ToList();

                    foreach (var item in studentAcademicRecord)
                    {
                        var statusCode = SARStatusCode.GetStatusCode(item.statuscode.GetValueOrDefault());

                        if (statusCode == usb_studentacademicrecord_statuscode.Applicant)
                        {
                            var offering = item.usb_ProgrammeOfferingLookup;
                            if (offering.IsNotNull())
                            {
                                var isOfferingCurrent = applicationFormApiController.IsOfferingDateCurrent(offering.Id);
                                if (!isOfferingCurrent) continue;
                                sar = item;
                                break;
                            }
                        }
                    }
                }

                finally
                {
                    crmServiceContext.Dispose();
                }

                if (sar?.usb_StudentAcademicRecordId != null)
                {
                    enrolmentId = sar.usb_StudentAcademicRecordId.Value;
                }
                else
                {
                    sar = new usb_StudentAcademicRecord
                    {
                        usb_StudentNumber = usNumber,
                        usb_ProgrammeOfferingLookup = new CrmEntityReference(usb_programmeoffering.EntityLogicalName, offeringId.GetValueOrDefault())
                    };

                    enrolmentId = crmServiceContext.Create(sar);
                }

                return enrolmentId;
            }
        }

        public static bool DoesEnrollmentAlreadyExist(Guid contactId, Guid offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_StudentAcademicRecord sar = null;
                try
                {
                    sar = crmServiceContext.usb_StudentAcademicRecordSet
                        .Where(p => p.usb_ContactLookup.Id == contactId)
                        .Where(p => p.usb_ProgrammeOfferingLookup.Id == offeringId)
                        .FirstOrDefault(p => p.statuscode == (int)usb_studentacademicrecord_statuscode.Applicant);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (sar == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static string FindStrongForeignerMatch(Guid nationalityId, DateTime dateOfBirth, string firstNames, string surname, Guid languageId, Guid genderId, Guid ethnicityId, string foreignIDNumber, string passportNumber)
        {
            if (foreignIDNumber == null)
            {
                foreignIDNumber = "";
            }

            if (passportNumber == null)
            {
                passportNumber = "";
            }

            List<Contact> foreignerMatches = null;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    foreignerMatches = crmServiceContext.ContactSet.
                                       Where(p => p.usb_NationalityLookup.Id == nationalityId).
                                       Where(p => p.BirthDate.GetValueOrDefault() == dateOfBirth).
                                       Where(p => p.FirstName == firstNames.Trim()).
                                       Where(p => p.LastName == surname.Trim()).
                                       Where(p => p.usb_contact_HomeLanguageLookup.Id == languageId).
                                       Where(p => p.usb_GenderLookup.Id == genderId).
                                       Where(p => p.usb_EthnicityLookup.Id == ethnicityId).
                                       Where(p => p.usb_PassportNumber == foreignIDNumber.Trim() || p.usb_PassportNumber == passportNumber.Trim()).ToList<Contact>();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            if (foreignerMatches.Count == 1)
            {
                return foreignerMatches[0].usb_StudentNumber;
            }
            else
            {
                return null;
            }
        }

        public static bool FindWeakForeignerMatches(Guid nationalityId, DateTime dateOfBirth, string firstNames, string surname)
        {
            List<Contact> foreignerMatches = null;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    foreignerMatches = crmServiceContext.ContactSet.
                                       Where(p => p.usb_NationalityLookup.Id == nationalityId).
                                       Where(p => p.BirthDate == dateOfBirth).
                                       Where(p => p.FirstName == firstNames.Trim()).
                                       Where(p => p.LastName == surname.Trim()).ToList<Contact>();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            if (foreignerMatches.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region Private Methods

        private static bool HasApplicationFormURL(Guid enrolmentId)
        {
            bool hasUrl = false;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var sar = crmServiceContext.usb_StudentAcademicRecordSet.FirstOrDefault(p => p.Id == enrolmentId);

                if (sar.IsNotNull())
                {
                    crmServiceContext.Dispose();

                    hasUrl = sar.usb_ApplicationFormURL.IsNotNullOrEmpty();
                }
            }

            return hasUrl;
        }

        #endregion

    }
}
