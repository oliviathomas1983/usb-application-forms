﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public static class CrmIdToSisCodeMapping
    {
        public static string GetTitleSisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_Title lookup = null;

                try
                {
                    lookup = (from s in crmServiceContext.usb_TitleSet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.usb_SISCode;
            }
        }

        public static string GetGenderSisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_gender lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.usb_genderSet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                switch (lookup.usb_name.ToUpper())
                {
                    case "MALE":
                        return "M";

                    case "FEMALE":
                        return "V";

                    case "NON BINARY":
                        return "N";

                    default:
                        throw new Exception();
                }
            }
        }

        public static string GetEthnicitySisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_ethnicity lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.usb_ethnicitySet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.usb_SisCode;
            }
        }

        public static string GetNationalitySisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_Nationality lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.usb_NationalitySet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.usb_SISCode;
            }
        }

        public static string GetLanguageSisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_languagehome lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.usb_languagehomeSet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.usb_LanguageCode;
            }
        }
    }
}
