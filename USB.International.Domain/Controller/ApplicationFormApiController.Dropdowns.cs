﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers
{
    public partial class ApplicationFormApiController
    {
        public string Test()
        {
            logger.Error("Test endpoint");
            return DateTime.Now.ToLongTimeString();
        }

        public Dictionary<string, Guid> Titles()
        {
            return Dropdowns.Titles();
        }

        public Dictionary<string, Guid> Genders()
        {
            return Dropdowns.Genders();
        }

        public Dictionary<string, Guid> Ethnicities()
        {
            return Dropdowns.Ethnicities();
        }


        public Dictionary<string, Guid> NonSAEthnicities()
        {
            return Dropdowns.NonSAEthnicities();
        }

        public Dictionary<string, Guid> Nationalities()
        {
            return Dropdowns.Nationalities();
        }

        public Dictionary<string, Guid> CorrespondenceLanguages()
        {
            return Dropdowns.CorrespondenceLanguages();
        }

        public Dictionary<string, Guid> Languages()
        {
            return Dropdowns.Languages();
        }

        public Dictionary<string, int> ForeignIdentificationTypes()
        {
            return Dropdowns.ForeignIdentificationTypes();
        }

        //public Dictionary<string, int> SouthAfricaIdentificationTypes()
        //{
        //    return Dropdowns.SouthAfricaIdentificationTypes();
        //}

        //public Dictionary<string, Guid> DietaryRequirements()
        //{
        //    return Dropdowns.DietaryRequirements();
        //}

        public IEnumerable<KeyValuePair<string, Guid>> Disabilities()
        {
            return Dropdowns.Disabilities();
        }

        public Dictionary<string, Guid> AddressCountries()
        {
            return Dropdowns.AddressCountries();
        }

        public Dictionary<string, int> QualificationFields()
        {
            return Dropdowns.QualificationFields();
        }

        public Dictionary<string, Guid> QualificationTypes()
        {
            return Dropdowns.QualificationTypes();
        }

        public Dictionary<string, int> Industries()
        {
            return Dropdowns.Industries();
        }

        public Dictionary<string, int> WorkAreas()
        {
            return Dropdowns.WorkAreas();
        }

        public Dictionary<string, int> Organization()
        {
            return Dropdowns.Organization();
        }

        public Dictionary<string, int> Industry()
        {
            return Dropdowns.Industry();
        }

        public Dictionary<string, int> OccupationalCategories()
        {
            return Dropdowns.OccupationalCategories();
        }

        public Dictionary<string, int> MarketingSources()
        {
            return Dropdowns.MarketingSources();
        }

        public Dictionary<string, int> MarketingReasons()
        {
            return Dropdowns.MarketingReasons();
        }

        public Dictionary<string, int> EmploymentType()
        {
            return Dropdowns.EmploymentType();
        }

        public Dictionary<string, int> EmployerAssistFinancially()
        {
            return Dropdowns.EmployerAssistFinancially();
        }

        public Dictionary<string, Guid> PermitTypes()
        {
            return Dropdowns.PermitTypes();
        }

        public Dictionary<string, Guid> MaritalStatus()
        {
            return Dropdowns.MaritalStatus();
        }

        public Dictionary<string, Guid> Offerings(bool isMBA, Guid programmeGuid)
        {
            return Dropdowns.Offerings(isMBA, programmeGuid);
        }

        public Dictionary<string, Guid> Programme()
        {
            return Dropdowns.Programme();
        }

        public Dictionary<string, int> LeesAfrikaans()
        {
            return Dropdowns.LeesAfrikaans();
        }

        public Dictionary<string, int> PraatAfrikaans()
        {
            return Dropdowns.PraatAfrikaans();
        }

        public Dictionary<string, int> SkryfAfrikaans()
        {
            return Dropdowns.SkryfAfrikaans();
        }

        public Dictionary<string, int> VerstaanAfrikaans()
        {
            return Dropdowns.VerstaanAfrikaans();
        }

        public Dictionary<string, int> ReadEnglish()
        {
            return Dropdowns.EnglishRead();
        }

        public Dictionary<string, int> TalkEnglish()
        {
            return Dropdowns.EnglishSpeak();
        }

        public Dictionary<string, int> WriteEnglish()
        {
            return Dropdowns.EnglishWrite();
        }

        public Dictionary<string, int> UnderstandEnglish()
        {
            return Dropdowns.EnglishUnderstand();
        }

        public Dictionary<string, int> MathematicsCompetency()
        {
            return Dropdowns.MathematicCompetency();
        }

        public Dictionary<string, int> MathematicsPercentage()
        {
            return Dropdowns.MathematicPercentage();
        }

        public Dictionary<string, int> PriorInvolvement()
        {
            return Dropdowns.PriorInvolvement();
        }

        public Dictionary<string, Guid> Institution()
        {
            return Dropdowns.Institution();
        }

        public Dictionary<string, int> FieldOfStudy()
        {
            return Dropdowns.FieldOfStudy();
        }

        public Dictionary<string, int> DurationOfExchange()
        {
            return CrmQueries.GetOptionSetValues(Contact.EntityLogicalName, "usb_DurationofExchange");
        }

        public Dictionary<string, int> InfoSessions()
        {
            return Dropdowns.InfoSessions();
        }
    }
}
