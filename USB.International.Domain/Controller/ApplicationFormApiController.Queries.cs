﻿using Microsoft.Xrm.Client;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Usb.International.Domain.Sharepoint;
using SettingsMan = USB.International.Domain.Properties.Settings;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers
{
    public partial class ApplicationFormApiController
    {
        public Guid GetIdOfShortCourseWithNumber(string courseNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var crmShortCourse = crmServiceContext.usb_programmeofferingSet.FirstOrDefault(p => p.usb_ProgrammeOfferingCode == courseNumber);

                if (crmShortCourse == null)
                {
                    throw new Exception();
                }

                return crmShortCourse.Id;
            }
        }
        
        public List<Document> GetSharePointUpoadedDocumentsInformation(Guid enrolmentId)
        {
            return SharepointActions.GetSharePointUpoadedDocuments(enrolmentId);
        }

        public List<Document> GetRequiredDocumentsInformation(Guid programmeId, Guid enrolmentId, int level = 1)
        {
            List<Document> documentList = CrmQueries.GetRequiredDocuments(programmeId, level);

            foreach (Document doc in documentList)
            {

                if (Cache.ContainsItem(enrolmentId.ToString(), doc.DocumentTypeId.ToString()))
                {
                    Tuple<string, byte[]> documentData = Cache.GetCachedItem(enrolmentId.ToString(), doc.DocumentTypeId.ToString());

                    doc.FileName = documentData.Item1;
                    doc.UploadedDate = DateTime.Now.ToString("dd/MM/yyyy");
                    doc.Status = "Received";
                    doc.Level = level;

                    IDictionary<string, object> metadata =
                        GetSharePointItemMetaData(enrolmentId, doc.DocumentTypeId ?? Guid.Empty);

                    if (metadata.IsNotNull())
                    {
                        doc.Reviewed = (metadata["Reviewed"].AsString().ToLower() == "yes");
                    }
                    else
                    {
                        doc.Reviewed = false;
                    }
                }
                else
                {
                    IDictionary<string, object> metadata =
                        GetSharePointItemMetaData(enrolmentId, doc.DocumentTypeId ?? Guid.Empty);

                    if (metadata.IsNotNull())
                    {
                        doc.FileName = metadata["Title"].AsString();
                        doc.UploadedDate = ((DateTime)metadata["Modified"]).ToString("dd/MM/yyyy");
                        doc.Status = "Received";
                        doc.Reviewed = (metadata["Reviewed"].AsString().ToLower() == "yes");
                        doc.Level = level;

                        Cache.AddOrReplace(enrolmentId.ToString(), doc.DocumentId.ToString(),
                                       SharepointActions.DownloadDocument(doc.DocumentId, enrolmentId));
                    }
                }
            }

            return documentList;
        }

        public List<Document> GetRequiredDocumentsInformationDirect(Guid programmeOfferingId, Guid enrolmentId, bool insideCRM, int level = 1)
        {
            List<Document> documentList = CrmQueries.GetRequiredDocuments(programmeOfferingId, level);
            
            if (!insideCRM)
            {
                var gmatShlDocuments = documentList.FirstOrDefault(d => d.Description == SettingsMan.Default.SHLSelectionName);
                documentList.Remove(gmatShlDocuments);

                var rplRef1 = documentList.FirstOrDefault(d => d.Description == SettingsMan.Default.RPLRef1Name);
                documentList.Remove(rplRef1);

                var rplRef2 = documentList.FirstOrDefault(d => d.Description == SettingsMan.Default.RPLRef2Name);
                documentList.Remove(rplRef2);

                var rplCandidate = documentList.FirstOrDefault(d => d.Description == SettingsMan.Default.RPLCandidateName);
                documentList.Remove(rplCandidate);
            }

            return documentList;
        }


        public Document GetRequiredDocumentInformation(Guid documentId, Guid enrolmentId)
        {
            Document document = CrmQueries.GetRequiredDocument(documentId);

            if (Cache.ContainsItem(enrolmentId.ToString(), documentId.ToString()))
            {
                Tuple<string, byte[]> documentData = Cache.GetCachedItem(enrolmentId.ToString(), documentId.ToString());

                document.FileName = documentData.Item1;
                document.UploadedDate = DateTime.Now.ToString("dd/MM/yyyy");
                document.Status = "Received";
            }
            else
            {
                IDictionary<string, object> metadata =
                   GetSharePointItemMetaData(enrolmentId, documentId);

                if (metadata.IsNotNull())
                {
                    document.FileName = metadata["Title"].AsString();
                    document.UploadedDate = ((DateTime)metadata["Modified"]).ToString("dd/MM/yyyy");
                    document.Status = "Received";
                    document.Reviewed = (metadata["Reviewed"].AsString().ToLower() == "yes");
                }
            }

            return document;
        }

        public Document GetRequiredDocumentInformationDirect(Guid documentId, Guid enrolmentId)
        {
            Document document = CrmQueries.GetRequiredDocument(documentId);

            IDictionary<string, object> metadata =
                GetSharePointItemMetaData(enrolmentId, documentId);

            if (metadata.IsNotNull())
            {
                document.FileName = metadata["Title"].AsString();
                document.UploadedDate = ((DateTime)metadata["Modified"]).ToString("dd/MM/yyyy");
                document.Status = "Received";
                document.Reviewed = (metadata["Reviewed"].AsString().ToLower() == "yes");
                document.DocumentTypeId = Guid.Empty;
            }

            return document;
        }

        public Document DeleteDocument(Guid documentId, Guid enrolmentId)
        {
            if (Cache.ContainsItem(enrolmentId.ToString(), documentId.ToString()))
            {
                Cache.RemoveItem(enrolmentId.ToString(), documentId.ToString());
            }

            return CrmQueries.GetRequiredDocument(documentId);
        }

        public ApplicationStatus GetApplicationStatus(Guid enrolmentId)
        {
            return CrmQueries.GetApplicationStatus(enrolmentId);
        }

        public string GetSharePointFileName(Guid enrolmentId, Guid documentTypeId)
        {
            return SharepointActions.GetFileName(documentTypeId, enrolmentId);
        }

        public IDictionary<string, object> GetSharePointItemMetaData(Guid enrolmentId, Guid documentTypeId)
        {
            return SharepointActions.GetItemMetaData(documentTypeId, enrolmentId);
        }
        public IDictionary<string, object> GetSharePointItemMetaDataById(Guid enrolmentId, int documentId)
        {
            return SharepointActions.GetItemMetaDataById(documentId, enrolmentId);
        }

        public Guid GetEnrolmentGuid(string usNumber, Guid? offeringId)
        {
            return CrmActions.GetEnrolmentGuid(usNumber, offeringId);
        }

        //public bool IsWebTokenValid(Guid webTokenId)
        //{
        //    return CrmQueries.IsWebTokenValid(webTokenId);
        //}

        public ApplicantStatus? GetApplicantStatus(Guid enrolmentId)
        {
            return CrmQueries.GetApplicantStatus(enrolmentId);
        }

        public bool IsApplicationFormSubmitted(Guid enrolmentId)
        {
            return CrmQueries.IsApplicationFormSubmitted(enrolmentId);
        }

        public bool IsApplicantFemaleAndMarried(Guid enrolmentId)
        {
            var applicationFormApiController = new ApplicationFormApiController();
            var personalDetials = applicationFormApiController.GetPersonalDetails(enrolmentId);

            if (personalDetials.IsNotNull())
            {
                return personalDetials.SelectedGenderName.Contains(SettingsMan.Default.FemaleName) &&
                            personalDetials.SelectedMaritalStatusName.Contains(SettingsMan.Default.MarriedName);
            }
            return false;
        }

        //public bool IsRPLCandidate(Guid enrolmentId)
        //{
        //    var studentRecordDetails = CrmActions.LoadStudentAcademicRecordByEnrolrmentId(enrolmentId);

        //    if (studentRecordDetails.IsNotNull())
        //    {
        //        if (studentRecordDetails.usb_RPLCandidateTwoOptions.IsNotNull())
        //        {
        //            return studentRecordDetails.usb_RPLCandidateTwoOptions.Value;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }

        //    return false;
        //}

        //public bool IsSHLTestRequired(Guid enrolmentId)
        //{
        //    var studentRecordDetails = CrmActions.LoadStudentAcademicRecordByEnrolrmentId(enrolmentId);

        //    if (studentRecordDetails.IsNotNull())
        //    {
        //        return studentRecordDetails.usb_SHLTestRequiredTwoOptions.HasValue ? studentRecordDetails.usb_SHLTestRequiredTwoOptions.Value : false;
        //    }

        //    return false;
        //}
    }
}
