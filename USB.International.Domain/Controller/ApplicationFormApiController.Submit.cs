﻿using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using StellenboschUniversity.UsbInternational.Integration.Crm.Crm;
using StellenboschUniversity.Utilities.NLogGelfExtensions;
using System;
using System.Linq;
using SettingsMan = USB.International.Domain.Properties.Settings;
using Usb.International.Domain.Sharepoint;
using USB.International.Domain.Controller.Dto;
using System.Collections.Generic;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers
{
    public partial class ApplicationFormApiController
    {
        private readonly string duplicateForeignerMessage = SettingsMan.Default.DuplicateForeignerMessage;

        public PersonalContactDetails GetPersonalDetails(string usNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var contact = crmServiceContext.ContactSet.FirstOrDefault(p => p.usb_StudentNumber == usNumber);

                return ContactMapper.MapToPersonalContactDetails(contact);
            }
        }

        public PersonalContactDetails GetPersonalDetails(Guid id)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var sar = crmServiceContext.usb_StudentAcademicRecordSet.FirstOrDefault(p => p.usb_StudentAcademicRecordId == id);

                PersonalContactDetails personalDetails = null;

                if (sar.IsNotNull())
                {
                    personalDetails = new PersonalContactDetails();
                    var contact = sar.usb_contact_usb_studentacademicrecord_Contact;

                    personalDetails = ContactMapper.MapToPersonalContactDetails(contact);
                }

                return personalDetails;
            }
        }

        public PersonalContactDetails GetPersonalDetailsByEmail(string email)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var contact = crmServiceContext.ContactSet.FirstOrDefault(p => p.EMailAddress2 == email);

                //return contact;
                //var sar = contact.usb_studentacademicrecord_ContactLookup;

                return ContactMapper.MapToPersonalContactDetails(contact);
            }
        }

        public AddressDetails GetAddressDetails(string usNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var contact = crmServiceContext.ContactSet.FirstOrDefault(p => p.usb_StudentNumber == usNumber);

                return ContactMapper.MapToAddressDetails(contact);
            }
        }

        public Workstudies GetWorkStudies(string usNumber, Guid? enrolmentId = null)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var enrolmentGuid = new Guid();

                if (enrolmentId.IsNull())
                {
                    enrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
                }
                else
                {
                    //enrolmentGuid = GetEnrolmentGuid(usNumber);
                    enrolmentGuid = (Guid)enrolmentId.AsGuid();
                }

                var studentAcademicRecord = crmServiceContext.usb_StudentAcademicRecordSet.FirstOrDefault(p => p.Id == enrolmentGuid);

                var employmentHistoryCRM = crmServiceContext.usb_WorkHistorySet.Where(p => p.usb_StudentAcademicRecordLookup.Id == studentAcademicRecord.Id);

                var listEmploymentHistory = new List<EmploymentHistory>();
                foreach (var item in employmentHistoryCRM)
                {
                    listEmploymentHistory.Add(CRMEntityMapper.MapToEmploymentHistory(item));
                }

                var tertiaryEducationHistoryCRM = crmServiceContext.usb_HighestQualificationSet.Where(p => p.usb_highestqualification_StudentAcademicRecordLookup.Id == studentAcademicRecord.Id);

                var listTertiaryEducationHistory = new List<TertiaryEducationHistory>();
                foreach (var item in tertiaryEducationHistoryCRM)
                {
                    listTertiaryEducationHistory.Add(CRMEntityMapper.MapToTertiaryEducationHistory(item));
                }

                var contact = crmServiceContext.ContactSet.FirstOrDefault(p => p.usb_StudentNumber == usNumber);
                var workStudies = EnrolmentMapper.MapToWorkStudies(contact);

                workStudies.EmploymentHistory = listEmploymentHistory;
                workStudies.TertiaryEducationHistory = listTertiaryEducationHistory;
                return workStudies;
            }
        }

        public string GetStudentNumber(string email)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var contact = (from c in crmServiceContext.ContactSet
                               where c.EMailAddress1 == email
                               select c).FirstOrDefault();

                if (contact.IsNotNull())
                {
                    return contact.usb_StudentNumber;
                }
            }

            return string.Empty;
        }

        private static IEnumerable<Contact> GetContacts(CrmServiceContext crmServiceContext)
        {
            var contact = (from c in crmServiceContext.ContactSet
                           where c.ContactId == Guid.Parse("03c0dd79-8279-e111-86d6-00155d62cf10")
                           select c).ToList();

            // foreach (var c  in contact) 
            //  {
            //    if (CrmActions.GetEnrolmentGuid(c.usb_USNumber) != Guid.Empty)
            // yield return c;
            //  }
            return contact;
        }

        public usb_studentacademicrecord_statuscode GetStatus(string usNumber)
        {
            var mapper = new SARStatusCode();
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {

                var studentAcademic = (from c in crmServiceContext.usb_StudentAcademicRecordSet
                                       where c.usb_StudentNumber == usNumber
                                       select c).FirstOrDefault();

                if (studentAcademic.IsNotNull() && studentAcademic.statecode == 0)
                {
                    mapper.GetStatusCode(studentAcademic.statuscode.GetValueOrDefault(2));
                }
            }

            return usb_studentacademicrecord_statuscode.Deactivated;
        }

        public bool ContactHasSARWithApplicantStatus(string usNumber)
        {
            //TODO: Richard use offeringId to get offering and then get offering StartDate
            bool match = false;
            var mapper = new SARStatusCode();
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {

                var studentAcademic = (from c in crmServiceContext.usb_StudentAcademicRecordSet
                                       where c.usb_StudentNumber == usNumber
                                       select c).ToList();

                foreach (var sar in studentAcademic)
                {
                    var statusCode = mapper.GetStatusCode(sar.statuscode.GetValueOrDefault());

                    //#TODO: Richard ignore Status for now - Chat to Marsunet to get latest specs
                    //if (statusCode == usb_studentacademicrecord_statuscode.Applicant)
                    //{
                    var offering = sar.usb_ProgrammeOfferingLookup;
                    if (offering.IsNotNull())
                    {
                        var isOfferingCurrent = IsOfferingDateCurrent(offering.Id);
                        match = isOfferingCurrent;
                        if (match)
                        {
                            break;
                        }
                    }
                    //}
                }
            }

            return match;
        }

        public bool IsOfferingDateCurrent(Guid offeringId)
        {
            bool isOfferingCurrent = true;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var offering = CrmQueries.GetOffering(offeringId);

                //TODO: See if using the start date of the offering would be better as year offering is a string field
                //var yearOffered = offering.usb_YearOffered.Replace(" ","");
                var yearOffered = offering.usb_StartDate.Value.AddDays(1).Year.AsString();


                int year;

                var isInt = int.TryParse(yearOffered, out year);

                var yearForApplication = DateTime.Now.AddYears(1).Year;

                if (isInt)
                {
                    if (year < yearForApplication)
                    {
                        isOfferingCurrent = false;
                    }
                }
            }

            return isOfferingCurrent;
        }

        public ApplicationSubmissionResult SubmitPersonalInformation(ApplicationSubmission applicationSubmission)
        {
            Guid contactId = Guid.Empty;
            Guid enrolmentGuid = Guid.Empty;
            string sisCorrespondenceLanguageCode = CrmQueries.GetSisCorrespondenceLanguageCode(applicationSubmission.Language.GetValueOrDefault());
            string usNumber = applicationSubmission.UsNumber;

            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            if (usNumber.IsNullOrEmpty())
            {
                // foreigner duplication check
                if (CrmQueries.GetNationalityName(applicationSubmission.Nationality.GetValueOrDefault()) != "South Africa")
                {
                    usNumber = CrmActions.FindStrongForeignerMatch(
                                    applicationSubmission.Nationality.GetValueOrDefault(),
                                    applicationSubmission.DateOfBirth.GetValueOrDefault(),
                                    applicationSubmission.FirstNames,
                                    applicationSubmission.Surname,
                                    applicationSubmission.Language.GetValueOrDefault(),
                                    applicationSubmission.Gender.GetValueOrDefault(),
                                    applicationSubmission.Ethnicity.GetValueOrDefault(),
                                    applicationSubmission.ForeignIdNumber,
                                    applicationSubmission.PassportNumber);

                    if (usNumber == null)
                    {
                        if (CrmActions.FindWeakForeignerMatches(applicationSubmission.Nationality.GetValueOrDefault(),
                                                     applicationSubmission.DateOfBirth.GetValueOrDefault(),
                                                     applicationSubmission.FirstNames, applicationSubmission.Surname) == true)
                        {
                            return new ApplicationSubmissionResult()
                            {
                                Successful = false,
                                Message = duplicateForeignerMessage
                            };
                        }
                    }
                }

                // get US number
                if (usNumber.IsNullOrEmpty())
                {
                    try
                    {
                        if (usNumber.IsNullOrEmpty())
                        {
                            usNumber =
                                PersonMaintenanceServiceInteractions.GetApplicantUSNumber(applicationSubmission,
                                                                                          sisCorrespondenceLanguageCode);
                        }
                    }
                    catch (ApplicationException ae)
                    {
                        ApplicationFormApiController.logger.LogWithContext(NLog.LogLevel.Error, "Could not get applicant US number from SIS", ae, applicationSubmission);

                        return new ApplicationSubmissionResult() { Successful = false, Message = ae.Message };
                    }
                    catch (Exception exception)
                    {
                        ApplicationFormApiController.logger.LogWithContext(NLog.LogLevel.Error, "Could not get applicant US number from SIS", exception, applicationSubmission);

                        return new ApplicationSubmissionResult() { Successful = false, Message = "Could not get US number for applicant." };
                    }
                }

                logger.LogWithContext(NLog.LogLevel.Info, String.Format("Applicant was assigned US number: {0}", usNumber), applicationSubmission);
            }
            // create contact
            try
            {
                contactId = CrmActions.GetContactId(usNumber);

                CrmActions.CreateOrUpdateContact(usNumber, applicationSubmission, contactId);

            }
            catch (Exception exception)
            {
                logger.Error("Could not create CRM contact", exception, applicationSubmission);
                return new ApplicationSubmissionResult() { Successful = false, Message = "Could not create contact." };
            }

            try
            {
                enrolmentGuid = CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission);
            }
            catch (Exception exception)
            {
                string innerExceptionMessage = "";

                if (exception.InnerException != null)
                {
                    if (exception.InnerException.Message != null)
                    {
                        innerExceptionMessage = exception.InnerException.Message;
                    }
                }

                logger.LogWithContext(NLog.LogLevel.Error, "Could not create CRM enrollment - inner exception: " + innerExceptionMessage, exception, applicationSubmission);
                return new ApplicationSubmissionResult() { Successful = false, Message = "Could not create enrollment." };
            }

            // handle uploaded documents

            logger.LogWithContext(NLog.LogLevel.Info, "Successful submit", applicationSubmission);

            return new ApplicationSubmissionResult() { Successful = true, Message = "", UsNumber = usNumber, EnrolmentId = enrolmentGuid, ContactId = contactId };
        }
        public ApplicationSubmissionResult SubmitAddressDetails(ApplicationSubmission applicationSubmission)
        {
            Guid contactId = Guid.Empty;

            string sisCorrespondenceLanguageCode = CrmQueries.GetSisCorrespondenceLanguageCode(applicationSubmission.Language.GetValueOrDefault());

            string usNumber = applicationSubmission.UsNumber;

            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            if (String.IsNullOrEmpty(usNumber))
            {
                // foreigner duplication check
                if (CrmQueries.GetNationalityName(applicationSubmission.Nationality.GetValueOrDefault()) != "South Africa")
                {
                    usNumber = CrmActions.FindStrongForeignerMatch(
                                    applicationSubmission.Nationality.GetValueOrDefault(),
                                    applicationSubmission.DateOfBirth.GetValueOrDefault(),
                                    applicationSubmission.FirstNames,
                                    applicationSubmission.Surname,
                                    applicationSubmission.Language.GetValueOrDefault(),
                                    applicationSubmission.Gender.GetValueOrDefault(),
                                    applicationSubmission.Ethnicity.GetValueOrDefault(),
                                    applicationSubmission.ForeignIdNumber,
                                    applicationSubmission.PassportNumber);

                    if (usNumber == null)
                    {
                        if (CrmActions.FindWeakForeignerMatches(applicationSubmission.Nationality.GetValueOrDefault(), applicationSubmission.DateOfBirth.GetValueOrDefault(), applicationSubmission.FirstNames, applicationSubmission.Surname) == true)
                        {
                            return new ApplicationSubmissionResult()
                            {
                                Successful = false,
                                Message = duplicateForeignerMessage
                            };
                        }
                    }
                }
            }

            // get US number
            if (String.IsNullOrEmpty(usNumber))
            {
                try
                {
                    usNumber = PersonMaintenanceServiceInteractions.GetApplicantUSNumber(applicationSubmission, sisCorrespondenceLanguageCode);
                }
                catch (Exception exception)
                {
                    ApplicationFormApiController.logger.LogWithContext(NLog.LogLevel.Error, "Could not get applicant US number from SIS", exception, applicationSubmission);

                    return new ApplicationSubmissionResult() { Successful = false, Message = "Could not get US number for applicant." };
                }
            }

            logger.LogWithContext(NLog.LogLevel.Info, String.Format("Applicant was assigned US number: {0}", usNumber), applicationSubmission);

            // create contact
            try
            {
                contactId = CrmActions.GetContactId(usNumber);
                CrmActions.CreateOrUpdateContact(usNumber, applicationSubmission, contactId);
                CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission);
            }
            catch (Exception exception)
            {
                logger.LogWithContext(NLog.LogLevel.Error, "Could not create CRM contact", exception, applicationSubmission);
                return new ApplicationSubmissionResult() { Successful = false, Message = "Could not create contact." };
            }

            logger.LogWithContext(NLog.LogLevel.Info, "Successful submit", applicationSubmission);

            return new ApplicationSubmissionResult() { Successful = true, Message = "" };
        }

        public ApplicationSubmissionResult SubmitWorkDetails(ApplicationSubmission applicationSubmission)
        {
            Guid contactId = Guid.Empty;
            string sisCorrespondenceLanguageCode = CrmQueries.GetSisCorrespondenceLanguageCode(applicationSubmission.Language.GetValueOrDefault());

            string usNumber = applicationSubmission.UsNumber;

            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            if (String.IsNullOrEmpty(usNumber))
            {
                // foreigner duplication check
                if (CrmQueries.GetNationalityName(applicationSubmission.Nationality.GetValueOrDefault()) != "South Africa")
                {
                    usNumber = CrmActions.FindStrongForeignerMatch(
                                    applicationSubmission.Nationality.GetValueOrDefault(),
                                    applicationSubmission.DateOfBirth.GetValueOrDefault(),
                                    applicationSubmission.FirstNames,
                                    applicationSubmission.Surname,
                                    applicationSubmission.Language.GetValueOrDefault(),
                                    applicationSubmission.Gender.GetValueOrDefault(),
                                    applicationSubmission.Ethnicity.GetValueOrDefault(),
                                    applicationSubmission.ForeignIdNumber,
                                    applicationSubmission.PassportNumber);

                    if (usNumber == null)
                    {
                        if (CrmActions.FindWeakForeignerMatches(applicationSubmission.Nationality.GetValueOrDefault(), applicationSubmission.DateOfBirth.GetValueOrDefault(), applicationSubmission.FirstNames, applicationSubmission.Surname) == true)
                        {
                            return new ApplicationSubmissionResult()
                            {
                                Successful = false,
                                Message = duplicateForeignerMessage
                            };
                        }
                    }
                }
            }

            // get US number
            if (String.IsNullOrEmpty(usNumber))
            {
                try
                {
                    usNumber = PersonMaintenanceServiceInteractions.GetApplicantUSNumber(applicationSubmission, sisCorrespondenceLanguageCode);
                }
                catch (Exception exception)
                {
                    ApplicationFormApiController.logger.LogWithContext(NLog.LogLevel.Error, "Could not get applicant US number from SIS", exception, applicationSubmission);

                    return new ApplicationSubmissionResult() { Successful = false, Message = "Could not get US number for applicant." };
                }
            }

            logger.LogWithContext(NLog.LogLevel.Info, String.Format("Applicant was assigned US number: {0}", usNumber), applicationSubmission);

            // create contact
            try
            {
                contactId = CrmActions.GetContactId(usNumber);
                CrmActions.CreateOrUpdateContact(usNumber, applicationSubmission, contactId);
                CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission);
            }
            catch (Exception exception)
            {
                logger.LogWithContext(NLog.LogLevel.Error, "Could not create CRM contact", exception, applicationSubmission);
                return new ApplicationSubmissionResult() { Successful = false, Message = "Could not create contact." };
            }

            return new ApplicationSubmissionResult() { Successful = true, Message = "" };
        }

        public ApplicationSubmissionResult SubmitDocumentation(ApplicationSubmission applicationSubmission,
            Guid programmeId, Guid enrolmentId, string usNumber,
            ApplicantStatus status, string programmeName, bool? SHLTestRequired = false)
        {

            var isMarriedFemale = IsApplicantFemaleAndMarried(enrolmentId);
            //var isRPLCandidate = IsRPLCandidate(enrolmentId);

            applicationSubmission.SHLTestRequiredTwoOptions = SHLTestRequired;
            applicationSubmission.EnrolmentGuid = enrolmentId;

            if (!applicationSubmission.DocumentUploadNames.Contains(SettingsMan.Default.DeclarationDocumentName))
            {
                return new ApplicationSubmissionResult()
                {
                    Successful = false,
                    Message = "Student Declaration form has to be upload and submitted",
                    ErrorId = SettingsMan.Default.DeclarationDocumentName
                };
            }

            List<Document> requiredDocumentList = null;

            if (status == ApplicantStatus.Applicant)
            {
                // Required Documents
                requiredDocumentList = CrmQueries.GetRequiredDocuments(programmeId);

                // Dependent Documents
                requiredDocumentList.AddRange(CrmQueries.GetRequiredDocuments(programmeId, 2));
            }
            else if (status == ApplicantStatus.Admitted)
            {
                requiredDocumentList = CrmQueries.GetRequiredDocuments(programmeId, 3);
            }

            requiredDocumentList = ResolveMarriageCertRequired(isMarriedFemale, requiredDocumentList);
            //requiredDocumentList = ResolveRPLCandidate(isRPLCandidate, requiredDocumentList);
            requiredDocumentList = ResolveSHLGMATDocs(SHLTestRequired.Value, requiredDocumentList);

            applicationSubmission.DocumentsOutstandingTwoOptions = false;
            applicationSubmission.DocumentsCompleteTwoOptions = true;

            foreach (Document document in requiredDocumentList)
            {
                if (!applicationSubmission.DocumentUploadKeys.Contains(document.DocumentId) &&
                    !applicationSubmission.DocumentExclusionKeys.Contains(document.DocumentId))
                {
                    applicationSubmission.DocumentsOutstandingTwoOptions = true;
                }
            }

            if (applicationSubmission.DocumentsOutstandingTwoOptions == true)
            {
                applicationSubmission.DocumentsCompleteTwoOptions = false;
            }

            if (CrmFileUpload.DoesDocumentLocationForEnrollmentExist(enrolmentId) == false)
            {
                CrmFileUpload.CreateDocumentLocationForEnrollment(enrolmentId);
            }

            Tuple<string, byte[]> documentData;
            string fileName;
            byte[] fileBytes;



            foreach (Document document in requiredDocumentList)
            {
                var reviewed = "No";
                if (Cache.ContainsItem(enrolmentId.ToString(), document.DocumentTypeId.ToString()) &&
                    applicationSubmission.DocumentUploadKeys.Contains(document.DocumentId))
                {
                    documentData = Cache.GetCachedItem(enrolmentId.ToString(), document.DocumentTypeId.ToString());
                    fileName = documentData.Item1;
                    fileBytes = documentData.Item2;

                    //Get document info from Sharepoint
                    var fileMetaData = SharepointActions.GetItemMetaData(document.DocumentTypeId.GetValueOrDefault(),
                        enrolmentId);

                    if (fileMetaData.IsNotNull())
                    {
                        reviewed = (string) fileMetaData["Reviewed"];
                    }

                    SharepointActions.UploadDocument(fileName, usNumber, document.Level, programmeName,
                        document.Description,
                        document.DocumentId, enrolmentId, fileBytes, reviewed);

                    Cache.RemoveItem(enrolmentId.ToString(), document.DocumentTypeId.ToString());
                }
                else if (Cache.ContainsItem(enrolmentId.ToString(), document.DocumentTypeId.ToString()) &&
                         !applicationSubmission.DocumentUploadKeys.Contains(document.DocumentId))
                {
                    Cache.RemoveItem(enrolmentId.ToString(), document.DocumentTypeId.ToString());
                    // Cleanup share point if necessary
                    SharepointActions.DeleteDocument(document.DocumentTypeId ?? Guid.Empty, enrolmentId);
                }
                else
                {
                    // Cleanup share point if necessary
                    SharepointActions.DeleteDocument(document.DocumentTypeId ?? Guid.Empty, enrolmentId);
                }
            }

            CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission);

            return new ApplicationSubmissionResult() {Successful = true, Message = ""};
        }

        public ApplicationSubmissionResult SubmitPaymentDetails(ApplicationSubmission applicationSubmission, Guid? enrollmentId, string usNumber)
        {
            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            var contactCreation = CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission);

            return new ApplicationSubmissionResult() { Successful = true, Message = "" };
        }

        public ApplicationSubmissionResult SubmitStatus(ApplicationSubmission applicationSubmission,
            Guid offeringGuidstring, string usNumber)
        {
            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission);

            return new ApplicationSubmissionResult() {Successful = true, Message = ""};
        }

        public void ExecuteApplicationAcknowledgmentWorkFlow(string usNumber, Guid? enrolmentId = null)
        {
            var sarId = new Guid();
            //var contactId = GetContactId(usNumber);
            if (enrolmentId.IsNull())
            {
                sarId = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }
            else
            {
                sarId = enrolmentId.GetValueOrDefault();
            }

            //TEMP
            var workflowId = SettingsMan.Default.WorkFlowGUID;
            CrmActions.ExecuteApplicationAcknowledgmentWorkFlow(workflowId, sarId);
        }

        #region Private Methods

        public List<Document> ResolveMarriageCertRequired(bool isMarriedFemale, List<Document> requiredDocumentList)
        {
            if (!isMarriedFemale)
            {
                var marriageCertificate = requiredDocumentList.FirstOrDefault(d => d.Description == SettingsMan.Default.MarriageCertificateName);
                requiredDocumentList.Remove(marriageCertificate);
            }

            return requiredDocumentList;
        }

        public List<Document> ResolveRPLCandidate(bool isRPLCandidate, List<Document> requiredDocumentList)
        {
            if (!isRPLCandidate)
            {
                var rplRef1 = requiredDocumentList.FirstOrDefault(d => d.Description == SettingsMan.Default.RPLRef1Name);
                requiredDocumentList.Remove(rplRef1);

                var rplRef2 = requiredDocumentList.FirstOrDefault(d => d.Description == SettingsMan.Default.RPLRef2Name);
                requiredDocumentList.Remove(rplRef2);

                var rplCandidate = requiredDocumentList.FirstOrDefault(d => d.Description == SettingsMan.Default.RPLCandidateName);
                requiredDocumentList.Remove(rplCandidate);
            }

            return requiredDocumentList;
        }

        public List<Document> ResolveSHLGMATDocs(bool isSHLTestRequired, List<Document> requiredDocumentList)
        {
            if (isSHLTestRequired)
            {
                var GMATSHLResults = requiredDocumentList.FirstOrDefault(d => d.Description == SettingsMan.Default.GMAT_SHLResultsName);
                requiredDocumentList.Remove(GMATSHLResults);

                var GMATSelectionTest = requiredDocumentList.FirstOrDefault(d => d.Description == SettingsMan.Default.GMATSelectionTestName);
                requiredDocumentList.Remove(GMATSelectionTest);
            }

            return requiredDocumentList;
        }

        #endregion

    }
}
