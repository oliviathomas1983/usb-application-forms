﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public class AcademicHistoryEntry
    {
        public string Qualification
        {
            get;
            set;
        }

        public string Field
        {
            get;
            set;
        }

        public string Institution
        {
            get;
            set;
        }

        public int YearAchieved
        {
            get;
            set;
        }
    }
}
