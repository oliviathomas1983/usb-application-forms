﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB.International.Domain.Controller.Dto.Enums;
using USB.International.Domain.Models;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public class ApplicationSubmission
    {
        public Guid Offering { get; set; }

        public ApplicationSource ApplicationSource { get; set; }

        public Guid? ContactIdGuid { get; set; }
        public Guid? EnrolmentGuid { get; set; }

        public string Surname { get; set; }
        public string FirstNames { get; set; }
        public string Initials { get; set; }
        public string GivenName { get; set; }
        public Guid? Title { get; set; }
        public Guid? Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public string NameOnCertificate { get; set; }

        public Guid? PermitType { get; set; }
        public string IdNumber { get; set; }
        public Guid? Ethnicity { get; set; }
        public Guid? MaritalStatus { get; set; }
        public Guid? Nationality { get; set; }
        public Guid? CountryOfIssue { get; set; }
        public IdentificationType ForeignIdType { get; set; }
        public string PassportNumber { get; set; }
        public DateTime? PassportExpiryDate { get; set; }
        public string ForeignIdNumber { get; set; }
        public DateTime? ForeignIdExpiryDate { get; set; }

        public Guid? CorrespondanceLanguage { get; set; }
        public Guid? Language { get; set; }
        public string MaidenName { get; set; }
        public Guid? DietaryRequirements { get; set; }
        public string DietaryRequirementsOther { get; set; }
        public Guid? Disability { get; set; }
        public string OtherDisability { get; set; }
        public bool UsesWheelchair { get; set; }

        public bool? IsSouthAfrican { get; set; }

        public bool? PersonalInformationTwoOptions { get; set; }
        public bool? AddressDetailsTwoOptions { get; set; }
        public bool? WorkStudiesTwoOptions { get; set; }
        public bool? TellUsMoreTwoOptions { get; set; }
        public bool? DocumentationTwoOptions { get; set; }

        public bool? PaymentTwoOptions { get; set; }
        public bool? StatusTwoOptions { get; set; }
        public bool? SHLTestRequiredTwoOptions { get; set; }
        public bool? DocumentsOutstandingTwoOptions { get; set; }
        public bool? DocumentsCompleteTwoOptions { get; set; }

        public bool? ApplicationSubmittedTwoOptions { get; set; }

        public bool? Round1InterviewReqTwoOptions { get; set; }
        public string WorkContactTelephone { get; set; }
        public int? EmployerAssistFinancially { get; set; }
        public string EmployerEmail { get; set; }
        public string EmployerTelephone { get; set; }
        public string EmployerName { get; set; }

        public bool usb_SearchEngineTwoOptions { get; set; }

        public int? usb_CareerChangeOptionSet { get; set; }

        public int? usb_ProgressCareerOptionSet { get; set; }
        public int? usb_AcquireManagementSkillsOptionset { get; set; }
        public int? usb_DevelopAsALeaderOptionset { get; set; }
        public int? usb_AcquireBusinessKnowledgeOptionset { get; set; }
        public int? usb_AquireThinkingSkillsOptionset { get; set; }
        public int? usb_ResponsibleLeader { get; set; }
        public int? usb_HigherSalary { get; set; }
        public int? usb_GainKnowledge { get; set; }

        public int? usb_LecturerQaulityOptionset { get; set; }
        public int? usb_ElectronicStudyMaterialOptionset { get; set; }
        public int? usb_StudyMaterialOptionset { get; set; }
        public int? usb_ResearchSupervisionOptionset { get; set; }
        public int? usb_ICTSupportOptionset { get; set; }
        public int? usb_AvailableCareerOptionset { get; set; }
        public int? usb_LibraryServicesOptionset { get; set; }

        public int? usb_AcademicSupportOptionset { get; set; }
        public int? usb_MentorsOptionset { get; set; }
        public int? usb_CampusSecurityOptionset { get; set; }
        public int? usb_CateringServicesOptionset { get; set; }

        public int? usb_AdminSupportOptionset { get; set; }
        public int? usb_OffcampusSupportOptionset { get; set; }
        public int? usb_DiverseClassGroupOptionset { get; set; }
        public int? usb_DiverseFaultyCompositeOptionset { get; set; }
        public int? usb_BlendedLearningOptionset { get; set; }
        public int? usb_ClassroomBasedOptionset { get; set; }
        public bool? usb_advertisementTwoOptions { get; set; }
        public bool? usb_PromotionalEmailsTwoOptions { get; set; }
        public bool? usb_ReferralByEmployerTwoOptions { get; set; }
        public bool? usb_ReferralByAlumnusTwoOptions { get; set; }
        public bool? usb_ReferralByCurrentStudentTwoOptions { get; set; }
        public bool? usb_ReferralByFamilyFriendTwoOptions { get; set; }
        public bool? usb_SocialMediaTwoOptions { get; set; }
        public bool? usb_NewsArticleTwoOptions { get; set; }
        public bool? usb_ConferenceExpoTwoOptions { get; set; }
        public bool? usb_StellenboschUniversityTwoOptions { get; set; }
        public bool? usb_USBWebsiteTwoOptions { get; set; }
        public bool? usb_USBEdTwoOptions { get; set; }

        public bool? usb_BrochureTwoOptions { get; set; }
        public bool? usb_OnCampusEventTwoOptions { get; set; }
        public bool? usb_TripleAccreditationTwoOptions { get; set; }
        public bool? usb_ValueForMoneyTwoOptions { get; set; }
        public bool? usb_RecommendedByAlumniTwoOptions { get; set; }
        public bool? usb_CloseToHomeTwoOptions { get; set; }
        public bool? usb_ExcellentReputationTwoOptions { get; set; }
        public bool? usb_RecommendedByEmployerTwoOptions { get; set; }
        public bool? usb_HighQualityStudentsTwoOptions { get; set; }
        public bool? usb_ProgrammeFormatTwoOptions { get; set; }
        public bool? usb_USBInternationalTwoOptions { get; set; }
        public bool? usb_USBAfricaFocusTwoOptions { get; set; }
        public bool? usb_USBGeneralLeadershipFocusTwoOptions { get; set; }
        public bool? usb_USBCollaborativeLearningTwoOptions { get; set; }
        public bool? usb_IAmAMatieTwoOptions { get; set; }
        public bool? usb_InternationalLecturersInClassTwoOptions { get; set; }
        public bool? usb_InternationalStudentsTwoOptions { get; set; }
        public bool? usb_DeliveryModeTwoOptions { get; set; }
        public int? usb_AcquireDevFinSkillsOptionset { get; set; }
        public int? usb_AfricaSustainableDevelopmentOptionset { get; set; }
        public int? usb_AfricaEmergingMarketOptionset { get; set; }
        public int? usb_DevelopSAEconomyOptionset { get; set; }
        public int? usb_AfricaMultiNationalEnvironmentOptionset { get; set; }
        public int? usb_AfricaStrategicDecisionOptionset { get; set; }
        public int? usb_BusinessLeaderOptionset { get; set; }
        public int? usb_InsightPoliciesProgrammesOptionset { get; set; }
        public int? usb_SelfDevelopmentOptionset { get; set; }
        public int? usb_EnhanceCoachingSkillsOptionset { get; set; }
        public int? usb_TheoreticalCoachingOptionset { get; set; }
        public int? usb_OwnCoachingPracticeOptionset { get; set; }
        public int? usb_OwnFrameworkModelOptionset { get; set; }
        public int? usb_DevelopmentOfOthersOptionset { get; set; }
        public int? usb_ProfessionalCoachesCommunityOptionset { get; set; }
        public int? usb_ProfessionalFuturistOptionset { get; set; }
        public int? usb_BenefitFromUniqueTypeOptionset { get; set; }
        public int? usb_ComplexOrgEnvironmentOptionset { get; set; }
        public int? usb_SAGrowthContributionOptionset { get; set; }
        public int? usb_TheoryOfFututeStudiesOptionset { get; set; }
        public int? usb_DevelopefutureStudiesOptionset { get; set; }
        public int? usb_AlignForFuturistOptionset { get; set; }
        public int? usb_SustainableFutureOptionset { get; set; }
        public int? usb_EntrepreneurialKnowledgeOptionset { get; set; }
        public int? usb_InnovativeOptionset { get; set; }
        public int? usb_DigitalSkillsOptionset { get; set; }
        public int? usb_ProgressToMBAOptionset { get; set; }
        public int? usb_PMSkillsOptionset { get; set; }
        public int? usb_ManageProjectsOptionset { get; set; }
        public int? usb_AquireScareSkillsOptionset { get; set; }
        public int? usb_ScenarioToolsOptionset { get; set; }
        public int? usb_AuthenticLeadershipOptioset { get; set; }
        public int? usb_MakeADifferenceOptionset { get; set; }
        public int? usb_HowToBeALeaderOptionset { get; set; }
        public int? usb_SustainableTransOptionset { get; set; }
        public int? usb_WhatDidYouDoLastYearOptionset { get; set; }

        public string usb_Referee1_Cellphone { get; set; }
        public string usb_Referee1_Position { get; set; }
        public string usb_Referee1_Name { get; set; }
        public string usb_Referee1_Telephone { get; set; }
        public string usb_Referee1_Email { get; set; }
        public string usb_Referee2_Cellphone { get; set; }
        public string usb_Referee2_Position { get; set; }
        public string usb_Referee2_Name { get; set; }
        public string usb_Referee2_Telephone { get; set; }
        public string usb_Referee2_Email { get; set; }
        public int? usb_salaryOptionset { get; set; }

        public int? usb_rplinterviewedby { get; set; }
        public int? usb_RPLCandidateTwoOptions { get; set; }

        public string TotalYearsWorkingExperience { get; set; }

        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string MobileNumber { get; set; }
        public string WorkNumber { get; set; }
        public string FaxNumber { get; set; }

        //public string PostalStreet1 { get; set; }
        //public string PostalStreet2 { get; set; }
        //public string PostalStreet3 { get; set; }
        //public string PostalSuburb { get; set; }
        //public string PostalCity { get; set; }
        //public string PostalPostalCode { get; set; }
        //public Guid? PostalAfrigis { get; set; }
        //public Guid? PostalCountry { get; set; }
        //public string PostalStateOrProvince { get; set; }
        //public string PostalAddressedTo { get; set; }

        public string ResidentialStreet1 { get; set; }
        public string ResidentialStreet2 { get; set; }
        public string ResidentialStreet3 { get; set; }
        //public string ResidentialSuburb { get; set; }
        public string ResidentialCity { get; set; }
        public string ResidentialPostalCode { get; set; }
        //public Guid? ResidentialAfrigis { get; set; }
        public Guid? ResidentialCountry { get; set; }
        public DateTime ResidentialValidUntil { get; set; }

        public Guid? EmergencyContactTitle { get; set; }
        public string EmergencyContactInitals { get; set; }
        public string EmergencyContactSurname { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactRelationship { get; set; }
        public Guid? EmergencyContactCountry { get; set; }
        public string EmergencyContactStreet1 { get; set; }
        public string EmergencyContactStreet2 { get; set; }
        public string EmergencyContactStreet3 { get; set; }
        public string EmergencyContactCityTown { get; set; }
        public string EmergencyContactPostalCode { get; set; }
        public string EmergencyContactTelephone { get; set; }
        public string EmergencyContactEmail { get; set; }

        #region Work and Studies

        public string HomeInstitution { get; set; }
        public PrincipleFieldOfStudy PrincipleFieldOfStudy { get; set; }
        public string YearsCompletedAtHomeInstitution { get; set; }
        public DurationOfExchange DurationOfExchange { get; set; }

        public List<TertiaryEducationHistory> Qualification { get; set; }
        public List<EmploymentHistory> EmploymentHistory { get; set; }

        public string YearsOfWorkingExperience { get; set; }
        public string MonthsOfWorkingExperience { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonEmailAddress { get; set; }

        #endregion

        public string UsNumber { get; set; }
        public Guid WebTokenId { get; set; }

        #region marketing

        public bool? infoSessionAttended { get; set; }
        public int? usb_InfoSessionOptionSet { get; set; }

        public bool? checkboxAdvertisement { get; set; }
        public bool? checkboxNewsArticle { get; set; }
        public bool? checkboxUsbPromotionalEmails { get; set; }
        public bool? checkboxConferenceOrExpo { get; set; }
        public bool? checkboxReferralByMyEmployer { get; set; }
        public bool? checkboxStellenboschUniversity { get; set; }
        public bool? checkboxReferralByAnAlumnus { get; set; }
        public bool? checkboxUsbWebsite { get; set; }
        public bool? checkboxReferralByACurrentStudent { get; set; }
        public bool? checkboxUsbEd { get; set; }
        public bool? checkboxReferralByAFamilyMember { get; set; }
        public bool? checkboxBrochure { get; set; }
        public bool? checkboxSocialMedia { get; set; }
        public bool? checkboxOnCampusEvent { get; set; }

        public int? radCareerChange { get; set; }
        public int? radCareerProgress { get; set; }
        public int? radManageSkills { get; set; }

        #region MPhil/PGDip DevFin 
        public int? radSpecialistSkills { get; set; }
        public int? radSustainableDev { get; set; }
        public int? radEmergingMarket { get; set; }
        public int? radDevSAEconomy { get; set; }
        public int? radManageMultiNationalBusiness { get; set; }
        public int? radStrategicDecisions { get; set; }
        public int? radInsightPoliciesProgrammes { get; set; }
        public int? radDevelopBusinessLeader { get; set; }
        #endregion

        #region MPhil/PGDip Future Studies
        public int? radFuturist { get; set; }
        public int? radUniqueProgram { get; set; }
        public int? radComplexEnviroment { get; set; }
        public int? radGrowthSAorContinent { get; set; }
        public int? radUnderstandFutures { get; set; }
        public int? radDeveloFutureModel { get; set; }
        public int? radAlignSkills { get; set; }
        public int? radSustainFuture { get; set; }
        public int? radScenarioPlanning { get; set; }
        #endregion

        #region MPhil Mangement Coaching
        public int? radDevelopMyself { get; set; }
        public int? radEnhanceCouchingSkills { get; set; }
        public int? radTheoreticalUnderstandingCouching { get; set; }
        public int? radStartOwnCoachingPractice { get; set; }
        public int? radPersonalisedCoachingModel { get; set; }
        public int? radDevelopmentOthers { get; set; }
        public int? radCommunityOfProfessionalCoaches { get; set; }
        #endregion

        #region PGDip BMA

        public int? radAcquireEntrKnowledge { get; set; }
        public int? radInnovative { get; set; }
        public int? radDigitalSkills { get; set; }
        public int? radProgressToMBA { get; set; }

        #endregion

        #region PGDipLeadership

        public int? radAuthenticLeadershipStyle { get; set; }
        public int? radMakeADifference { get; set; }
        public int? radHowToBeALeaderInSAContext { get; set; }
        public int? radSustainableTransformation { get; set; }

        #endregion

        #region PGDip Project Management

        public int? radProjectManagementSkills { get; set; }
        public int? radManageProjects { get; set; }
        public int? radScarceSkill { get; set; }

        #endregion

        public int? radDevelopLeader { get; set; }
        public int? radBusinessKnowledge { get; set; }
        public int? radCriticalThinking { get; set; }
        public int? radEthicalLeader { get; set; }
        public int? radSalary { get; set; }
        public int? radKnowBusinessStrategy { get; set; }
        public int? radLecturerQuality { get; set; }
        public int? radElectronicMaterial { get; set; }
        public int? radPrintedStudyMaterial { get; set; }
        public int? radStudyMaterial { get; set; }
        public int? radICTSupport { get; set; }
        public int? radCareerService { get; set; }
        public int? radLibraryService { get; set; }
        public int? radAcademicSupport { get; set; }
        public int? radAvailabilitySupport { get; set; }
        public int? radAvailabilityMentors { get; set; }
        public int? radCampusSecurity { get; set; }
        public int? radCateringServices { get; set; }
        public int? radAdministrativeSupport { get; set; }
        public int? radOffCampusSupport { get; set; }
        public int? radDiverseClassGroup { get; set; }
        public int? radDiverseFacultyComposition { get; set; }
        public int? radBlendedLearning { get; set; }
        public int? radClassroomDelivery { get; set; }

        public bool? chkTripleAccreditation { get; set; }
        public bool? chkValueForMoney { get; set; }
        public bool? chkRecommendedByAlumni { get; set; }
        public bool? chkGeographicLocation { get; set; }
        public bool? chkExcellentReputation { get; set; }
        public bool? chkRecommendedByEmplyer { get; set; }
        public bool? chkHighQualityStudents { get; set; }
        public bool? chkProgrammeFormat { get; set; }
        public bool? chkNetworking { get; set; }
        public bool? chkAfrica { get; set; }
        public bool? chkLeadership { get; set; }
        public bool? chkLearning { get; set; }
        public bool? chkLecturerElectives { get; set; }
        public bool? chkInternationalStudents { get; set; }
        public bool? chkDeliveryMode { get; set; }

        #endregion

        public List<int> DocumentUploadKeys { get; set; }
        public List<string> DocumentUploadNames { get; set; }

        public List<int> DocumentExclusionKeys { get; set; }

        public string ApplicationFormsBaseUrl { get; set; }

    }
}
