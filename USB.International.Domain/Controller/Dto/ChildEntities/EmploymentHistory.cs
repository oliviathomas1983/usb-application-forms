﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public class EmploymentHistory
    {
        public Guid? Id { get; set; }

        public string Employer { get; set; }
        public string JobTitle { get; set; }
        public int? WorkArea { get; set; }
        public string Organization { get; set; }
        public int? Industry { get; set; }
        public int? StartDate { get; set; }
        public int? EndDate { get; set; }
        public int? Type { get; set; }
        public bool? Latest { get; set; }
    }
}
