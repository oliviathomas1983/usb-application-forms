﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public class TertiaryEducationHistory
    {
        public Guid? Id { get; set; }

        public Guid? Institution { get; set; }
        public string OtherInstitution { get; set; }
        public string Degree { get; set; }
        public int FieldOfStudy { get; set; }
        public string StudentNumber { get; set; }
        public int? StartDate { get; set; }
        public int? EndDate { get; set; }
        public bool? Completed { get; set; }
        public bool? First { get; set; }
        public Guid? DegreeLookup { get; set; }
    }
}
