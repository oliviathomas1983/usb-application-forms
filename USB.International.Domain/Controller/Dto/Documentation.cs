﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public class Documentation
    {
        public List<Document> RequiredDocuments { get; set; }
        public List<int> ExcludedDocumentIds { get; set; }
    }
}
