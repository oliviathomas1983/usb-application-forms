﻿namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public enum ApplicantStatus : int
    {
        Nominated = 1,
        Applicant = 864480010,
        ApplicationSubmitted = 864480011,
        Admitted = 864480001,
        NotAdmitted = 864480002,
        Accepted = 864480016,
        NotAccepted = 864480018,
        Registered = 864480006,
        Passed = 864480007,
        Failed = 864480008,
        Cancelled = 864480009,
        Deactivated = 2
    }
}
