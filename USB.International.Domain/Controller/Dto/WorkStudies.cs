﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB.International.Domain.Controller.Dto.Enums;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public class Workstudies
    {
        public string HomeInstitution { get; set; }
        public PrincipleFieldOfStudy PrincipleFieldOfStudy { get; set; }
        public string YearsCompletedAtHomeInstitution { get; set; }
        public DurationOfExchange DurationOfExchange { get; set; }
        public List<TertiaryEducationHistory> TertiaryEducationHistory { get; set; }
        
        public List<EmploymentHistory> EmploymentHistory { get; set; }

        #region Current Employment

        public string YearsOfWorkingExperience { get; set; }
        public string MonthsOfWorkingExperience { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonEmailAddress { get; set; }

        #endregion
    }
}
