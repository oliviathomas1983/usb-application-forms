﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public class CRMEntityMapper
    {
        public static EmploymentHistory MapToEmploymentHistory(usb_WorkHistory application)
        {
            var employmentHistory = new EmploymentHistory()
            {
                Id = application.Id,
                Employer = application.usb_CurrentEmployer,
                JobTitle = application.usb_JobTitle,
                WorkArea = application.usb_WorkAreaOptionSet ?? 0,
                Organization = application.usb_Organisation,
                Industry = application.usb_Industry ?? 0,
                StartDate = application.usb_PeriodFrom?.AddYears(1).Year ?? 0,
                EndDate = application.usb_PeriodTo?.AddYears(1).Year ?? 0,
                Type = application.usb_TypeOfOrganisationOptionset,
                Latest = application.usb_LatestJobIndicatorTwoOptions ?? false
                
                //Employer = application.usb_CurrentEmployer,
                //JobTitle = application.usb_JobTitle,
                //WorkArea = application.usb_WorkArea.IsNotNull() ? application.usb_WorkArea : 0,
                //TypeOfOrganisation = application.usb_TypeOfOrganisation.IsNotNull() ? application.usb_TypeOfOrganisation : 0,
                //OrganizationOther = application.usb_Organisation,
                //Industry = application.usb_Industry.IsNotNull() ? application.usb_Industry : 0,
                //StartDate = application.usb_PeriodFrom?.AddYears(1).Year ?? 0,
                //EndDate = application.usb_PeriodTo?.AddYears(1).Year ?? 0,
                //OccupationalCategory = application.usb_Industry.IsNotNull() ? application.usb_Industry : 0,
                
                
                //Latest = application.usb_LatestJobIndicator.IsNotNull() ? application.usb_LatestJobIndicator : false
            };

            return employmentHistory;
        }

        public static TertiaryEducationHistory MapToTertiaryEducationHistory(usb_HighestQualification application)
        {
            var employmentHistory = new TertiaryEducationHistory()
            {
                Id = application.Id,
                Institution = application.usb_AcademicInstitutionLookup.IsNotNull() ? application.usb_AcademicInstitutionLookup.Id : Guid.Empty,
                OtherInstitution = application.usb_AcademicInstitutionOther,
                Completed = application.usb_Completed ?? false,
                FieldOfStudy = application.usb_FieldOfStudyOptionset ?? 0,
                StartDate = application.usb_PeriodFrom?.AddYears(1).Year ?? 0,
                EndDate = application.usb_PeriodTo?.AddYears(1).Year ?? 0,
                Degree = application.usb_Qualification,
                StudentNumber = application.usb_StudentNumber,
                First = application.usb_FirstQualificationTwoOptions ?? false

                //Id = application.Id,
                //Completed = application.usb_Completed.GetValueOrDefault(),                
                //DegreeLookup = application.usb_HighestQualificationLookup?.Id ?? Guid.Empty,
                //EndDate = application.usb_PeriodTo?.AddYears(1).Year ?? 0,
                //FieldOfStudy = application.usb_FieldOfStudyOptionset.IsNotNull() ? application.usb_FieldOfStudy.Value : 0,
                //First = application.usb_FirstQualificationTwoOptions.GetValueOrDefault(),
                //Institution = application.usb_AcademicInstitution?.Id ?? Guid.Empty,
                //OtherInstitution = application.usb_AcademicInstitutionOther,
                //StartDate = application.usb_PeriodFrom?.AddYears(1).Year ?? 0,
                //StudentNumber = application.usb_StudentNumber
            };

            return employmentHistory;
        }

    }
}
