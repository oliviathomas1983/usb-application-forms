﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using USB.International.Domain.Controller.Dto.Enums;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public partial class EnrolmentMapper
    {
        public static Workstudies MapToWorkStudies(Contact application)
        {
            return new Workstudies()
            {
                HomeInstitution = application.usb_AcademicInstitutionHome,
                PrincipleFieldOfStudy = (PrincipleFieldOfStudy)Enum.ToObject(typeof(PrincipleFieldOfStudy), application.usb_FieldOfStudyHome.GetValueOrDefault()),
                YearsCompletedAtHomeInstitution = application.usb_NoOfYearsCompleted.GetValueOrDefault().ToString(),
                DurationOfExchange = (DurationOfExchange)Enum.ToObject(typeof(DurationOfExchange), application.usb_DurationofExchange.GetValueOrDefault()),
                YearsOfWorkingExperience = application.usb_TotalYearsOfWorkExperience,
                MonthsOfWorkingExperience = application.usb_totalmonthsofworkexperience,
                ContactPersonName = application.usb_contactnamehome,
                ContactPersonEmailAddress = application.usb_contactemailhome,

                EmploymentHistory = new List<EmploymentHistory>(),
                TertiaryEducationHistory = new List<TertiaryEducationHistory>()

                //ContactPersonEmailAddress = studentAcademicRecord.usb_EmployerEmail,
                //ContactPersonName = studentAcademicRecord.usb_EmployerName,
                //ContactPersonTelephone = studentAcademicRecord.usb_EmployerTelephone,
                //EnglishProficiencyReading = studentAcademicRecord.usb_EnglishReadOptionset,
                //EnglishProficiencySpeaking = studentAcademicRecord.usb_EnglishSpeakOptionset,
                //EnglishProficiencyUnderstanding = studentAcademicRecord.usb_EnglishUnderstandOptionset,
                //EnglishProficiencyWriting = studentAcademicRecord.usb_EnglishWriteOptionset,
                //MathematicsCompetency = studentAcademicRecord.usb_HighestLevelMathematicsCompetedOptionset,
                //MathematicsPercentage = studentAcademicRecord.usb_SymbolAchieved,

                //PriorInvolvement = studentAcademicRecord.usb_WhatDidYouDoLastYearOptionset,
                //YearsOfWorkingExperience = studentAcademicRecord.usb_TotalYearsWorkingExperience,
                //WillEmployerBeAssistingFinancially = studentAcademicRecord.usb_EmployerassistfinanciallyOptionset
            };
        }
    }
}
