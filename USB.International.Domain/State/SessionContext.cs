﻿using System;
using System.Collections.Generic;
using System.Web;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using USB.International.Domain.Models;
using USB.International.Domain.Utilities;

namespace USB.International.Domain.State
{
    public class SessionContext
    {
        private const string EnrolmentGuidString = "EnrollmentGuid";
        private const string EmailString = "email";
        private const string UsNumberString = "UsNumber";
        private const string ContactIdString = "ContactId";
        private const string idParameter = "idParameter";
        private const string reviewIdParameter = "reviewIdParameter";
        private const string programmeGuid = "programmeGuid";
        private const string programmeName = "programmeName";
        private const string webTokenGuid = "webTokenId";
        private const string applicantStatusString = "applicantStatus";
        private const string insideCRMString = "insideCRM";
        private const string appFormSubmittedString = "appFormSubmitted";

        public SessionContext(string applicantStatus, Guid enrolmentId, bool insideCrm, Guid programmeId,
            string usNumber, string programmeName, bool? shlTestRequired)
        {

            ApplicantStatus = applicantStatus;
            EnrolmentId = enrolmentId;
            InsideCRM = insideCrm;
            ProgrammeId = programmeId;
            UsNumber = usNumber;
            ProgrammeName = programmeName;
        }

        public SessionContext()
        {
        }

        public static SessionContext FromSession(HttpSessionStateBase session)
        {
            var result = new SessionContext
            {
                ApplicantStatus = session[applicantStatusString] == null ? null : new Enum<ApplicantStatus>((int)session[applicantStatusString]).Name,
                EnrolmentId = (Guid?) session[EnrolmentGuidString] ?? Guid.Empty,
                ProgrammeId = (Guid?) session[programmeGuid] ?? Guid.Empty,
                InsideCRM = (bool?) session[insideCRMString] ?? false,
                UsNumber = (string) session[UsNumberString]
            };
            return result;
        }

        public string ApplicantStatus { get; set; }
        public Guid EnrolmentId { get; set; }
        public bool InsideCRM { get; set; }
        public Guid ProgrammeId { get; set; }
        public string UsNumber { get; set; }
        public string ProgrammeName { get; internal set; }
        public bool IncludeGMAT { get; set; }
        public string RootUrl { get; set; }

        public IList<FileUploadSection> Sections { get; set; }

        public ApplicantStatus? ApplicantStatusEnumValue
        {
            get
            {
                if (String.IsNullOrEmpty(ApplicantStatus))
                    return null;

                return new Enum<ApplicantStatus>(ApplicantStatus).Value;
            }
        }

        public bool IsValid()
        {

            return this.EnrolmentId != Guid.Empty
                   && !String.IsNullOrEmpty(this.UsNumber) && ProgrammeId != Guid.Empty
                   && !String.IsNullOrEmpty(this.ApplicantStatus)
                   && Enum.Parse(typeof(ApplicantStatus), this.ApplicantStatus) != null;
        }

    }
}
