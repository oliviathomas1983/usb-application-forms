﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.International.Domain.Utilities
{
    public interface IEnum<T>
    {
        T Value { get; }
        string Name { get; }
        int Code { get; }
        bool HasFlag(T flag);
        bool HasFlag(int flag);
        bool HasAnyFlag(params T[] flag);
        bool HasAnyFlags(IEnumerable<T> flag);
        IEnumerable<int> Indexes();
        T this[int code] { get; }
    }
}
