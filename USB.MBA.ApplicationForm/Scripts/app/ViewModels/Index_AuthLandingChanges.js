﻿function Index() {
    var self = this;
  
    var rootUrl = "/MBA/";

    self.emailAddress = ko.observable().extend({ required: true });
    //self.usNumber = ko.observable().extend({ required: true });
    //self.idNumber = ko.observable().extend({ required: true });
    self.PersonalDetails_url = "Page/PersonalDetails";

    //self.IsApplicant = ko.observable(false);
    self.IsApplicant = ko.observable(false);
    self.IsApplicant2 = ko.observable(false);
    self.mailSend = ko.observable(false);
    self.requirementsUnderstood = ko.observable(false);
    self.IsApplicant3 = ko.observable(false);
    

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        return pattern.test(emailAddress);
    };

    var Index = {
        eMail: self.emailAddress
    }

    Index.errors = ko.validation.group(Index);

    self.sendEMail = function () {
        var form = $('#__AjaxAntiForgeryForm');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        $.ajax({
            url: rootUrl + "Page/SendMail",//?email=" + self.emailAddress(),
            contentType: 'application/json',
            data: ko.toJSON({
                __RequestVerificationToken: token,
                email: self.emailAddress()
            }),
            type: 'POST',
            //async: false,
            success: function (j) {
                if (j.Success == true) {
                    self.mailSend(true);
                }
                else {
                    self.mailSend(false);
                }
            },
            error: function (error) {
                console.log(error.responseText);
                self.mailSend(false);
            }
        });
    }

    self.Proceed = function () {
        var form = $('#__AjaxAntiForgeryForm');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        $.ajax({
            url: rootUrl + "Page/Proceed?email=" + self.emailAddress() + "&usNumber=" + self.UsNumber + "&idNumber=" + self.idNumber,
            contentType: 'application/json',
            data: ko.toJSON({
                __RequestVerificationToken: token,
                email: self.emailAddress()
            }),
            type: 'POST',
            //async: false,
            success: function (j) {
                if (j != "") {
                    window.location = j;
                    /*rootUrl + self.PersonalDetails_url + "?email=" + self.emailAddress();*/
                }
                else {

                    //   $("#ValidateStudentDetails").hide();
                    $("#ForgotDetails").show();

                }
            },
            error: function (error) {
                console.log(error.responseText);
                self.mailSend(false);
            }
        });
    }
    self.ProceedPopulate = function () {
        var form = $('#__AjaxAntiForgeryForm');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        $.ajax({
            url: rootUrl + "Page/ProceedPopulate?email=" + self.emailAddress() + "&usNumber=" + self.UsNumber + "&idNumber=" + self.idNumber,
            contentType: 'application/json',
            data: ko.toJSON({
                __RequestVerificationToken: token,
                email: self.emailAddress()
            }),
            type: 'POST',
            //async: false,
            success: function (j) {
                if (j != "") {
                    window.location = rootUrl + self.PersonalDetails_url + "?email=" + self.emailAddress();
                    //window.location = j;
                    /*rootUrl + self.PersonalDetails_url + "?email=" + self.emailAddress();*/
                }
                else {

                    //   $("#ValidateStudentDetails").hide();
                    $("#ForgotDetails").show();

                }
            },
            error: function (error) {
                console.log(error.responseText);
                self.mailSend(false);
            }
        });
    }

    self.Cancel = function () {
        if (Index.errors().length === 0) {
            var form = $('#__AjaxAntiForgeryForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            $.ajax({
                url: rootUrl + "Page/Cancel",
                contentType: 'application/json',
                data: ko.toJSON({
                    __RequestVerificationToken: token,
                    email: self.emailAddress()

                }),
                type: "POST",
                success: function (j) {
                   
                        window.location = rootUrl + self.PersonalDetails_url + "?email=" + self.emailAddress();
                   
                 
                },
                error: function (error) {
                    console.log(error.responseText);
              
                }
            }).done(function (response) {
                console.log(response);
            });
        }
        else {
            Index.errors.showAllMessages();
            window.scrollTo(0, 0);
        }
    }
    //bool SarFound = false;
    //bool DegreeFound = false;
    //bool CurrentYear = false;
    //bool CancelledSar = false;

    self.Continue = function () {
        //Check if email valid
        if (!isValidEmailAddress(self.emailAddress())) {
            self.emailAddress.setError("Please enter a valid email address");
        }

          if (Index.errors().length === 0) {            
            var form = $('#__AjaxAntiForgeryForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            $.ajax({
                url: rootUrl + "Page/SubmitIndex",
                contentType: 'application/json',
                data: ko.toJSON({
                    __RequestVerificationToken: token,
                    email: self.emailAddress()

                }),
                type: "POST",
                success: function (j) {
                    if (j.NewApplicant) {
                        window.location = rootUrl + self.PersonalDetails_url + "?email=" + self.emailAddress();
                    }
                    else if (j.RequestSarPopulate) {
                        //Show a wanna populate  and display auth after 
                       
                        self.IsApplicant3(true);
                    }
                    else if (j.SendEmailDisplay)
                    {
                        //Display Email Link send
                        self.IsApplicant(true);
                    }
                    else {
                        self.IsApplicant2(true);
                    }
                },
                error: function (error) {
                    console.log(error.responseText);
                    self.IsApplicant(false);
                    self.IsApplicant2(false);
                    self.IsApplicant3(false);
                    waitingDialog.hide();
                }
            }).done(function (response) {
                console.log(response);
            });
        }
        else {
            Index.errors.showAllMessages();
            window.scrollTo(0, 0);
        }

    }
 }

//////////////////////////////////////////////////////////
    
//old submit and check for sar history///////////////
//    self.Continue = function () {
       
//        if (!isValidEmailAddress(self.emailAddress()))
//        {
//            self.emailAddress.setError("Please enter a valid email address");
//        }

//        if (Index.errors().length === 0) {
//            //waitingDialog.show('Loading');
//            var form = $('#__AjaxAntiForgeryForm');
//            var token = $('input[name="__RequestVerificationToken"]', form).val();
//            $.ajax({
//                url: rootUrl + "Page/SubmitIndex",//?email=" + self.emailAddress(),
//                contentType: 'application/json',
//                data: ko.toJSON({
//                    __RequestVerificationToken: token,
//                    email: self.emailAddress()

//                }),
//                type: "POST",
//                success: function (j) {
//                    if (j.HasSARWithApplicantStatus) {
//                        //self.IsApplicant(true);  
//                        self.IsApplicant2(true);
//                       // self.mailSend(false);
                       
//                        waitingDialog.hide();
//                    } else {
                       
//                        window.location = rootUrl + self.PersonalDetails_url + "?email=" + self.emailAddress();
//                    }
//                },
//                error: function (error) {
//                    console.log(error.responseText);
//                    //self.IsApplicant(false);
//                    self.IsApplicant2(false);
//                    waitingDialog.hide();
//                }
//            }).done(function (response) {
//                console.log(response);
//            });
//        }
//        else {
//            Index.errors.showAllMessages();
//            window.scrollTo(0, 0);
//        }
       
//    }
//}

ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindings, vm) {
        ko.utils.registerEventHandler(element, "keyup", function (event) {
            if (event.keyCode === 13) {
                ko.utils.triggerEvent(element, "change");
                valueAccessor().call(vm, vm); //set "this" to the data and also pass it as first arg, in case function has "this" bound
            }

            return true;
        });
    }
};

$(document).ready(function () {

    $("#tabs").hide();

    $.ajaxSetup({
        contentType: 'application/json'
    });
    vm = new Index();
    ko.applyBindings(vm);
});




