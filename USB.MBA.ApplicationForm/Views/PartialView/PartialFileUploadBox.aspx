﻿<div class="row form-section documentation-wrap" 
     data-bind="visible: !$data.IsExcluded(),
                style: {
                    borderColor: (($root.errorId() === $data.Description()) && ($data.Status() !== 'Received')) ? 'red' : '#B2BEC3',
                    backgroundColor: (($root.errorId() === $data.Description()) && ($data.Status() !== 'Received')) ? '#FFEEEE' : 'white'
                }">
    <div class="col-md-6">
        <div class="help-block file-upload-block">
            <div class="upload">
                <div>
                    <label data-bind="text: $data.Description" class=""></label>
                </div>
                <div class="row" style="padding-top: 10px">
                    <div class="col-sm-4">
                        <button type="button" class="btn btnSaveProceed files-label fileChooser" 
                                data-bind="click: $root.triggerFileUpload.bind($data, $index()), disable: $data.Reviewed,
                                           visible: !$root.isDisabled() && (!$root.isAdmittedApplicant() || ($data.Level() >= 3))">Upload</button>
                        <input class="fileUpload" 
                               data-bind="attr: { id: 'fileUpload' + $index() }"
                               type="file" name="files[]" style="visibility: hidden" />
                    </div>
                    
                    <div class="col-sm-offset-1 col-sm-7" style="padding-top: 5px; visibility: hidden">
                        <div class="row">
                            <span data-bind="attr: { id: 'progressLabel' + $index() }" 
                                  class="col-sm-6">
                                Uploading...
                            </span>  
                            <span data-bind="attr: { id: 'progressPecentage' + $index() }" 
                                  class="col-sm-2 col-sm-offset-4">0 %</span>
                        </div>
                        <div class="progress" style="height: 10px; background-color: #918B29;">
                            <div data-bind="attr: { id: 'progressBar' + $index() }" 
                                 class="progress progress-striped" 
                                 role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" 
                                 style="width: 0%; height:10px; background-color: #992f40;">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 upload-details" style="display: block;padding: 40px 30px;">
        <div class="row">
            <div class="col-xs-4">
                <label class="">File name</label>
                <div><a data-bind="text: $data.FileName,
                                   attr: { href: '../Documentation/DownloadFile?documentId=' + $data.Id() }"></a></div>
            </div>
            <div class="col-xs-8">
                <div class="row">
                    <div class="col-lg-2">
                        <label>Delete</label>
                        <button data-bind="visible: $data.Status() === 'Received', disable: $root.isDisabled() || ($root.isAdmittedApplicant() && ($data.Level() < 3)) || !$root.isDeleteEnabled(),
                                           click: $root.deleteDocument" 
                                class="btn btn-sm">Delete</button>
                    </div>
                    <div class="col-lg-4">
                        <label class="">Date uploaded</label>
                        <div>
                            <span class="date" data-bind="text: $data.UploadedDate"></span>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label class="">Status</label>
                        <div>
                            <span class="upload-status" data-bind="text: $data.Status"></span>
                        </div>
                    </div>
                </div>
                <div style="padding-top: 20px" data-bind="visible: $data.Status() !== 'Received'">
                    <i><p data-bind="text: $data.NotUploadedStatus"></p></i>
                </div>
            </div>
        </div>
        <div data-bind="visible: ($data.Status() === 'Received') && $root.isReviewEnabled(),
                        disable: $root.isReviewEnabled() && $root.isAdmittedApplicant() && ($data.Level() < 3) " 
             style="left:0;bottom:0;position:absolute;">

             <input type="checkbox" data-bind="checked: $data.Reviewed"/>&nbsp;Reviewed
        </div>
    </div>
</div>

