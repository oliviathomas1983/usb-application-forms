﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using USB.Domain;
using USB.Domain.Models;

namespace USB.MBA.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IUSBMBAService
    {
        [OperationContract]
        ApplicationSubmissionResult SavePersonalDetails(string personalDetails);

        [OperationContract]
        ApplicationSubmissionResult SaveAddressDetails(string addressDetails);

        [OperationContract]
        ApplicationSubmissionResult SaveWorkDetails(string workDetails);

        [OperationContract]
        ApplicationSubmissionResult SaveDocument(DocumentType documentType, byte[] binary, Guid id);

        [OperationContract]
        ApplicationSubmissionResult DeleteDocument(DocumentType documentType, Guid id);

    }
}
