﻿function Index() {
    var self = this;
    
    var rootUrl = "/ShortenedAppFormMBA/";

    self.emailAddress = ko.observable().extend({ required: true });
    self.studentNumber = ko.observable();

    self.studentHistory = ko.observableArray([]);
    self.selectedStudentHistory = ko.observable().extend({ required: true });

    self.canContinue = ko.observable(true);

    self.selectedStudentHistory.subscribe(function(item) {
        if (item != undefined) {
            self.emailAddress(item.EmailAddress);
            self.studentNumber(item.UsNumber);
        }
    });

    self.PersonalDetails_url = "Page/PersonalDetails";

    self.IsApplicant = ko.observable(false);
    self.mailSend = ko.observable(false);
    self.requirementsUnderstood = ko.observable(false);

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        return pattern.test(emailAddress);
    };

    var Index = {
        eMail: self.emailAddress,
        studentNumber: self.studentNumber
    }

    Index.errors = ko.validation.group(Index);

    self.sendEMail = function () {
        var form = $('#__AjaxAntiForgeryForm');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        $.ajax({
            url: rootUrl + "Page/SendMail",//?email=" + self.emailAddress(),
            contentType: 'application/json',
            data: ko.toJSON({
                __RequestVerificationToken: token,
                email: self.emailAddress()
            }),
            type: 'POST',
            //async: false,
            success: function (j) {
                if (j.Success == true) {
                    self.mailSend(true);
                }
                else {
                    self.mailSend(false);
                }
            },
            error: function (error) {
                console.log(error.responseText);
                self.mailSend(false);
            }
        });
    }

    self.GetStudentHistory = function() {
        self.canContinue(true);
        if (Index.errors().length === 0) {
            self.toggleLoadingOverlay("show");
            var form = $('#__AjaxAntiForgeryForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            $.ajax({
                url: rootUrl + "Page/SubmitStudentDetails",
                contentType: 'application/json',
                data: ko.toJSON({
                    __RequestVerificationToken: token,
                    email: self.emailAddress(),
                    studentNumber: self.studentNumber()
                }),
                type: "POST",
                success: function (result) {

                    if (result != undefined) {
                        if (result.length > 0) {
                            self.studentHistory(result);
                        } else {
                            self.canContinue(false);
                        }
                    }
                },
                error: function (error) {
                    console.log(error.responseText);
                    self.IsApplicant(false);
                    waitingDialog.hide();
                }
            }).done(function (response) {
                self.toggleLoadingOverlay("hide");
                console.log(response);
            });
        }
        else {
            self.toggleLoadingOverlay("hide");
            Index.errors.showAllMessages();
            window.scrollTo(0, 0);
        }

    };

    self.Continue = function () {
       
        if (!isValidEmailAddress(self.emailAddress()))
        {
            self.emailAddress.setError("Please enter a valid email address");
        }

        if (Index.errors().length === 0 && self.selectedStudentHistory()) {
            //waitingDialog.show('Loading');
            var form = $('#__AjaxAntiForgeryForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            $.ajax({
                url: rootUrl + "Page/SubmitIndex",//?email=" + self.emailAddress(),
                contentType: 'application/json',
                data: ko.toJSON({
                    __RequestVerificationToken: token,
                    email: self.emailAddress(),
                    studentNumber: self.studentNumber()
                }),
                type: "POST",
                success: function (j) {
                    if (j.HasSARWithApplicantStatus) {
                        self.IsApplicant(true);
                        waitingDialog.hide();
                    } else {
                        window.location = rootUrl + self.PersonalDetails_url + "?email=" + self.emailAddress() + "&studentNumber=" + self.studentNumber();
                    }
                },
                error: function (error) {
                    console.log(error.responseText);
                    self.IsApplicant(false);
                    waitingDialog.hide();
                }
            }).done(function (response) {
                console.log(response);
            });
        }
        else {
            Index.errors.showAllMessages();
            window.scrollTo(0, 0);
        }
       
    }

    self.toggleLoadingOverlay = function (state) {

        switch (state) {
            case "show":
                $("#proceedWithAppSection")
                    .LoadingOverlay(state,
                    {
                        image: "./Images/avatar-red.gif",
                        size: "30%"
                    });
            case "hide":
                $("#proceedWithAppSection").LoadingOverlay(state, true);
        }
    };
}

ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindings, vm) {
        ko.utils.registerEventHandler(element, "keyup", function (event) {
            if (event.keyCode === 13) {
                ko.utils.triggerEvent(element, "change");
                valueAccessor().call(vm, vm); //set "this" to the data and also pass it as first arg, in case function has "this" bound
            }

            return true;
        });
    }
};

$(document).ready(function () {

    $("#tabs").hide();

    $.ajaxSetup({
        contentType: 'application/json'
    });
    vm = new Index();
    ko.applyBindings(vm);
});




