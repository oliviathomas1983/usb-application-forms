﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebForms
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Page", action = "MBA", id = UrlParameter.Optional }
            );

           // routes.MapRoute(
           //     name: "Default2",
           //     url: "{controller}/{action}/{id}",
           //     defaults: new { controller = "Contact", action = "GetApplicationStatus", id = UrlParameter.Optional }
           // );

           // routes.MapRoute(
           //    name: "Default3",
           //    url: "{controller}/{action}/{id}",
           //    defaults: new { controller = "LoadData", action = "PersonalDropdown", id = UrlParameter.Optional }
           //);
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{action}/{id}",
            //    defaults: new { controller = "Page", action = "MBA", id = UrlParameter.Optional },
            //    namespaces: new[] { "namespace WebForms.Controllers" });

            //routes.MapRoute(
            //name: "Default2",
            //url: "{action}/{id}",
            //defaults: new { controller = "Contact", action = "GetApplicationStatus", id = UrlParameter.Optional },
            //namespaces: new[] { "namespace WebForms.Controllers" });

        //    routes.MapRoute(
        //      "Default1",
        //      "Page/{action}",
        //      new
        //      {
        //          controller = "page",
        //          action = "MBA"
        //      }
        //    );

        //    routes.MapRoute(
        //       "Default2",
        //       "Contact/{action}", 
        //       new
        //       {
        //           controller = "contact",
        //           action = "GetApplicationStatus"
        //       }
        //     );

        //    routes.MapRoute(
        //  "Default3",
        //  "LoadData/{action}",
        //  new
        //  {
        //      controller = "loaddata",
        //      action = "AddressDropdown"
        //  }
        //);

            //routes.MapRoute(
            //  "Default3",
            //  "Contact/{action}",
            //  new
            //  {
            //      controller = "Contact",
            //      action = "GetApplicationStatus"
            //  }
            //);


        }
    }
}
