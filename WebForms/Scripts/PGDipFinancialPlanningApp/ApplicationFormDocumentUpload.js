﻿$(function () {
    $('#SignedDeclaration').fileupload({
        dataType: "json",
        url: "/api/UploadGMATSelectionTest",
        limitConcurrentUploads: 1,
        sequentialUploads: true,
        progressInterval: 100,
        //maxChunkSize: 10000,
        add: function (e, data) {
            $('#filelistholder').removeClass('hide');
            data.context = $('<div />').text(data.files[0].name).appendTo('#filelistholder');
            $('</div><div class="progress"><div class="bar" style="width:0%"></div></div>').appendTo(data.context);
            $('#btnUploadAll').click(function () {
                data.submit();
            });
        },
        done: function (e, data) {
            data.context.text(data.files[0].name + '... Completed');
            $('</div><div class="progress"><div class="bar" style="width:100%"></div></div>').appendTo(data.context);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#overallbar').css('width', progress + '%');
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.bar').css('width', progress + '%');
        }
    })

    $('#ApplicationFee').fileupload({
        dataType: "json",
        url: "/api/UploadApplicationFee",
        limitConcurrentUploads: 1,
        sequentialUploads: true,
        progressInterval: 100,
        //maxChunkSize: 10000,
        add: function (e, data) {
            $('#filelistholder').removeClass('hide');
            data.context = $('<div />').text(data.files[0].name).appendTo('#filelistholder');
            $('</div><div class="progress"><div class="bar" style="width:0%"></div></div>').appendTo(data.context);
            $('#btnUploadAll').click(function () {
                data.submit();
            });
        },
        done: function (e, data) {
            data.context.text(data.files[0].name + '... Completed');
            $('</div><div class="progress"><div class="bar" style="width:100%"></div></div>').appendTo(data.context);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#overallbar').css('width', progress + '%');
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.bar').css('width', progress + '%');
        }
    })

    $('#GMATSelectionTest').fileupload({
        dataType: "json",
        url: "/api/UploadGMATSelectionTest",
        limitConcurrentUploads: 1,
        sequentialUploads: true,
        progressInterval: 100,
        //maxChunkSize: 10000,
        add: function (e, data) {
            $('#filelistholder').removeClass('hide');
            data.context = $('<div />').text(data.files[0].name).appendTo('#filelistholder');
            $('</div><div class="progress"><div class="bar" style="width:0%"></div></div>').appendTo(data.context);
            $('#btnUploadAll').click(function () {
                data.submit();
            });
        },
        done: function (e, data) {
            data.context.text(data.files[0].name + '... Completed');
            $('</div><div class="progress"><div class="bar" style="width:100%"></div></div>').appendTo(data.context);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#overallbar').css('width', progress + '%');
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.bar').css('width', progress + '%');
        }
    })

    $('#CertifiedAcademicRecords').fileupload({
        dataType: "json",
        url: "/api/UploadCertifiedAcademicRecord",
        limitConcurrentUploads: 1,
        sequentialUploads: true,
        progressInterval: 100,
        //maxChunkSize: 10000,
        add: function (e, data) {
            $('#filelistholder').removeClass('hide');
            data.context = $('<div />').text(data.files[0].name).appendTo('#filelistholder');
            $('</div><div class="progress"><div class="bar" style="width:0%"></div></div>').appendTo(data.context);
            $('#btnUploadAll').click(function () {
                data.submit();
            });
        },
        done: function (e, data) {
            data.context.text(data.files[0].name + '... Completed');
            $('</div><div class="progress"><div class="bar" style="width:100%"></div></div>').appendTo(data.context);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#overallbar').css('width', progress + '%');
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.bar').css('width', progress + '%');
        }
    })

    $('#CertifiedDegrees').fileupload({
        dataType: "json",
        url: "/api/upload",
        limitConcurrentUploads: 1,
        sequentialUploads: true,
        progressInterval: 100,
        //maxChunkSize: 10000,
        add: function (e, data) {
            $('#filelistholder').removeClass('hide');
            data.context = $('<div />').text(data.files[0].name).appendTo('#filelistholder');
            $('</div><div class="progress"><div class="bar" style="width:0%"></div></div>').appendTo(data.context);
            $('#btnUploadAll').click(function () {
                data.submit();
            });
        },
        done: function (e, data) {
            data.context.text(data.files[0].name + '... Completed');
            $('</div><div class="progress"><div class="bar" style="width:100%"></div></div>').appendTo(data.context);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#overallbar').css('width', progress + '%');
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.bar').css('width', progress + '%');
        }
    })

        $('#CV').fileupload({
        dataType: "json",
        url: "/api/UploadCV",
        limitConcurrentUploads: 1,
        sequentialUploads: true,
        progressInterval: 100,
        //maxChunkSize: 10000,
        add: function (e, data) {
            $('#filelistholder').removeClass('hide');
            data.context = $('<div />').text(data.files[0].name).appendTo('#filelistholder');
            $('</div><div class="progress"><div class="bar" style="width:0%"></div></div>').appendTo(data.context);
            $('#btnUploadAll').click(function () {
                data.submit();
            });
        },
        done: function (e, data) {
            data.context.text(data.files[0].name + '... Completed');
            $('</div><div class="progress"><div class="bar" style="width:100%"></div></div>').appendTo(data.context);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#overallbar').css('width', progress + '%');
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.bar').css('width', progress + '%');
        }
    })

    $('#MarriageCertificate').fileupload({
        dataType: "json",
        url: "/api/UploadMarriageCertificate",
        limitConcurrentUploads: 1,
        sequentialUploads: true,
        progressInterval: 100,
        //maxChunkSize: 10000,
        add: function (e, data) {
            $('#filelistholder').removeClass('hide');
            data.context = $('<div />').text(data.files[0].name).appendTo('#filelistholder');
            $('</div><div class="progress"><div class="bar" style="width:0%"></div></div>').appendTo(data.context);
            $('#btnUploadAll').click(function () {
                data.submit();
            });
        },
        done: function (e, data) {
            data.context.text(data.files[0].name + '... Completed');
            $('</div><div class="progress"><div class="bar" style="width:100%"></div></div>').appendTo(data.context);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#overallbar').css('width', progress + '%');
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.bar').css('width', progress + '%');
        }
    })

    $('#MotivationalEssays').fileupload({
        dataType: "json",
        url: "/api/upload",
        limitConcurrentUploads: 1,
        sequentialUploads: true,
        progressInterval: 100,
        //maxChunkSize: 10000,
        add: function (e, data) {
            $('#filelistholder').removeClass('hide');
            data.context = $('<div />').text(data.files[0].name).appendTo('#filelistholder');
            $('</div><div class="progress"><div class="bar" style="width:0%"></div></div>').appendTo(data.context);
            $('#btnUploadAll').click(function () {
                data.submit();
            });
        },
        done: function (e, data) {
            data.context.text(data.files[0].name + '... Completed');
            $('</div><div class="progress"><div class="bar" style="width:100%"></div></div>').appendTo(data.context);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#overallbar').css('width', progress + '%');
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.bar').css('width', progress + '%');
        }
    })
});