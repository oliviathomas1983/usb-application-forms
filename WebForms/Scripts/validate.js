﻿
Validate = function() {
    var self = {
       invalidHandler: function(event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                    ? 'You missed 1 field. It has been highlighted'
                    : 'You missed ' + errors + ' fields. They have been highlighted';
               // alert(message);
            }
        },
        errorClass: "text-danger",
        errorElement: "span"
    };

    $.each($('form'), function (index, item) {
        MakeInputIdsUnique($(item));
    });
    return self;
};

var message = "Please supply a valid value ";
$.validator.addMethod(
        "regex",
        function (value, element, regexp) {

            var exp;
            if (IsDefinedAndNotNull(regexp)) {
                if (IsDefinedAndNotNull(regexp.Validate)) {
                    exp = regexp.Validate();
                } else
                {
                    exp = regexp;
                }
                if (exp == "Missing") {
                    return false;
                }
                var re = new RegExp(exp);
                return this.optional(element) || re.test(value);
            }
            return true;
        },
        message 
);


$.validator.addMethod(
        "fileuploadrequired",
        function (value, element, condition) {
            //return ko.contextFor(element).$parent.Files().length > 1;

            return true;
        },
        "You must upload at least one document on this category" 
);

$.validator.addClassRules({

    "val-fileupload-required": {
        fileuploadrequired: true
    },
    "val-required": {
        required: true
    },
    "val-parequired": {
        required: {
            depends: function() {
                return !($("#SameAsPostalAddress").is(":checked"));
            }
        }
    },
    "val-id": {
        regex: {
            Validate: function () {
                var dependentField = 'Country';
                var countries = ko.dataFor($("#" + dependentField)[0])['Countries'];
                var countryId = ($("#" + dependentField)).val();
                var country = $.grep(countries, function (item) {
                    return item.ID() == countryId;
                });
                if (country.length == 0)
                    return 0;
                var c = country[0];
                var result = c.Regex == null ? null : c.Regex();
                return result;
            }
        }
        
    },
    "val-date": {
        date: true
    },
    "val-email": {
        email: true
    } ,
    "val-digits": {
        digits: true
    },
    "val-phone": {
        regex:  "[0](\\d{9})|([0](\\d{2})( |-)((\\d{3}))( |-)(\\d{4}))|[0](\\d{2})( |-)(\\d{7})"
    }, 
    "val-cell": {
        regex: "^0(6|7|8){1}[0-9]{1}[0-9]{7}$"
    },
   
    "val-comment": {
        maxlength:  500
    }
    , "val-postcode": {
        minlength: 4,
        maxlength: 4,
        digits: true
    }
    , "val-persalno": {
        regex: "[A-Z]{1}[0-9]{7}"
    }
});

function MakeInputIdsUnique(elem) {

    var element = elem;
    if ($(element).is("select") || $(element).is("input") || $(element).is("textarea") ){
        AddValidationMessage(element);
    }

    $.each($(element).find("select, input, textarea"), function (index, item) {
        AddValidationMessage(item);
    });
}

function AddValidationMessage(item) {

    var id = $(item).attr("id");
    if (id == undefined)
        id = "input" + ++jQuery.guid;
    var uniqueid = ($("*[id*=" + id + "]").length > 1) ? (id + ++jQuery.guid) : id;
    $(item).attr("id", uniqueid);
    $(item).attr("name", uniqueid);
}




function GetForm(selector) {

    var form = $(selector).first().closest('form');

    if (!form.length) {
        form = $(selector).first().find('form');
    }

    return form;
}


