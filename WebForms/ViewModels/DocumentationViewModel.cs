﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;


namespace WebForms.ViewModels
{
    public class DocumentationViewModel
    {
        public bool UploadGMatScores { get; set; }

        public List<Document> RequiredDocumentsViewModel { get; set; }
    }
}